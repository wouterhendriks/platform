import { HareScriptVM } from "./wasm-hsvm";
import { HSVMHeapVar } from "./wasm-hsvmvar";
import { HSVM_VariableId } from "wh:internal/whtree/lib/harescript-interface";

export interface HSVMCallsProxy {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- it's overhead to have to define the type whenever you invoke. But feel free to extend commonlibs.ts!
  [key: string]: (...args: unknown[]) => Promise<any>;
}

export type HSVMObject = HSVMObjectWrapper & HSVMCallsProxy;

export type HSVMLibrary = HSVMCallsProxy;

export function argsToHSVMVar(vm: HareScriptVM, args: unknown[]): HSVMHeapVar[] {

  const funcargs: HSVMHeapVar[] = [];
  for (const arg of args) {
    const newvar = vm.allocateVariable();
    newvar.setJSValue(arg);
    funcargs.push(newvar);
  }
  return funcargs;
}

export function cleanupHSVMCall(vm: HareScriptVM, args: HSVMHeapVar[], result: HSVMHeapVar | undefined) {
  for (const arg of args)
    arg.dispose();

  result?.dispose();
}

export class HSVMObjectWrapper {
  $obj: HSVMHeapVar;

  constructor(vm: HareScriptVM, objid: HSVM_VariableId) {
    this.$obj = vm.allocateVariable();
    vm.wasmmodule._HSVM_CopyFrom(vm.hsvm, this.$obj.id, objid);
  }

  async $get(prop: string): Promise<unknown> {
    const retvalholder = await this.$obj.getMember(prop);
    const retval = retvalholder.getJSValue();
    retvalholder.dispose();
    return retval;
  }

  async $invoke(name: string, args: unknown[]) {
    const funcargs = argsToHSVMVar(this.$obj.vm, args);
    let result: HSVMHeapVar | undefined;
    try {
      result = await this.$obj.vm.callWithHSVMVars(name, funcargs, this.$obj.id);
      return result ? result.getJSValue() : undefined;
    } finally {
      cleanupHSVMCall(this.$obj.vm, funcargs, result);
    }
  }
}

///Proxies an object living in the HSVM
export class HSVMObjectProxy {
  get(target: HSVMObjectWrapper, prop: string, receiver: unknown) {
    if (prop === "then") //do not appear like our object is a promise
      return undefined;
    if (prop in target)
      return (target as unknown as Record<string, unknown>)[prop];

    return (...args: unknown[]) => target.$invoke(prop, args);
  }
}

export async function invokeOnVM(vm: HareScriptVM, lib: string, name: string, args: unknown[]) {
  //TODO detect HSVM Vars and copyfrom them?
  const funcargs = argsToHSVMVar(vm, args);

  const result = await vm.callWithHSVMVars(lib + "#" + name, funcargs);
  try {
    return result?.getJSValue();
  } finally {
    cleanupHSVMCall(vm, funcargs, result);
  }
}

export class HSVMLibraryProxy {
  private readonly vm: HareScriptVM;
  private readonly lib: string;

  constructor(vm: HareScriptVM, lib: string) {
    this.vm = vm;
    this.lib = lib;
  }

  get(target: object, prop: string, receiver: unknown) {
    if (prop === "then") //do not appear like our object is a promise
      return undefined;

    return (...args: unknown[]) => this.invoke(prop, args);
  }

  ///JavaScript supporting invoke
  async invoke(name: string, args: unknown[]) {
    return invokeOnVM(this.vm, this.lib, name, args);
  }
}

export class HSVMObjectCache {
  cachedobjects = new Map<number, WeakRef<HSVMObject>>;
  nextcachedobjectid = 53000;
  vm;

  constructor(vm: HareScriptVM) {
    this.vm = vm;
  }

  ensureObject(id: HSVM_VariableId): HSVMObject {
    //Do we already have a proxy for this object?
    const proxycolumnid = this.vm.getColumnId("^$proxyid");
    if (this.vm.wasmmodule._HSVM_ObjectMemberExists(this.vm.hsvm, id, proxycolumnid)) {
      const proxyvar = this.vm.wasmmodule._HSVM_ObjectMemberRef(this.vm.hsvm, id, proxycolumnid, /*skipaccess=*/1);
      const proxyvarid = this.vm.wasmmodule._HSVM_IntegerGet(this.vm.hsvm, proxyvar);
      const existingproxy = this.cachedobjects.get(proxyvarid)?.deref();
      if (existingproxy)
        return existingproxy;

      this.cachedobjects.delete(proxyvarid);
      this.vm.wasmmodule._HSVM_ObjectMemberDelete(this.vm.hsvm, id, proxycolumnid, /*skipaccess=*/1);
    }

    //Assign new proxy number
    const objid = ++this.nextcachedobjectid;
    const proxy = new HSVMObjectProxy;
    const obj: HSVMObject = new Proxy(new HSVMObjectWrapper(this.vm, id), proxy) as HSVMObject;
    this.cachedobjects.set(objid, new WeakRef(obj));

    const intvar = this.vm.wasmmodule._HSVM_AllocateVariable(this.vm.hsvm);
    this.vm.wasmmodule._HSVM_IntegerSet(this.vm.hsvm, intvar, objid);
    this.vm.wasmmodule._HSVM_ObjectMemberInsert(this.vm.hsvm, id, proxycolumnid, intvar, /*isprivate=*/1, /*skipaccess=*/1);
    this.vm.wasmmodule._HSVM_DeallocateVariable(this.vm.hsvm, intvar);

    return obj;
  }

  countObjects() {
    //Remove expired entries from the map
    this.cachedobjects.forEach((weakref, key, map) => {
      if (!weakref.deref()) map.delete(key);
    });
    return this.cachedobjects.size;
  }
}
