/* TODO get things to export */

export { allocateHSVM, type HareScriptVM } from "./wasm-hsvm";
export type { HSVMObject } from "./wasm-proxies";
export { HareScriptBlob, HareScriptMemoryBlob, isHareScriptBlob } from "./hsblob";
export { loadlib } from "./contextvm";
