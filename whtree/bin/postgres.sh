#!/bin/bash

# Let's start (and setup?) PostgreSQL!
if [ -z "$WEBHARE_DBASENAME" ]; then
  echo "WEBHARE_DBASENAME name not set"
  exit 1
fi
if [ -z "$WEBHARE_DATAROOT" ]; then
  echo "WEBHARE_DATAROOT name not set"
  exit 1
fi

# We put everything under a postgresql folder, so we can chown that to ourselves in the future
PSROOT="${WEBHARE_DATAROOT}postgresql"

if [ -n "$WEBHARE_IN_DOCKER" ]; then  #TODO should share with recreate-database and postgres-single
  RUNAS="chpst -u postgres:whdata"
  PSBIN="/usr/lib/postgresql/11/bin/"
elif [ "$WHBUILD_PLATFORM" = "darwin" ]; then
  # Read the version of the PostgreSQL database, fall back to version 13 (as specified in webhare.rb) for new databases
  PGVERSION=$(cat $PSROOT/db/PG_VERSION 2>/dev/null)
  if [ -z "${PGVERSION}" ]; then
    PGVERSION=13
  fi
  if [ -x "$(brew --prefix)/opt/postgresql@${PGVERSION}/bin/postmaster" ]; then
    PSBIN="$(brew --prefix)/opt/postgresql@${PGVERSION}/bin/"
  else
    echo "This database requires postgres version @${PGVERSION}. Please install it"
  fi
else
  PSBIN="/usr/pgsql-11/bin/"
fi

cd "${BASH_SOURCE%/*}/../etc/"
if [ -z "$WEBHARE_PGCONFIGFILE" ]; then
  if [ -n "$WEBHARE_IN_DOCKER" ]; then
    WEBHARE_PGCONFIGFILE="${BASH_SOURCE%/*}/../etc/postgresql-docker.conf"
  else
    WEBHARE_PGCONFIGFILE="${BASH_SOURCE%/*}/../etc/postgresql-sourceinstall.conf"
  fi
fi

mkdir -p "$PSROOT"

function generateConfigFile()
{
  echo "include '$WEBHARE_PGCONFIGFILE'"
  # include_if_exists generates noise if the file doesn't exist
  if [ -f "$WEBHARE_DATAROOT/etc/postgresql-custom.conf" ]; then
    echo "include '$WEBHARE_DATAROOT/etc/postgresql-custom.conf'"
  fi
}

if [ ! -d "$PSROOT/db" ]; then

  # remove previous initialization attempt
  if [ -d "$PSROOT/tmp_initdb" ]; then
    echo "Removing previous initialization attempt in $PSROOT/tmp_initdb"
    rm -rf "$PSROOT/tmp_initdb"
  fi

  mkdir "$PSROOT/tmp_initdb/"

  if [ -n "$WEBHARE_IN_DOCKER" ]; then
    chown postgres "$PSROOT" "$PSROOT/tmp_initdb"
  fi

  echo "Prepare PostgreSQL database in $PSROOT"
  if ! $RUNAS $PSBIN/initdb -U postgres -D "$PSROOT/tmp_initdb" --auth-local=trust --encoding 'UTF-8' --locale='C' ; then
    echo DB initdb failed
    exit 1
  fi

  # Set the configuration file
  generateConfigFile > "$PSROOT/tmp_initdb/postgresql.conf"

  # CREATE DATABASE cannot be combined with other commands
  # log in to 'postgres' database so we can create our own
  if ! echo "CREATE DATABASE \"$WEBHARE_DBASENAME\";" | $RUNAS $PSBIN/postgres --single -D "$PSROOT/tmp_initdb" postgres ; then
    echo DB create db failed
    exit 1
  fi
  DOCKERGRANTS=
  if [ -n "$WEBHARE_IN_DOCKER" ]; then
    DOCKERGRANTS="GRANT SELECT ON ALL TABLES IN SCHEMA pg_catalog TO root;GRANT SELECT ON ALL TABLES IN SCHEMA information_schema TO root;"
  fi
  if ! echo "CREATE USER root;ALTER USER root WITH SUPERUSER;GRANT ALL ON DATABASE \"$WEBHARE_DBASENAME\" TO root;$DOCKERGRANTS" | $RUNAS $PSBIN/postgres --single -D "$PSROOT/tmp_initdb" "$WEBHARE_DBASENAME" ; then
    echo DB create user failed
    exit 1
  fi
  mv "$PSROOT/tmp_initdb/" "$PSROOT/db/"
else

  if [ -d "$PSROOT/db.switchto" ]; then
    echo "Switching to NEW postgresql database!"
    mv "$PSROOT/db" "$PSROOT/db.bak.$(date +%Y%m%dT%H%M%S)"
    mv "$PSROOT/db.switchto" "$PSROOT/db"
  fi

  generateConfigFile > "$PSROOT/db/postgresql.conf"

  if [ -f "$PSROOT/db/pg_hba.conf" ]; then
    # previous webhares created this file, remove it because it is now unused
    rm -f "$PSROOT/db/pg_hba.conf"
  fi
fi


echo "Starting PostgreSQL"
exec $RUNAS $PSBIN/postmaster -D "$PSROOT/db" 2>&1
