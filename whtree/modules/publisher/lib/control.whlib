﻿<?wh
/** @short Scheduler/queue control
    @long Controls the publication queue of WebHare AP. This library offers function to process
          the file 'published' cell, add files to the publication queue and analyze the contents
          of the queue.
    @private ScheduleFile/FolderRepublish must become a single function, and with MakeChangesIndexedPromise exposed elsehwere.
             clean up arg syntax (with a transitional vararg period and we can reenable control.whlib)
*/

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::internal/jobs.whlib";

LOADLIB "mod::publisher/lib/database.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/tasks.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/internal/services.whlib";
LOADLIB "mod::system/lib/internal/whfs/events.whlib";
LOADLIB "mod::system/lib/webserver/management.whlib";
LOADLIB "mod::system/lib/internal/whfs/support.whlib" EXPORT SetFlagsInPublished, GetFlagsFromPublished, IsPublish, GetErrorFromPublished, GetOncePublishedFromPublished, TestFlagFromPublished
                                                          , PublishedFlag_OncePublished, PublishedFlag_Scheduled, PublishedFlag_Warning, PublishedFlag_HasWebDesign
                                                          , PublishedFlag_StripExtension, PublishedFlag_HasPublicDraft, PublishedFlag_SubmittedForApproval;

/** @short See if a document has events scheduled for it
    @param published A file.published cell
    @return True if the file was ever succesfully published (its file.url cell is valid) */
PUBLIC BOOLEAN FUNCTION GetScheduledFromPublished(INTEGER published)
{
  RETURN TestFlagFromPublished(published, PublishedFlag_Scheduled);
}



///Republish all files in the specified folder
///folderid:     Folder whose files should be republished
///allfirsttime: True to mark all files as published for the first time (eg. after a folder move. SHOULD DROP THIS FLAG)
PUBLIC MACRO __RepublishFolder(INTEGER folderid, BOOLEAN toplevel, BOOLEAN non_template_too, STRING mask, BOOLEAN onlyiffailed)
{
  INTEGER prio := toplevel ? PubPrio_FolderRepub : PubPrio_SubFolderRepub;
  UPDATE system.fs_objects SET published := ConvertToWillpublish(published, FALSE, FALSE, prio)
               WHERE parent = folderid
                     AND isfolder = FALSE
                     AND (mask = "*" ? TRUE : ToUppercase(fs_objects.name) LIKE ToUppercase(mask))
                     AND publish = TRUE
                     AND (non_template_too ? TRUE :
                                (fs_objects.lastpublishdate < fs_objects.modificationdate
                                 OR GetErrorFromPublished(published)!=0
                                 OR TestFlagFromPublished(published, PublishedFlag_HasWebDesign)
                                )
                     AND (onlyiffailed ? GetErrorFromPublished(published) != 0 : TRUE)
                         );

  OBJECT handler := GetWHFSCommitHandler();
  handler->FolderRepublish((SELECT AS INTEGER parentsite FROM system.fs_objects WHERE id=folderid), folderid);
}

/** Republish all files in the specified folder, and all files in its subfolders*/
PRIVATE MACRO RepublishFolderRecursive(INTEGER folderid, BOOLEAN toplevel, BOOLEAN non_template_too)
{
  __RepublishFolder(folderid, toplevel, non_template_too, "*", FALSE);

  OBJECT folder := OpenWHFSObject(folderid);
  IF(NOT ObjectExists(folder))
    RETURN; //no longer exists (or a virtual theme folder, but we're trying to get rid of them)

  IF (folder->parentsite != 0) // within a site?
  {
    FOREVERY (RECORD folderrec FROM SELECT id FROM system.fs_objects
                                            WHERE parent = folderid
                                                  AND isfolder = TRUE
                                                  AND type != 1
                                                  AND url != "")
    {
      RepublishFolderRecursive(folderrec.id, FALSE, non_template_too);
    }
  }
  ELSE // outside a site, republish all published sites
  {
    RECORD ARRAY sites :=
        SELECT sites.id
          FROM system.sites
             , system.fs_objects
         WHERE sites.id IN folder->GetDescendantFolderids()
           AND fs_objects.id = sites.id
           AND fs_objects.url != "";

    FOREVERY (RECORD site FROM sites)
      RepublishFolderRecursive(site.id, FALSE, non_template_too);
  }
}

/** @short republish a specific file
    @long Use this function to schedule a republish of a specific file in your site.
          If a user does not have edit rights on a file, the file will not be republished.
    @param fileid The Id of the file to republish
*/
PUBLIC MACRO ScheduleFileRepublish(INTEGER fileid)
{
  OBJECT obj := OpenWHFSObject(fileid);
  IF(ObjectExists(obj) AND obj->isfolder = FALSE)
    obj->ScheduleRepublish();
}


/** @short republish a specific folder
    @long Use this function to schedule the publication of all files in a specific folder in your site.
          If a user does not have edit rights on a file, the file will not be republished.
    @param folderid The Id of the folder to republish
    @param recursive TRUE to republish the folder, and all subfolders.
    @param non_templates_too (optional) also publish files which do not depend on a template and rarely need to be republished
*/
PUBLIC MACRO ScheduleFolderRepublish(INTEGER folderid, BOOLEAN recursive, BOOLEAN non_templates_too DEFAULTSTO FALSE)
{
  if (recursive)
  {
    // call the RepublishFolder macro
    RepublishFolderRecursive(folderid,TRUE, non_templates_too);
  }
  else
  {
    // call the RepublishFolder macro
    __RepublishFolder(folderid,TRUE, non_templates_too, "*", FALSE);
  }
}


/** @short Add a scheduled task for a file
    @param fileid ID of the affected file
    @param when Date and time, in UTC, when the task should be executed
    @param eventtype The type of event to schedule (1 = start publishing (or republish), 2 = stop publishing, 3 = move file, 4 = delete file, 5 = make file indexdoc)
    @param destfolder Destination folder for scheduled move commands */
PUBLIC MACRO AddFileTask(INTEGER fileid, DATETIME when, INTEGER eventtype, INTEGER destfolder)
{
  // insert the event
  INSERT INTO publisher.schedule (file,when,event,folder)
                          VALUES (fileid,when,eventtype,destfolder);

  // mark the file as having a schedule attached
  UPDATE system.fs_objects
         SET published := SetFlagsInPublished(published, PublishedFlag_Scheduled, TRUE)
         WHERE id = fileid;

  ScheduleTimedTask("publisher:scheduledtasks", [ when := when ]);
}

/** @short Edit a scheduled task
    @param taskid ID of the task to edit
    @param when Date and time, in UTC, when the task should be executed
    @param eventtype The type of event to schedule (1 = start publishing, 2 = stop publishing, 3 = move file, 4 = delete file, 5 = make file indexdoc)
    @param destfolder Destination folder for scheduled move commands */
PUBLIC MACRO EditFileTask(INTEGER taskid, DATETIME when, INTEGER eventtype, INTEGER destfolder)
{
  RECORD task := SELECT * FROM publisher.schedule WHERE id=taskid;
  IF (NOT RecordExists(task))
    RETURN;

  UPDATE publisher.schedule SET when   := VAR when,
                                event  := eventtype,
                                folder := destfolder
                            WHERE schedule.id = taskid;

  //moved a scheduled item forward?
  IF (when < task.when)
    ScheduleTimedTask("publisher:scheduledtasks", [ when := when ]);
}





/** @short Delete a scheduled task
    @param taskid ID of the task to delete */
PUBLIC MACRO DeleteFileTask(INTEGER taskid)
{
  RECORD task := SELECT * FROM publisher.schedule WHERE id=taskid;
  IF (NOT RecordExists(task))
    RETURN;

  DELETE FROM publisher.schedule WHERE id = taskid;

  IF (NOT RecordExists(SELECT FROM publisher.schedule WHERE schedule.file = task.file LIMIT 1))
  {
    //No more remaining tasks, so disable the 'Scheduled' status for the file
    UPDATE system.fs_objects
       SET published := SetFlagsInPublished(published, PublishedFlag_Scheduled, FALSE)
     WHERE id = task.file;
  }
}

MACRO DoFolder(INTEGER folderid, STRING filemask, BOOLEAN top, BOOLEAN nontemplatetoo, BOOLEAN recursive, BOOLEAN onlyiffailed)
{
  __RepublishFolder(folderid, /*toplevel=*/top, /*nontemplatetoo=*/nontemplatetoo, filemask, onlyiffailed);

  IF(recursive)
  {
    FOREVERY (INTEGER subfolderid FROM SELECT AS INTEGER ARRAY id FROM system.fs_objects WHERE parent = folderid AND isfolder)
      DoFolder(subfolderid, filemask, FALSE, nontemplatetoo, TRUE, onlyiffailed);
  }
}
PUBLIC RECORD FUNCTION __DoRepublishByMask(STRING mask, BOOLEAN nontemplatetoo, BOOLEAN recursive, MACRO PTR callback, BOOLEAN onlyiffailed, STRING limitdesign)
{
  STRING sitemask := Left(mask, SearchSubstring(mask,':'));
  STRING foldername;
  STRING filemask;
  RECORD retval := [ errors := DEFAULT STRING ARRAY ];

  IF (sitemask = "")
  {
    sitemask := mask;
    foldername := "/";
    filemask := "*";
  }
  ELSE
  {
    STRING filefolderpath := Substring(mask, Length(sitemask)+1);
    foldername := SearchSubstring(filefolderpath,'/') = -1 ? "/" : GetDirectoryFromPath(filefolderpath);
    filemask := SearchSubstring(filefolderpath,'/') = -1 ? filefolderpath : GetNameFromPath(filefolderpath);
  }

  RECORD ARRAY affected_sites := SELECT id, name
                                   FROM system.sites
                                  WHERE outputweb != 0
                                        AND locked = FALSE
                                        AND ToUppercase(sites.name) LIKE ToUppercase(sitemask)
                              ORDER BY ToUppercase(sites.name);

  IF(limitdesign != "")
  {
    affected_sites := OpenWHFSType("http://www.webhare.net/xmlns/publisher/sitesettings")->Enrich(affected_sites, "ID", ["sitedesign"]);
    DELETE FROM affected_sites WHERE affected_sites.sitedesign != limitdesign;
  }

  IF(Length(affected_sites)=0)
  {
    INSERT "No site matched mask '" || mask || "'" INTO retval.errors AT END;
    RETURN retval;
  }

  FOREVERY(RECORD siterec FROM affected_sites)
  {
    GetPrimary()->PushWork();

    IF(callback != DEFAULT MACRO PTR)
      callback( ((#siterec+1) * 1f)/Length(affected_sites), siterec.name);

    OBJECT folder := OpenWHFSObject(siterec.id);
    IF(foldername != "/")
      folder := folder->OpenByPath(foldername, [ allowinitialslash := TRUE ]);

    IF(NOT ObjectExists(folder))
    {
      INSERT "Cannot find folder '" || foldername || "' in site '" || siterec.name || "'" INTO retval.errors AT END;
    }
    ELSE
    {
      //ScheduleFolderRepublish(site.root, TRUE, this->nontemplatetoo);
      DoFolder(folder->id, filemask, TRUE, nontemplatetoo, recursive, onlyiffailed);
    }
    GetPrimary()->PopWork();
  }
  RETURN retval;
}

PUBLIC MACRO ScheduleMaskRepublish(STRING mask, BOOLEAN recursive, BOOLEAN non_templates_too DEFAULTSTO FALSE)
{
  __DoRepublishByMask(mask, non_templates_too, recursive, DEFAULT MACRO PTR, FALSE, "");
}

PUBLIC OBJECT FUNCTION MakeChangesIndexedPromise()
{
  RETURN GetWHFSCommitHandler()->WaitForChangesIndexed();
}

BOOLEAN FUNCTION IsFolderPublishing(OBJECT rootfolder, BOOLEAN accepterrors, INTEGER ARRAY expecterrorsfor)
{
  // See if any object is marked as currently publishing
  INTEGER ARRAY folderids := [ INTEGER(rootfolder->id) ];
  folderids := folderids CONCAT rootfolder->GetDescendantFolderids();

  RECORD ARRAY objects := SELECT id, isfolder, publish, published, whfspath, errordata FROM system.fs_objects WHERE NOT isfolder AND parent IN folderids;
  FOREVERY (RECORD o FROM objects)
    IF (o.publish AND (o.published%100000) > 0 AND (o.published%100000) < 100)
      RETURN TRUE; //these files are still queued

  // Query the publisher queue if any of these objects are on the queue
  OBJECT service := __OpenSynchronousWebHareService("publisher:publication");
  INTEGER ARRAY scheduled := service->TestFilesScheduled(SELECT AS INTEGER ARRAY id FROM objects);
  BOOLEAN isscheduled := IsValueSet(scheduled);
  service->CloseService();

  FOREVERY(INTEGER expected FROM expecterrorsfor)
  {
    RECORD obj := SELECT * FROM objects WHERE id = expected;
    IF(NOT RecordExists(obj))
      THROW NEW Exception(`Expected publication of file #${expected} to fail but it's not inside folder ${rootfolder->whfspath}`);
    IF(GetErrorFromPublished(obj.published) = 0)
      THROW NEW Exception(`Publication of file ${obj.whfspath} (#${expected}) unexpectedly succeeded`);
  }

  IF(NOT accepterrors)
  {
    BOOLEAN anyerrors;
    FOREVERY (RECORD o FROM objects)
      IF(o.id NOT IN expecterrorsfor AND GetErrorFromPublished(o.published) != 0)
      {
        Print(`Publication error for #${o.id} (${o.whfspath}): errorcode ${GetErrorFromPublished(o.published)}${o.errordata != "" ? ","||o.errordata:""}\n`);
        anyerrors := TRUE;
      }

    IF(anyerrors)
      THROW NEW Exception(`Unexpected publication errors - you may need to pass accepterrors := TRUE to WaitForPublishCompletion if this was intentional\n`);
  }

  RETURN isscheduled;
}

/** Wait for all event completions to finish
    @param deadline Error out when the wait hasn't completed after this time
*/
MACRO WaitForEventCompletions(DATETIME deadline)
{
  OBJECT eventcompletionlink := ConnectToGlobalIPCPort("system:eventcompletion");
  WHILE (TRUE)
  {
    /* A single request is usually enough for synchronization, because it handles all
       completions between requests
    */
    RECORD rec := eventcompletionlink->DoRequest( [ type := "havependingcompletions" ]);
    IF(NOT CellExists(rec,"msg"))
    {
      DumpValue(rec,'tree');
      THROW NEW Exception("system:eventcompletion is unexpectedly unavailable");
    }
    IF (NOT rec.msg.result)
      BREAK;

    Sleep(10);
    IF(GetCurrentDatetime() >= deadline)
      THROW NEW Exception("system:eventcompletion isn't completing");
  }
  eventcompletionlink->Close();
}

/** Wait for publication to complete
    @param startingfolder Folder we're waiting to complete republishing (recursively)
    @cell(datetime) options.deadline Maximum time to wait (defaults to MAX_DATETIME)
    @cell(integer) options.reportfrequency If set, frequency (in ms) to report we're still waiting
    @cell(boolean) options.skipeventcompletion Do not wait for publisher (republish) events to complete processing
    @cell(boolean) options.accepterrors Accept any error in the publication (WebHare 4.33+)
    @cell(integer array) options.expecterrorsfor Explicit file IDs we accept and expect an error for
    @return True if publication completed, false on timeout*/
PUBLIC BOOLEAN FUNCTION WaitForPublishCompletion(INTEGER startingfolder, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ deadline := MAX_DATETIME
                             , reportfrequency := 0
                             , skipeventcompletion := FALSE
                             , accepterrors := FALSE
                             , expecterrorsfor := INTEGER[]
                             ], options);

  IF(NOT HavePrimaryTransaction() OR GetPrimary()->IsWorkOpen())
    THROW NEW Exception(`Cannot have open work while waiting for publish completion`);

  IF(NOT options.skipeventcompletion)
    WaitForEventCompletions(options.deadline);

  OBJECT eventmgr := NEW EventManager;
  TRY
  {
    OBJECT folderobj := OpenWHFSObject(startingfolder);
    INTEGER ARRAY folderids := [ INTEGER(folderobj->id) ] CONCAT folderobj->GetDescendantFolderids();
    FOREVERY(INTEGER fol FROM folderids)
      eventmgr->RegisterInterest("publisher:publish.folder." || fol);

    DATETIME nextfeedback := MAX_DATETIME;
    IF(options.reportfrequency != 0)
      nextfeedback := AddTimeToDate(options.reportfrequency, GetCurrentDatetime());

    WHILE(IsFolderPublishing(folderobj, options.accepterrors, options.expecterrorsfor))
    {
      /* Check every second because we won't get feedback from the publisher queue when items have completed
         (which will be some time AFTER they have committed)
      */
      RECORD event := eventmgr->ReceiveEvent(MIN[]([nextfeedback, options.deadline, AddTimeToDate(1000, GetCurrentDatetime()) ]));
      IF(event.status = "timeout")
      {
        IF(GetCurrentDatetime() >= options.deadline)
          RETURN FALSE;

        IF(GetCurrentDatetime() >= nextfeedback) //don't talk unless explicitly requested
        {
          PrintTo(2, "Publication in folder #" || startingfolder || " still isn't complete...\n");
          nextfeedback := AddTimeToDate(options.reportfrequency, GetCurrentDatetime());
        }
      }
    }
    RETURN TRUE;
  }
  FINALLY
  {
    eventmgr->Close();
  }
}

