<?wh
LOADLIB "mod::publisher/lib/internal/forms/parser.whlib";
LOADLIB "mod::system/lib/validation.whlib";

PUBLIC OBJECTTYPE FormdefValidator EXTEND XMLValidatorBase
<
  UPDATE PUBLIC MACRO ValidateDocument()
  {
    FOREVERY(OBJECT formnode FROM this->xml->GetElementsByTagNameNS("http://www.webhare.net/xmlns/publisher/forms","form")->GetCurrentElements())
    {
      STRING formname := formnode->GetAttribute("name");
      RECORD res := ParseFormDef(formnode, formname, this->respath, formname, DEFAULT OBJECT, [ validatexmlonly := TRUE, accepterrors := TRUE ]);
      this->errors := this->errors CONCAT res.errors;
      this->warnings := this->warnings CONCAT res.warnings;
    }
  }
>;

MACRO FixGUIDs(OBJECT formdef, OBJECT parentnode)
{
  FOREVERY(RECORD comp FROM GatherFormComponentsFromChildren(parentnode, [ getallchildren := FALSE ]))
  {
    IF(comp.type="component" AND NOT comp.node->HasAttribute("guid"))
      comp.node->SetAttribute("guid", formdef->formdefinitions->__CreateGUID(parentnode->localname = "handlers" ? "formhandler" : "formcomp"));
    ELSE IF(comp.type="option")
      comp.node->RemoveAttribute("guid"); //some 4.18 pre-releases incorrectly added this to options
  }
}

PUBLIC MACRO RepairFormIfNeeded(OBJECT formdef)
{
  //Fix any missing GUIDs
  FOREVERY(STRING fixgroupname FROM ["handlers","trash","page"])
    FOREVERY(OBJECT fixgroup FROM formdef->node->GetChildElementsByTagNameNS("http://www.webhare.net/xmlns/publisher/forms", fixgroupname)->GetCurrentElements())
    {
      IF(fixgroupname="page" AND NOT fixgroup->HasAttribute("guid"))
        fixgroup->SetAttribute("guid", formdef->formdefinitions->__CreateGUID("formpage"));
      FixGUIDs(formdef, fixgroup);
    }
}
