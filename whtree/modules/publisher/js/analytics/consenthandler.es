export * from "./consenthandler";

import { warnESFile } from "@mod-system/js/internal/es-warning";
warnESFile("@mod-publisher/js/analytics/consenthandler.es");
