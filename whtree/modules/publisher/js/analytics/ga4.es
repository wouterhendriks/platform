export * from "./ga4";

import { warnESFile } from "@mod-system/js/internal/es-warning";
warnESFile("@mod-publisher/js/analytics/ga4.es");
