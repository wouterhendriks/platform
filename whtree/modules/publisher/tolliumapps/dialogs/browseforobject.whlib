<?wh

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::publisher/lib/dialogs.whlib";
LOADLIB "mod::publisher/lib/control.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";


/** @short Adds a Recent Folder to a user's list of recently used folders.
    @param addfolderid The WHFS ID of the folder to add.
*/
MACRO AddRecentFolder(OBJECT user, INTEGER addfolderid)
{
  OBJECT folder := OpenWHFSObject(addfolderid);
  IF(NOT ObjectExists(folder) OR NOT folder->isfolder)
    RETURN;

  RECORD ARRAY recent := GetRecentFolders(user); //note, 'max' is already enfocred here
  recent := [ [ whfsid := addfolderid, creationdate := folder->creationdate ], ...(SELECT whfsid,creationdate FROM recent WHERE whfsid != addfolderid) ];

  GetPrimary()->BeginWork();
  user->WriteRegistryKey("publisher.whfs.recentfolders", recent, [createifneeded := TRUE]);
  GetPrimary()->CommitWork();
}

/** @short Retrieves a list of recently used folders for the current user.
    @long The number of results is limited to the user's setting for maximum number of stored folders
          and ordered by the date and time that it was added to the list in ascending order.
    @return(RECORD ARRAY)
      @cell(STRING) return.title The title of the folder, or it's name if not set.
      @cell(STRING) return.parentsite The parent site name of the folder (can be empty).
      @cell(STRING) return.fullpath The folder's full WHFS path.
      @cell(INTEGER) return.whfsid The folder's WHFS ID as given.
      @cell(INTEGER) return.ordering The folder entry's ordering within the list of recent folders.
      @cell(STRING) return.name The folder entry's unique identifying name within the list of recent folders.
*/
RECORD ARRAY FUNCTION GetRecentFolders(OBJECT user)
{
  RECORD ARRAY folders := user->ReadRegistryKey("publisher.whfs.recentfolders", [ fallback := RECORD[] ]);
  IF(Length(folders)=0)
    RETURN DEFAULT RECORD ARRAY;

  INTEGER ARRAY folderids := SELECT AS INTEGER ARRAY whfsid FROM folders;
  RETURN SELECT whfsid := fs_objects.id
              , title := fs_objects.name
              , fs_objects.creationdate
           FROM system.fs_objects, folders
          WHERE fs_objects.id IN folderids
                AND fs_objects.id = folders.whfsid
                AND fs_objects.creationdate = folders.creationdate
                AND fs_objects.isactive
       ORDER BY SearchElement(folderids, fs_objects.id)
          LIMIT user->ReadRegistryKey("tollium.dashboard.numrecentfolders", [ fallback := 5 ]);
}


PUBLIC STATIC OBJECTTYPE BrowseForObject EXTEND TolliumScreenBase
<
  PUBLIC PROPERTY value(GetValue, SetValue);

  PUBLIC PROPERTY selection(GetSelection, -);

  /** the folder which should initially be selected */
  PUBLIC PROPERTY folder(GetFolder, SetFolder);

  /** ??? (can only be set before RunDialog()) */
  PUBLIC INTEGER ARRAY roots;

  /** title to use for the dialog (can only be set before RunDialog()) */
  PUBLIC STRING title;

  /** text to show at the top of the dialog */
  PUBLIC STRING description;

  /** If TRUE, files are accepted
  */
  PUBLIC PROPERTY acceptfiles(pvt_acceptfiles, SetAcceptFiles);
  PUBLIC BOOLEAN pvt_acceptfiles;

  /** If TRUE, folders are accepted
  */
  PUBLIC PROPERTY acceptfolders(pvt_acceptfolders, setacceptfolders);
  PUBLIC BOOLEAN pvt_acceptfolders;

  /** If TRUE, root folder is accepted
  */
  PUBLIC PROPERTY acceptrootfolder(pvt_acceptrootfolder, setacceptrootfolder);
  PUBLIC BOOLEAN pvt_acceptrootfolder;

  /** If TRUE, external folders are accepted (the default)
  */
  PUBLIC PROPERTY acceptforeignfolders(pvt_acceptforeignfolders, setacceptforeignfolders);
  PUBLIC BOOLEAN pvt_acceptforeignfolders;

  /** If TRUE, non-published files will also be shown
  */
  PUBLIC BOOLEAN acceptunpublished;

  /** If TRUE, always hide the 'file view type' option
  */
  PUBLIC PROPERTY hideviewtype(pvt_hideviewtype, SetHideViewtype);
  PUBLIC BOOLEAN pvt_hideviewtype;

  /** Space-separated list of file/folder type namespaces of accepted files/folders, leave empty to accept all types
  */
  PUBLIC PROPERTY accepttypes(accepttypedefs, SetAcceptTypes);

  STRING ARRAY accepttypedefs; // Namespaces of accepted types
  INTEGER ARRAY accepttypeids; // Id's of accepted types
  INTEGER recyclebinid;

  /** Indicates whether only single or multiple selections are allowed
  */
  PUBLIC PROPERTY selectmode(pvt_selectmode, SetSelectMode);
  PUBLIC STRING pvt_selectmode;

  PUBLIC PROPERTY type(GetType, SetType);

  /** The type namespace for WHFS thumbnails
  */
  PUBLIC PROPERTY thumbnailwhfstype(GetThumbnailWHFSType, SetThumbnailWHFSType);
  STRING pvt_thumbnailwhfstype;

  /** The field within the thumbnailwhfstype namespace containing the actual thumbnail
  */
  PUBLIC PROPERTY thumbnailwhfsfield(GetThumbnailWHFSField, SetThumbnailWHFSField);
  STRING pvt_thumbnailwhfsfield;

  /** Show 'read-only' overlays, defaults to FALSE
  */
  PUBLIC PROPERTY showreadonly(^folders->showreadonly, ^folders->showreadonly);

  BOOLEAN submittedfolder;
  INTEGER mostrecentfolder;

  // Params: fileid
  PUBLIC MACRO Init(RECORD data)
  {
    BOOLEAN ismultiple := CellExists(data,'__selectmode') AND data.__selectmode = "multiple";
    VARIANT expectvalue;
    IF(ismultiple)
      expectvalue := INTEGER[];
    ELSE
      expectvalue := 0;
    DELETE CELL __selectmode FROM data;

    data := ValidateOptions(
       [ roots := INTEGER[]
       , accepttypes := STRING[]
       , type := "list"
       , title := ""
       , description := ""
       , acceptfiles := TRUE
       , acceptfolders := TRUE
       , acceptforeignfolders := FALSE //switching the default to FALSE, much safer...
       , acceptunpublished := TRUE // show both published and non-published files by default
       , hideviewtype := FALSE
       , thumbnailwhfstype := ""
       , thumbnailwhfsfield := ""
       , value := expectvalue
       , folder := 0
       , showreadonly := FALSE
       , recyclebinid := 0
       ], data, [ enums := [ type := [ "list", "thumbnails" ]
                           ]]);

    //set private props as much as possible to avoid many ApplySettingsToView calls
    this->pvt_selectmode := ismultiple ? "multiple" : "single";
    this->accepttypedefs := data.accepttypes;
    ^fileviewtype->value := data.type;
    this->roots := data.roots;
    this->title := data.title;
    this->description := data.description;
    this->pvt_acceptfiles   := data.acceptfiles;
    this->pvt_acceptfolders := data.acceptfolders;
    this->pvt_acceptforeignfolders := data.acceptforeignfolders;
    this->acceptunpublished := data.acceptunpublished;
    this->pvt_thumbnailwhfstype := data.thumbnailwhfstype;
    this->pvt_thumbnailwhfsfield := data.thumbnailwhfsfield;
    this->pvt_hideviewtype := data.hideviewtype;
    this->recyclebinid := data.recyclebinid;

    IF(data.showreadonly)
      this->showreadonly := TRUE;

    ^folders->filteritems := PTR this->FilterFolders;

    this->ApplySettingsToView(TRUE);

    IF(IsValueSet(data.value))
    {
      this->value := data.value;
    }
    ELSE IF(data.folder != 0)
    {
      this->folder := data.folder = -1 ? 0 : data.folder;
    }
    this->frame->focused := this->acceptfiles ? this->type = "thumbnails" ? ^filesthumbs : ^fileslist : ^folders;
  }

  MACRO PopulateRecentAndFavoriteFolders()
  {
    OBJECT user := this->tolliumuser;
    RECORD ARRAY recents := GetRecentFolders(this->tolliumuser);
    RECORD ARRAY favorites := SELECT title, target.folderid AS whfsid
                                FROM this->tolliumuser->GetRegistryKey("tollium.dashboard.shortcuts", RECORD[])
                               WHERE app  = "publisher:app" AND CellExists(target, "folderid") ;

    RECORD quicklinks := [ recentfolders := recents
                         , favoritefolders := favorites
                         ];

    RECORD ARRAY folders :=
      [ [ indent := 0, rowkey := -1, title := "Recent folders", type := "header" ]
      , ...(SELECT indent := 1, title, rowkey := whfsid, type := "recent" FROM recents)
      , [ indent := 0, rowkey := -2, title := "Favorite folders", type := "header" ]
      , ...(SELECT indent := 1, title, rowkey := whfsid, type := "favorite" FROM favorites)
      ];

    IF(Length(this->roots) > 0) // Filter out any folders that should not be visible:
    {
      RECORD ARRAY filteredfolders;
      FOREVERY(RECORD pathitem FROM folders)
      {
        OBJECT currentfolder := OpenWHFSObject(pathitem.rowkey);
        IF(pathitem.rowkey > 0 AND NOT ObjectExists(currentfolder))
          CONTINUE;// Skip folders which no longer exist

        BOOLEAN isvisible := FALSE;

        FOREVERY(INTEGER root FROM this->roots)
          IF((ObjectExists(currentfolder) AND currentfolder->IsChildOf(root)) OR pathitem.rowkey < 0)
            isvisible := TRUE;

        IF(isvisible)
          INSERT pathitem INTO filteredfolders AT END;
      }
      folders := filteredfolders;
    }
    //Get most recent or most favorite folder as suggestion first...
    this->mostrecentfolder := SELECT AS INTEGER rowkey FROM folders WHERE rowkey>0;

    ^gotobutton->visible := Length(this->roots) = 0;
    ^folderpathbutton->visible := Length(this->roots) = 0;
    ^gotoanything->enabled := Length(this->roots) = 0;

    /// Remove headers for empty lists:
    IF(Length(SELECT * FROM folders WHERE type = "favorite") = 0)
      DELETE FROM folders WHERE rowkey = -2;

    IF(Length(SELECT * FROM folders WHERE type = "recent") = 0)
      DELETE FROM folders WHERE rowkey = -1;

    /// Disable if we have nothing to show:
    IF(Length(folders) = 0)
    {
      ^folderpathbutton->enabled := FALSE;
      RETURN;
    }
    ELSE
      ^folderpathbutton->enabled := TRUE;

    //create the folder path menu, to be filled
    OBJECT folderpathmenu := this->CreateTolliumComponent("menuitem");
    FOREVERY(RECORD pathitem FROM folders)
    {
      OBJECT menuitem := this->CreateTolliumComponent("menuitem");
      menuitem->title := pathitem.title;
      menuitem->checked := FALSE;//pathitem.rowkey = ^folders->value;
      STRING hint;
      IF(CellExists(pathitem, "fullpath"))
      {
        hint := pathitem.fullpath;
        IF(pathitem.parentsite != "")
          hint := hint || " (" || pathitem.parentsite || ")";
      }
      menuitem->hint := hint;

      menuitem->indent := pathitem.indent;

      IF(pathitem.rowkey > 0)
      {
        OBJECT menuaction := this->CreateTolliumComponent("action");
        menuaction->onexecute := PTR this->SetFolder(pathitem.rowkey);
        menuitem->action := menuaction;
      }

      folderpathmenu->InsertMenuItemAfter(menuitem,DEFAULT OBJECT, FALSE);
    }

    // if we already have a folderpath menu, kill it
    IF (ObjectExists(^folderpathbutton->menu))
      ^folderpathbutton->menu->DeleteComponent();

    // assign the new menu. Folderpath is the button above the tree, folderpath2 is the button above the list, if the tree is hidden
    ^folderpathbutton->menu := folderpathmenu;
  }

  INTEGER FUNCTION GetFolder()
  {
    IF(^folders->selectmode = "multiple")
    {
      INTEGER ARRAY val := ^folders->value;
      IF(Length(val)>0)
        RETURN val[0];
      ELSE
        RETURN 0;
    }
    ELSE
    {
      RETURN ^folders->value;
    }
  }
  MACRO DoGotoAnything()
  {
    OBJECT gotoobject := this->LoadScreen("filemgrdialogs.gotoobject");
    IF (gotoobject->RunModal() = "ok")
    {
      INTEGER gotoid := gotoobject->id;
      IF(NOT ^filepart->visible) //get the parent  ?
        gotoid := SELECT AS INTEGER isfolder ? id : parent FROM system.fs_objects WHERE id = gotoid;

      this->value := gotoid;
    }
  }
  MACRO SetFolder(INTEGER folder)
  {
    IF(^folders->selectmode = "multiple")
    {
      IF(folder = 0)
        ^folders->value := DEFAULT INTEGER ARRAY;
      ELSE
        ^folders->value := [folder];
    }
    ELSE
    {
      ^folders->value := folder;
    }
  }

  PUBLIC OBJECT FUNCTION __GetCurrentFilelist()
  {
    RETURN this->type="thumbnails" ? ^filesthumbs : ^fileslist;
  }

  VARIANT FUNCTION GetSelection()
  {
    IF((this->acceptfiles AND NOT this->submittedfolder)
      OR (this->acceptfolders AND this->selectmode = "multiple"))
    {
      //ADDME are there tags in the filelist? currently only roots do it, so no
      IF(this->selectmode = "multiple")
        RETURN SELECT contentmode := "", id, modulekey := "" FROM this->__GetCurrentFilelist()->selection;

      RECORD selection := this->__GetCurrentFilelist()->selection;
      IF(RecordExists(selection))
        RETURN CELL[ contentmode := "", selection.id, modulekey := "" ]; //publisher is only in foldertree
      ELSE
        RETURN DEFAULT RECORD;
    }

    RECORD selectedfolder := ^folders->selection;
    IF(this->selectmode="single")
      RETURN RecordExists(selectedfolder) ? CELL[ selectedfolder.contentmode, id := selectedfolder.rowkey, selectedfolder.modulekey ] : DEFAULT RECORD;
    ELSE
      RETURN RecordExists(selectedfolder) ? [CELL[ selectedfolder.contentmode, id := selectedfolder.rowkey, selectedfolder.modulekey ]] : DEFAULT RECORD ARRAY;
  }

  INTEGER FUNCTION GetIdFromRow(RECORD row)
  {
    IF(row.id < 0 AND row.modulekey = "publisher:0.1")
      RETURN this->recyclebinid;
    IF(row.id = 0) //root
      RETURN -1;
    RETURN row.id;
  }

  /** @short gets the selected files
  */
  VARIANT FUNCTION GetValue()
  {
    IF(this->selectmode = "multiple")
      RETURN SELECT AS INTEGER ARRAY this->GetIdFromRow(row) FROM this->GetSelection() AS row;

    RECORD selection := this->GetSelection();
    RETURN RecordExists(selection) ? this->GetIdFromRow(selection) : 0;
  }

  /** @short set the selected files
  */
  MACRO SetValue(VARIANT value)
  {
    IF(this->acceptfiles)
    {
      INTEGER firstvalue;
      IF(TypeID(value) = TypeID(INTEGER ARRAY))
        firstvalue := Length(value) > 0 ? value[0] : 0;
      ELSE
        firstvalue := value;

      IF(firstvalue != 0)
      {
        ^folders->value := SELECT AS INTEGER parent FROM system.fs_objects WHERE id = firstvalue;
      }
      this->__GetCurrentFilelist()->value := value;
    }
    ELSE
    {
      ^folders->value := value;
    }
  }
  RECORD ARRAY FUNCTION FilterFolders(RECORD ARRAY infolders)
  {
    IF(NOT this->acceptforeignfolders)
      DELETE FROM infolders WHERE type=1;

    DELETE FROM infolders WHERE id < 0 AND (this->recyclebinid != 0 ? modulekey != "publisher:0.1"  : TRUE);
    RETURN infolders;
  }

  STRING FUNCTION GetType()
  {
    RETURN ^fileviewtype->value;
  }
  MACRO SetType(STRING type)
  {
    IF (type NOT IN ["list","thumbnails"])
      THROW NEW Exception('Unknown BrowseWHFSObject dialog type \''||type || "'");

    ^fileviewtype->value := type;
    ^fileslist->foldertree := type="list" ? ^folders : DEFAULT OBJECT;
    ^filesthumbs->foldertree := type="thumbnails" ? ^folders : DEFAULT OBJECT;
    this->ApplySettingsToView(FALSE);
  }

  MACRO SetThumbnailWHFSField(STRING fieldname)
  {
    this->pvt_thumbnailwhfsfield := fieldname;
  }

  MACRO SetThumbnailWHFSType(STRING typens)
  {
    this->pvt_thumbnailwhfstype := typens;
  }

  STRING FUNCTION GetThumbnailWHFSField()
  {
    IF(this->pvt_thumbnailwhfsfield = "")
      this->pvt_thumbnailwhfsfield := "thumbnail";

    RETURN this->pvt_thumbnailwhfsfield;
  }

  STRING FUNCTION GetThumbnailWHFSType()
  {
    IF(this->pvt_thumbnailwhfstype = "")
      this->pvt_thumbnailwhfstype := "http://www.webhare.net/xmlns/publisher/previewinfo";

    RETURN this->pvt_thumbnailwhfstype;
  }

  MACRO SetSelectMode(STRING type)
  {
    this->pvt_selectmode := type;
    this->ApplySettingsToView(FALSE);
  }

  MACRO SetAcceptFiles(BOOLEAN acceptfiles)
  {
    IF(this->pvt_acceptfiles = acceptfiles)
      RETURN;

    this->pvt_acceptfiles := acceptfiles;
    this->ApplySettingsToView(FALSE);
  }

  MACRO SetAcceptFolders(BOOLEAN acceptfolders)
  {
    this->pvt_acceptfolders := acceptfolders;
    this->ApplySettingsToView(FALSE);
  }

  MACRO SetAcceptRootFolder(BOOLEAN acceptrootfolder)
  {
    this->pvt_acceptrootfolder := acceptrootfolder;
    this->ApplySettingsToView(FALSE);
  }

  MACRO SetAcceptForeignFolders(BOOLEAN acceptfolders)
  {
    this->pvt_acceptforeignfolders := acceptfolders;
    this->ApplySettingsToView(FALSE);
  }

  MACRO SetHideViewType(BOOLEAN hideviewtype)
  {
    this->pvt_hideviewtype := hideviewtype;
    this->ApplySettingsToView(FALSE);
  }

  MACRO DoSelectView()
  {
    this->ApplySettingsToView(FALSE);
  }

  MACRO ApplySettingsToView(BOOLEAN ispreinit)
  {
    STRING ARRAY filelistflags := this->acceptfiles ? this->acceptfolders ? DEFAULT STRING ARRAY : ["!isfolder"] : ["isfolder"];

    ^submitaction->enableon[0].checkflags := filelistflags;
    IF(NOT this->acceptrootfolder AND Length(this->roots) = 0) //disable selecting the root, but only if the root is really the publisher root (no limited folder selection)
      INSERT "!isroot" INTO ^submitaction->enableon[0].checkflags AT END;

    ^submitaction->enableon[1].checkflags := filelistflags;
    // The thumbnailview only shows files, we don't have to update the checkflags

    IF(this->acceptfiles OR this->pvt_selectmode = "multiple")
    {
      ^fileviewtype->visible := this->pvt_hideviewtype = FALSE;
      ^filepart->visible := TRUE;

      ^folders->selectmode := "single";
      ^folders->accepttypes := DEFAULT STRING ARRAY;

      OBJECT filecomp := this->__GetCurrentFilelist();
      filecomp->selectmode := this->pvt_selectmode;
      filecomp->accepttypes := this->accepttypedefs;
      filecomp->filteritems := PTR this->FilterItems;

      OBJECT toselect := ^fileviewtype->value="thumbnails" ? ^thumbnailtab : ^filelisttab;
      IF(toselect != ^fileparttabs->selectedtab OR ispreinit) //first run, ensure our components are initialized
      {
        ^fileparttabs->selectedtab := toselect;
        IF(^fileviewtype->value="thumbnails")
        {
          ^filesthumbs->SetThumbnailWHFSType(this->thumbnailwhfstype, this->thumbnailwhfsfield);
        }
        ELSE
        {
          ^fileslist->hidefiles := NOT this->acceptfiles;
          ^fileslist->hidefolders := NOT this->acceptfolders;
        }

        // Ensure the current filelist is listening to the folder tree
        this->__GetCurrentFilelist()->foldertree := ^folders;
      }

      IF (this->acceptfolders)
      {
        IF (NOT this->acceptfiles)
        {
          this->frame->title := this->title != "" ? this->title : this->GetTid(".title-folder");
          ^descr->value := this->description != "" ? this->description : this->GetTid(".descr-folder");
        }
        ELSE
        {
          this->frame->title := this->title!="" ? this->title : this->GetTid(".title-filefolder");
          ^descr->value := this->description!="" ? this->description : this->GetTid(".descr-filefolder");
        }
      }
      ELSE
      {
        this->frame->title := this->title!="" ? this->title : this->GetTid(".title-file");
        ^descr->value := this->description!="" ? this->description : this->GetTid(".descr-file");
      }
    }
    ELSE
    {
      ^fileviewtype->visible := FALSE;
      ^filepart->visible := FALSE;

      ^folders->selectmode := this->pvt_selectmode;
      ^folders->accepttypes := this->accepttypedefs;

      this->frame->title := this->title != "" ? this->title : this->GetTid(".title-folder");
      ^descr->value := this->description != "" ? this->description : this->GetTid(".descr-folder");
    }

    IF (Length(this->roots) > 0)
    {
      ^folders->SetupLimitedRoots(this->roots);
      IF(Length(this->roots)=1)
        INSERT this->roots[0] INTO ^folders->expanded AT END;
    }

    this->PopulateRecentAndFavoriteFolders();

    // Reload folder tree (and file list), so new settings (e.g. accepttypes) are applied correctly to already loaded items
    ^folders->ReloadOverview();

    IF((this->selectmode = "single" AND ^folders->value = 0))
      ^folders->SetvalueIfValid(this->mostrecentfolder);
  }

  MACRO SetAcceptTypes(STRING ARRAY types)
  {
    this->accepttypedefs := types;
    this->ApplySettingsToView(FALSE);
  }

  RECORD ARRAY FUNCTION FilterItems(RECORD ARRAY items)
  {
    IF (NOT this->acceptunpublished)
      items := SELECT * FROM items WHERE IsPublish(published);
    RETURN items;
  }

  MACRO DoOpenInPublisher(OBJECT source)
  {
    INTEGER toopen := source->selection.rowkey; //note: works on both multi and single selects due to record array to record cast
    IF(NOT this->tolliumuser->HasRightOn("system:fs_browse",toopen))
    {
      this->RunMessageBox(".norightsforpublisher");
      RETURN;
    }
    RunPublisherFileManager(this, toopen);
  }
  MACRO DoOpenInPublisher_Folders()
  {
    this->DoOpenInPublisher(^folders);
  }
  MACRO DoOpenInPublisher_FilesList()
  {
    this->DoOpenInPublisher(^fileslist);
  }

  MACRO DoOpenFilelist()
  {
    IF(this->type="thumbnails")
    {
      RECORD selection := this->__GetCurrentFilelist()->selection;
      IF (NOT RecordExists(selection))
        RETURN;
      INTEGER addid;
      IF(NOT selection.isfolder)
        addid := ^folders->selection.rowkey;
      ELSE
        addid := selection.id;

      AddRecentFolder(this->tolliumuser, addid);
      this->tolliumresult := "ok";
      RETURN;
    }
    IF(this->pvt_selectmode = "multiple")
    {
      RECORD ARRAY selection := this->__GetCurrentFilelist()->selection;
      IF(Length(selection)>1 OR NOT selection[0].isfolder)
      {
        AddRecentFolder(this->tolliumuser, ^folders->selection.rowkey);
        this->tolliumresult := "ok";
      }
      ELSE
      {
        ^folders->value := selection[0].rowkey;
      }
    }
    ELSE
    {
      RECORD selection := this->__GetCurrentFilelist()->selection;
      IF(selection.isfolder)
      {
        ^folders->value := selection.rowkey;
        RETURN;
      }
      AddRecentFolder(this->tolliumuser, ^folders->selection.rowkey);
      this->tolliumresult := "ok";
    }
  }

  MACRO DoSubmitFile()
  {
    this->submittedfolder := FALSE;
    AddRecentFolder(this->tolliumuser, ^folders->selection.rowkey);
    this->tolliumresult := "ok";
  }
  MACRO DoSubmitFolder()
  {
    this->submittedfolder := TRUE;
    AddRecentFolder(this->tolliumuser, ^folders->selection.rowkey);
    this->tolliumresult := "ok";
  }

  UPDATE PUBLIC STRING FUNCTION RunModal()
  {
    STRING result := TolliumScreenBase::RunModal();
    IF(result = "ok")
      AddRecentFolder(this->tolliumuser, ^folders->selection.rowkey);
    RETURN result;
  }

  PUBLIC INTEGER FUNCTION RunSelectfileDialog()
  {
    RETURN this->RunModal() = "ok" ? this->value : 0;
  }
  PUBLIC MACRO SetCustomComponents(OBJECT customscreen)
  {
    ^customcomponents->contents := customscreen;
  }
>;
