/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

import * as googleRecaptcha from "@mod-publisher/js/captcha/google-recaptcha";

export function setup() {
  googleRecaptcha.setupGoogleRecaptcha();
}
