﻿<?wh

LOADLIB "wh::files.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/dialogs.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/webharepeers/remoteserverapi.whlib";

LOADLIB "mod::tollium/lib/commondialogs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";

LOADLIB "mod::wrd/lib/address.whlib";
LOADLIB "mod::wrd/lib/api.whlib";
LOADLIB "mod::wrd/lib/database.whlib";
LOADLIB "mod::wrd/lib/internal/tolliumfuncs.whlib";
LOADLIB "mod::wrd/lib/internal/wrdsettings.whlib";
LOADLIB "mod::wrd/lib/internal/metadata/moduledefs.whlib";


RECORD ARRAY FUNCTION GenerateAccountOptions(RECORD ARRAY suitableattributes, STRING fieldtocheck)
{
  RETURN [[ rowkey := 0, title := "" ]]
         CONCAT
         SELECT rowkey := id, title := tag || " (" || title || ")"
           FROM suitableattributes
          WHERE id != 0 AND GetCell(suitableattributes,fieldtocheck);
}


// -----------------------------------------------------------------------------
//
// SchemaManager
//

PUBLIC OBJECTTYPE Main EXTEND TolliumScreenBase
< OBJECT pvt_selected_schema;

  PUBLIC INTEGER value;

  PUBLIC MACRO Init(RECORD data)
  {
    this->ResetList();

    IF(data.curschema = 0)
      data.curschema := this->tolliumuser->GetRegistryKey("wrd.lastopenedaccount",0);
    IF(data.curschema != 0)
      this->schemas->SetValueIfValid([INTEGER(data.curschema)]);

    this->frame->flags.right_schemacreate := this->tolliumuser->HasRight("wrd:createschema");
    this->frame->flags.right_superuser := this->tolliumuser->HasRight("system:sysop");

    IF(IsProductionOrAsIf())
      ^delschema->enableon[0].max := 1;
  }

  MACRO OnDrop(RECORD dragdata, RECORD droptarget, STRING dropaction, STRING droptype)
  {
    this->OnImport(WrapBlob(dragdata.items[0].data, dragdata.items[0].filename));
  }

  MACRO GotSchemaListUpdate(RECORD ARRAY events)
  {
    this->ResetList();
  }

  MACRO ResetList()
  {
    RECORD ARRAY rows :=
        SELECT rowkey := id
             , right_metadata :=  this->tolliumuser->HasRightOn("wrd:metadata", id)
             , *
             , isprotected :=     protected
             , protected :=       protected ? 1 : 0
             , usermgmt :=        usermgmt ? 2 : 0
          FROM ListWRDSchemas([user := this->tolliumuser]);

    this->schemas->rows := rows;
  }

  PUBLIC MACRO DoOpenSchema()
  {
    this->value := this->schemas->value[0];
    this->tolliumuser->SetRegistryKey("wrd.lastopenedaccount", this->value);
    this->tolliumresult := "ok";
  }

  MACRO DoAddSchema()
  {
    OBJECT dialog := this->LoadScreen(".newschema");
    IF (dialog->RunModal() = "ok")
      this->schemas->value := [ INTEGER(dialog->schemaid) ];
  }

  MACRO DoEditSchema()
  {
    this->RunScreen("#schemaprops", DEFAULT RECORD, [ contexts := [ wrdschema := OpenWRDSchemaById(this->schemas->value[0]) ] ]);
    this->ResetList();
  }

  MACRO DoDelSchema()
  {
    RECORD ARRAY tokill := this->schemas->selection;
    FOREVERY(RECORD sch FROM tokill)
    {
      IF(sch.isprotected)
      {
        this->RunSimpleScreen("error", GetTid("wrd:browser.messages.schema_protected_delete", sch.tag));
        RETURN;
      }
    }

    IF(IsProductionOrAsIf()) //NOTE in this case we can only delete one schema at a time
    {
      this->RunScreen("#deleteschemaprod", [ tag := tokill[0].tag ]);
      RETURN;
    }

    STRING msg := Length(tokill) = 1 ? GetTid("wrd:schemamanager.messageboxes.deleteschema", tokill[0].tag)
                                     : GetTid("wrd:schemamanager.messageboxes.deleteschemas", ToString(Length(tokill)));
    IF(this->RunSimpleScreen("verify", msg) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();
    FOREVERY(RECORD kill FROM tokill)
      OpenWRDSchemaById(kill.rowkey)->DeleteSelf();
    work->Finish();
  }

  MACRO OnImport(RECORD infile)
  {
    OBJECT screen := this->LoadScreen(".importschema", [ infile := infile ]);
    screen->RunModal();
    this->schemas->value := [ INTEGER(screen->schemaid) ];
  }

  MACRO DoSynchronize()
  {
    this->RunScreen("#synchronizeschema", DEFAULT RECORD, [ contexts := [ wrdschema := OpenWRDSchemaById(this->schemas->value[0]) ] ]);
  }
>;

// -----------------------------------------------------------------------------
//
// New schema
//

PUBLIC OBJECTTYPE NewSchema EXTEND TolliumScreenBase
<
  PUBLIC INTEGER schemaid;

  MACRO Init(RECORD data)
  {
  }

  PUBLIC MACRO SuggestName(STRING name)
  {
    //ADDME unique-ify name?
    this->name->value := name;
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();
    OBJECT duplicate := OpenWRDSchema(this->name->value);
    IF (ObjectExists(duplicate))
      work->AddError(GetTid("wrd:schemamanager.errors.duplicateschema", this->name->value));

    INTEGER newschemaid;
    IF(NOT work->HasFailed())
    {
      OBJECT wrdschema := CreateWRDSchema(this->name->value, [ description := this->description->value
                                                             ]);
      newschemaid := wrdschema->id;

      IF (NOT this->tolliumuser->HasRightOn("wrd:metadata", newschemaid))
        this->tolliumuser->UpdateGrant("grant", "wrd:metadata", newschemaid, this->tolliumuser, [ allowselfassignment := TRUE ]);
    }

    IF(NOT work->Finish())
      RETURN FALSE;

    this->schemaid := newschemaid;
    RETURN TRUE;
  }
>;

// -----------------------------------------------------------------------------
//
// Update schema properties
//

PUBLIC STATIC OBJECTTYPE SchemaProps EXTEND TolliumScreenBase
<
  MACRO Init(RECORD data)
  {
    IF(NOT this->tolliumuser->HasRightOn("wrd:metadata", this->contexts->wrdschema->id))
      THROW NEW Exception("You do not have metadata rights to this schema");

    ^accounttype->options := [[ rowkey := 0, title := "" ]]
                                  CONCAT
                                  SELECT rowkey := id, title := tag
                                    FROM this->contexts->wrdschema->ListTypes()
                                   WHERE Length(GetSuitableAccountAttributes(this->contexts->wrdschema->GetTypeById(id)))>0
                                ORDER BY tag;

    ^wrdschema->value := SELECT name, description, title, usermgmt, accounttype, protected, accountlogin, accountemail, accountpassword, creationdate
                           FROM wrd.schemas
                          WHERE id = this->contexts->wrdschema->id;

    //Allow usermgmt only to be modified by sysop, and only to be set if whuser_unit exists (as a quick check whether you applied usermgmt.wrdschema.xml)
    ^usermgmt->enabled := this->tolliumuser->HasRight("system:sysop") AND (^usermgmt->value OR ObjectExists(this->contexts->wrdschema->GetType("WHUSER_UNIT")));

    ^validationsettings->value := this->contexts->wrdschema->GetSchemaSetting("wrd:addressverification", [ fallback := DEFAULT RECORD]);
  }

  MACRO OnAccountTypeChange()
  {
    RECORD ARRAY attrs;
    OBJECT curtype := this->contexts->wrdschema->GetTypeById(^accounttype->value);
    IF(ObjectExists(curtype))
      attrs := GetSuitableAccountAttributes(curtype);

    ^accountlogin->options := GenerateAccountOptions(attrs, "allowforlogin");
    ^accountemail->options := GenerateAccountOptions(attrs, "allowforemail");
    ^accountpassword->options := GenerateAccountOptions(attrs, "allowforpassword");
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();

    RECORD schemarec := ^wrdschema->value;

    OBJECT duplicate := OpenWRDSchema(schemarec.name);
    IF (ObjectExists(duplicate) AND duplicate->id != this->contexts->wrdschema->id)
      work->AddError(GetTid("wrd:schemamanager.errors.duplicateschema", schemarec.name));

    IF (^usermgmt->value AND NOT this->tolliumuser->HasRight("system:sysop"))
      work->AddError(GetTid("wrd:schemamanager.errors.needsysoptoenableusermgmt"));

    IF(NOT work->HasFailed())
    {
      SetupWRDAddressVerification(this->contexts->wrdschema, ^validationsettings->value);
      this->contexts->wrdschema->UpdateMetadata(CELL[...^wrdschema->value, DELETE creationdate]);
    }

    RETURN work->Finish();
  }
>;


PUBLIC STATIC OBJECTTYPE ImportSchema EXTEND TolliumScreenBase
<
  PUBLIC INTEGER schemaid;
  RECORD infile;

  MACRO Init(RECORD data)
  {
    this->infile := data.infile;
  }
  BOOLEAN FUNCTION Submit()
  {
    OBJECT screen := CreateProgressDialog(this,
                                          "mod::wrd/lib/internal/tollium-bgtasks.whlib",
                                          "ImportArchive",
                                          [ data := this->infile.data
                                          , userid := this->tolliumuser->authobjectid //FIXME is this safe to transfer this way?  should send auth object
                                          , ignoremissingreferences := ^ignoremissingreferences->value
                                          , renameto := ^renameto->value
                                          ]);
    IF(screen->RunModal() != "ok") //cancelled (FIXME what happens if _we_ get a cancel but the background task completed anyway?)
      RETURN TRUE;

    this->schemaid := screen->result.schemaid;

    SWITCH(screen->result.status)
    {
      CASE "renamed"
      {
        this->RunMessageBox(".import_schema_replaced", screen->result.schemaname, screen->result.renamedschemaname);
      }
      CASE "protected"
      {
        this->RunMessageBox(".import_schema_protected", screen->result.schemaname);
      }
      CASE "cannotreplace"
      {
        this->RunMessageBox(".import_schema_norights", screen->result.schemaname);
      }
      CASE "ok"
      {
        //fine
      }
      DEFAULT
      {
        this->RunMessageBox(".import_schema_errors", Detokenize(screen->result.errors,"\n"));
      }
    }
    RETURN TRUE;
  }
>;


PUBLIC STATIC OBJECTTYPE SynchronizeSchema EXTEND TolliumScreenBase
<
  MACRO Init(RECORD data)
  {
    IF (NOT this->tolliumuser->HasRightOn("wrd:metadata", this->contexts->wrdschema->id)
        OR (this->contexts->wrdschema->usermgmt AND NOT this->tolliumuser->HasRight("system:sysop")))
    {
      this->tolliumresult := "cancel";
      RETURN;
    }

    ^syncpoints->eventmasks := this->contexts->wrdschema->^wrd_settings->GetEventMasks();
    ^syncpoints->AddAction(this->GetTid(".synchronize"), PTR this->DoSynchronize);
  }

  RECORD ARRAY FUNCTION OnGetSyncpoints()
  {
    RETURN
        SELECT rowkey  :=             #syncpoints + 1
             , peer
             , remotewrdschema
             , wrd_settingid
          FROM GetWRDSetting(this->contexts->wrdschema, "syncpoints") AS syncpoints;
  }

  MACRO DoSynchronize()
  {
    RECORD tosync := ^syncpoints->selection;

    RECORD syncoptions := this->RunScreen("exchange.xml#syncoptions", [ type := "import" ]);
    IF (RecordExists(syncoptions))
    {
      OBJECT peer := OpenWebHarePeer(this, tosync.peer, [ scopes := [ "wrd:read" ] ]);
      IF (NOT ObjectExists(peer))
        RETURN;

      RECORD marshallinfo := peer->GetMarshallableInfo();

      RECORD syncdata := CELL
          [ wrdschema :=        this->contexts->wrdschema->id
          , peerinfo :=         peer->GetMarshallableInfo()
          , remotewrdschema :=  tosync.remotewrdschema
          , syncoptions.includetypes
          ];
      OBJECT screen := CreateProgressDialog(this,
                                            "mod::wrd/lib/internal/tollium-bgtasks.whlib",
                                            "SyncSchema",
                                            syncdata);

      LogAuditEvent("wrd:schemasync", CELL
          [ ...syncdata
          , wrdschema := this->contexts->wrdschema->tag
          , DELETE peerinfo
          ]);

      IF(screen->RunModal() = "ok")
      {
        this->RunSimpleScreen("info", this->GetTid(".synccompleted"));
        this->tolliumresult := "ok";
        RETURN;
      }
    }
  }

  MACRO OnEditSyncpoint(RECORD row)
  {
    this->RunScreen("#editschemasyncpoint", row);
  }

  MACRO OnDeleteSyncpoint(RECORD row)
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".verifydisconnectsyncedschema")) != "yes")
      RETURN;

    OBJECT work := this->BeginWork();
    RECORD ARRAY syncpoints := GetWRDSetting(this->contexts->wrdschema, "syncpoints");
    DELETE FROM syncpoints AT row.rowkey - 1;
    SetWRDSetting(this->contexts->wrdschema, "syncpoints", syncpoints);

    work->Finish();
  }
>;

PUBLIC STATIC OBJECTTYPE EditSchemaSyncPoint EXTEND TolliumScreenBase
<
  RECORD initdata;

  MACRO Init(RECORD data)
  {
    this->initdata := data;

    IF (CellExists(this->initdata, "PEER"))
    {
      ^peer->value := this->initdata.peer;
      ^remotewrdschema->value := this->initdata.remotewrdschema;
    }
    ELSE //adding
    {
      ^remotewrdschema->value := this->contexts->wrdschema->tag;
    }
  }

  INTEGER FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();
    IF (^peer->value = "")
      work->AddError(this->GetTid(".pleaseselectapeer"));
    IF (work->HasFailed())
    {
      work->Finish();
      RETURN 0;
    }

    RECORD ARRAY syncpoints := GetWRDSetting(this->contexts->wrdschema, "syncpoints");
    RECORD data := CELL [ peer := ^peer->value, remotewrdschema := ^remotewrdschema->value ];

    INTEGER editpos;
    IF (RecordExists(this->initdata)) //editing
       editpos := SELECT AS INTEGER #syncpoints + 1 FROM syncpoints WHERE syncpoints.wrd_settingid = this->initdata.wrd_settingid;

    IF(editpos > 0)
    {
      syncpoints[editpos-1] := CELL[...syncpoints[editpos-1], ...data];
    }
    ELSE
    {
      INSERT data INTO syncpoints AT END;
      editpos := Length(syncpoints);
    }

    SetWRDSetting(this->contexts->wrdschema, "syncpoints", syncpoints);

    RETURN work->Finish() ? editpos : 0;
  }

  MACRO DoSelectPeer()
  {
    OBJECT selectedpeer := RunConnectRemoteWebHareDialog(this);
    IF(ObjectExists(selectedpeer))
      ^peer->value := selectedpeer->url;
  }
>;

PUBLIC STATIC OBJECTTYPE DeleteSchemaProd EXTEND TolliumScreenBase
<
  STRING tag;

  MACRO Init(RECORD data)
  {
    this->tag := data.tag;
    this->frame->title := this->GetTid(".confirmdeletion", this->tag);
    ^warndeletion->value := GetTid("wrd:schemamanager.messageboxes.deleteschema", this->tag);
    ^deleteschema->title := this->GetTid(".deleteschema", this->tag);

    RECORD schemadef := GetModuleWRDSchemaDefinition(this->tag);
    IF(RecordExists(schemadef) AND schemadef.isexact)
      ^confirmmoduleowneddeletion->label := this->GetTid(".confirmmoduleowneddeletion", schemadef.sourcemodule);
    ELSE
      ^confirmmoduleowneddeletion->visible := FALSE;

    this->OnConfirmChange();
  }

  MACRO OnConfirmChange()
  {
    ^deleteschema->enabled := ^confirmmoduleowneddeletion->value OR ^confirmmoduleowneddeletion->visible = FALSE;
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->BeginUnvalidatedWork();
    OpenWRDSchema(this->tag)->DeleteSelf();
    RETURN work->Finish();
  }
>;
