﻿<?wh
/** @short Process and format names
    @topic wrd/names
*/

LOADLIB "mod::tollium/lib/gettid.whlib";
LOADLIB "mod::wrd/lib/internal/query/generators.whlib";

STRING ARRAY dutchsecondinfixes := [ "DE", "DER", "DEN", "'T" ];
STRING ARRAY dutchinfixes := [ "IN", "VAN" ] CONCAT dutchsecondinfixes;
STRING ARRAY germaninfixes := [ "ZU", "ZUM" ];
//things we know are last names
STRING ARRAY dutchlastnames := ["KLEINE","GROTE","ONDER"];
STRING ARRAY allinfixes := dutchinfixes CONCAT germaninfixes;

STRING FUNCTION FixInitials(STRING part)
{
  part := TrimWhitespace(part);
  IF(UCSearchSubstring(part," ")=-1 AND UCSearchSubstring(part,".")=-1) //no spaces or dots
  {
    STRING fixedpart;
    FOR(INTEGER i:=0;i<UCLength(part);i:=i+1)
      fixedpart := fixedpart || ToUppercase(UCSubstring(part,i,1)) || ".";
    part := fixedpart;
  }
  IF(part != "" AND part NOT LIKE "*.")
    part:=part||".";

  //every first part of an initial must be capitalized (but not Sj to SJ)
  STRING ARRAY initialparts := Tokenize(part,".");
  FOREVERY(STRING partword FROM initialparts)
    initialparts[#partword] := ToUppercase(UCLeft(partword,1)) || ToLowercase(UCSubstring(partword,1));
  part := Detokenize(initialparts,".");

  RETURN part;
}

BOOLEAN FUNCTION IsInitials(STRING part)
{
  IF(part LIKE "*.*") //contains dots
    RETURN TRUE;
  IF(Length(part)=1)
    RETURN TRUE;
  //no vowels?
  BOOLEAN no_vowels := TRUE;
  FOR (INTEGER i := 0; i < UCLength(part) AND no_vowels; i := i + 1)
    IF (UCSearchSubstring("BCDFGHJKLMNPQRSTVWXZ", ToUppercase(UCSubstring(part, i, 1))) = -1)
      no_vowels := FALSE;
  RETURN no_vowels;
}

/** @short Split a full name into wrd name parts
    @long This function does a heuristic split of a given name into the WRD name fiels
    @cell(integer) return.wrd_gender
    @cell(string) return.wrd_firstname
    @cell(string) return.wrd_firstnames
    @cell(string) return.wrd_initials
    @cell(string) return.wrd_infix
    @cell(string) return.wrd_lastname
*/
PUBLIC RECORD FUNCTION SplitWRDName(STRING inname)
{
  STRING ARRAY nametoks;

  //Normalize dashes, make sure "van der x - van der y" is translated to "van der x-van der y"
  WHILE(inname LIKE "* -*")
    inname := Substitute(inname, " -", "-");
  WHILE(inname LIKE "*- *")
    inname := Substitute(inname, "- ", "-");

  //Tokenize the name first
  FOREVERY(STRING tok FROM Tokenize(inname," "))
  {
    tok:=TrimWhitespace(tok);
    IF(tok="")
      CONTINUE;
    INSERT tok INTO nametoks AT END;
  }

  STRING ARRAY firstnametoks;
  STRING ARRAY infixtoks;
  STRING ARRAY lastnametoks;
  STRING ARRAY initialtoks;

  /* Name parser state
     0: parsing first names
     1: started parsing an infix
     2: second part of an infix
     3: last name
     4: parsing initials
  */
  INTEGER state;

  FOR(INTEGER cursor := 0; cursor < Length(nametoks); cursor := cursor + 1)
  {
    STRING curtok := nametoks[cursor];

    IF(state IN [0,4] AND ToUppercase(curtok) IN allinfixes)
    {
      state := 1;
    }
    ELSE IF(state IN [0,4] AND ToUppercase(curtok) IN dutchlastnames)
    {
      state := 3;
    }
    ELSE IF(state = 1 AND ToUppercase(curtok) IN dutchsecondinfixes)
    {
      state := 2;
    }
    ELSE IF (state = 1)
    {
      state := 3;
    }
    ELSE IF(state = 2) //parsing 'der' after 'vna der'
    {
      state := 3;
    }
    ELSE IF(state = 0 AND cursor = Length(nametoks)-1) //too close to last name, switch to last name mode
      state := 3;
    ELSE IF(cursor > 0 AND curtok LIKE "*-*") //any dash must be in the last name
      state := 3;
    ELSE IF(cursor = 0 AND state = 0 AND IsInitials(curtok)) //any part containing a dot is an intial?
      state := 4;
    ELSE IF(state = 4 AND curtok NOT LIKE "*.*") //in initial-state - once we're out of dots, it must be a start of a last name
      state := 3;

    IF(state=0)
      INSERT curtok INTO firstnametoks AT END;
    ELSE IF(state=4)
      INSERT FixInitials(curtok) INTO initialtoks AT END;
    ELSE IF(state IN [1,2])
      INSERT curtok INTO infixtoks AT END;
    ELSE
      INSERT curtok INTO lastnametoks AT END;
  }

  //Initialcaps all firstnames, but capitalize at '-'
  FOREVERY(STRING firstname FROM firstnametoks)
  {
    STRING ARRAY partwords := Tokenize(firstname,"-");
    FOREVERY(STRING partword FROM partwords)
      partwords[#partword] := ToUppercase(UCLeft(partword,1)) || ToLowercase(UCSubstring(partword,1));
    firstname := Detokenize(partwords,"-");

    INSERT UCLeft(firstname,1) || "." INTO initialtoks AT END;
    firstnametoks[#firstname] := firstname;
  }

  //Lowercase all infixes
  FOREVERY(STRING infix FROM infixtoks)
  {
    infix := ToLowercase(infix);
    infixtoks[#infix] := infix;
  }

  //Fix lastnames, they require some more complex caps rules, each component separated with '-' needs re-capsing
  STRING ARRAY lastnameparts := Tokenize(Detokenize(lastnametoks," "),"-");
  FOREVERY(STRING part FROM lastnameparts)
  {
    //capitalize all words, except the standard infixes
    STRING ARRAY partwords := Tokenize(part," ");
    FOREVERY(STRING partword FROM partwords)
      IF(ToUppercase(partword) IN dutchinfixes CONCAT dutchsecondinfixes)
        partwords[#partword] := ToLowercase(partword);
      ELSE
        partwords[#partword] := ToUppercase(UCLeft(partword,1)) || ToLowercase(UCSubstring(partword,1));

    lastnameparts[#part] := Detokenize(partwords," ");
  }

  RECORD retval := [ wrd_gender := 0
                   , wrd_firstname := Length(firstnametoks) > 0 ? firstnametoks[0] : ""
                   , wrd_firstnames := Detokenize(firstnametoks, " ")
                   , wrd_initials := Detokenize(initialtoks,"")
                   , wrd_infix := Detokenize(infixtoks," ")
                   , wrd_lastname := Detokenize(lastnameparts,"-")
                   ];
  RETURN retval;
}


/** @short Generate a salutation, as specified by WRD
    @param field Salutation type: one of "WRD_FORMALNAME", "WRD_FULLNAME", "WRD_SALUTE_FORMAL", "WRD_ADDRESS_FORMAL"
    @cell(string) options.languange Languagecode: 'en', 'nl' or de'. if unset, falls back to current tid language
    @return The requested name or salutation, if possible to form */
PUBLIC STRING FUNCTION FormatWRDNameField(STRING field, RECORD data, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions([ language := "" ], options);
  IF(options.language = "")
    options.language := GetTidLanguage();

  RETURN GetGenerator(field, options.language)(data);
}

