﻿<?wh
/** @topic wrdauth/saml */

/** Base type for SAML SP configuration
*/
PUBLIC STATIC OBJECTTYPE SAMLSPConfigBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// WRD Auth plugin
  OBJECT plugin;

  /// Configuration data
  RECORD config;

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(OBJECT plugin, RECORD configdata)
  {
    this->plugin := plugin;
    this->config := configdata;
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  /** Process an incoming SAML AuthenticationStatement
      @param statement Decoded SAML statement
      @return Processing result
      @cell return.entityid Entity ID
      @cell return.name Name of the user
      @cell return.code 'ok', 'unknownlogin', 'error'
  */
  PUBLIC RECORD FUNCTION ProcessAuthenticationStatement(RECORD statement)
  {
    THROW NEW Exception("This function must be overridden");
  }
>;

/** Base type for SAML IDP configuration
*/
PUBLIC STATIC OBJECTTYPE SAMLIDPConfigBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  OBJECT plugin;

  /// Configuration data
  RECORD config;

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(OBJECT plugin, RECORD configdata)
  {
    this->plugin := plugin;
    this->config := configdata;
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  /** Generates the data for an authentication statement for the currently logged in user
      @return
      @cell(record) return.subject
      @cell(record) return.subject.nameid
      @cell(string) return.subject.nameid.format
      @cell(string) return.subject.nameid.value
      @cell(record array) return.attributes
      @cell(string) return.attributes.name
      @cell(string) return.attributes.value
  */
  PUBLIC RECORD FUNCTION GenerateAuthenticationStatement()
  {
    THROW NEW Exception("This function must be overridden");
  }

  /** Generates the data for an authentication statement for the currently logged in user
      @param requestdata Data describing the request
      @cell(object ../internal/auth/saml.whlib#SAMLConnectedServiceProvider) requestdata.sp Receiving service provider
      @cell(string) requestdata.requestid SAML request id, empty for IdP-initiated login
      @cell(record) requestdata.endpoint Endpoint this statement is sent to #includecelldef ../internal/auth/saml.whlib#SAMLConnectedServiceProvider::GetEndpointOfTypeByIdx.return
      @cell(datetime) requestdata.now Signature time of the authentication response
      @return Authentication statement data
      @cell(record) return.subject
      @cell(record) return.subject.nameid
      @cell(string) return.subject.nameid.format
      @cell(string) return.subject.nameid.value
      @cell(record array) return.attributes
      @cell(string) return.attributes.name
      @cell(string) return.attributes.value
  */
  PUBLIC RECORD FUNCTION GenerateAuthenticationStatement2(RECORD requestdata)
  {
    RETURN this->GenerateAuthenticationStatement();
  }

>;
