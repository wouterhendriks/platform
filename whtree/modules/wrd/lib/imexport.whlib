<?wh
/** @short Import/export of WRD schemas and metadata management
    @topic wrd/imexport
*/

LOADLIB "wh::xml/dom.whlib";
LOADLIB "wh::xml/xsd.whlib";

LOADLIB "mod::system/lib/resources.whlib";

LOADLIB "mod::wrd/lib/api.whlib";
LOADLIB "mod::wrd/lib/internal/import.whlib";
LOADLIB "mod::wrd/lib/internal/exchange.whlib";
LOADLIB "mod::wrd/lib/internal/support.whlib";
LOADLIB "mod::wrd/lib/internal/schemadef.whlib";
LOADLIB "mod::wrd/lib/database.whlib";


PUBLIC RECORD ARRAY FUNCTION MakeGuidToWRDIdMapping(INTEGER schemaid, STRING ARRAY inguids)
{
  FOREVERY(STRING guid FROM inguids)
    inguids[#guid] := DecodeWRDGUID(guid);

  RETURN SELECT inval := EncodeWRDGuid(entities.guid), outval := entities.id
           FROM wrd.entities, wrd.types
          WHERE entities.guid IN inguids
                AND entities.type = types.id
                AND types.wrd_schema = schemaid;
}

PUBLIC RECORD ARRAY FUNCTION MakeWRDIdToGuidMapping(INTEGER ARRAY inids)
{
  RETURN SELECT inval := id, outval := EncodeWRDGuid(guid) FROM wrd.entities WHERE entities.id IN inids;
}

/** Get a XML export of this schema */
PUBLIC OBJECT FUNCTION ExportWRDSchemaDefinition(OBJECT wrdschema, OBJECT doc)
{
  OBJECT root := doc->CreateElementNS(ns_schemadef, "schemadefinition");
  CreateWRDSchemaDefinitionNodes(wrdschema, root);
  RETURN root;
}

PUBLIC BLOB FUNCTION CreateWRDSchemaDefinitionFile(OBJECT wrdschema)
{
  OBJECT newdoc := NEW XMLDomImplementation->CreateDocument(ns_schemadef, "schemadefinition", DEFAULT OBJECT);
  CreateWRDSchemaDefinitionNodes(wrdschema, newdoc->documentelement);
  newdoc->NormalizeDocument();

  //Selfcheck
  OBJECT schemadefxsd := MakeXMLSchema(GetWebHareResource("mod::wrd/data/schemadefinition.xsd"));
  IF (NOT ObjectExists(schemadefxsd))
    THROW NEW Exception("Unable to use schemadefinition.xsd, it has errors");

  RECORD ARRAY errors := schemadefxsd->ValidateDocument(newdoc);
  IF(Length(errors)>0)
  {
    THROW NEW Exception(`Errors in generated WRD schemadefinition (${errors[0].message}`);
  }

  RETURN newdoc->GetDocumentBlob(TRUE);
}

PUBLIC OBJECT FUNCTION MakeWRDSchemaExporter(OBJECT wrdschema)
{
  IF(NOT ObjectExists(wrdschema))
    THROW NEW Exception(`Missing wrdschema to export`);
  OBJECT exporter := NEW WRDImExport(wrdschema, DEFAULT RECORD);
  exporter->usemarshalformat:=TRUE;
  RETURN exporter;
}

PUBLIC OBJECT FUNCTION MakeWRDDataImporter(OBJECT wrdtype)
{
  RETURN NEW WRDImporter(wrdtype);
}

PUBLIC OBJECT FUNCTION CreateWRDSchemaFromImport(BLOB importarchive, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  OBJECT importer := NEW WRDImExport(DEFAULT OBJECT, options);
  importer->RunImport(importarchive);

  INTEGER schemaid := importer->GetImportedSchemaID();
  IF(schemaid=0)
    THROW NEW Exception("WRD Import did not contain any schema");

  RETURN OpenWRDSchemaById(schemaid);
}
