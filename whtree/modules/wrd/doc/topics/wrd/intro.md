WRD offers an 'entity, attribute, value' database with support for relations between entities and lifetimes for the entities.

WRD offers specialized types for storing personal, name and contact information, but a schema can be used to store any
type of data.

WRD is split into separate schemas which all have their own structure and metadata.
