﻿<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::promise.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/services.whlib";


/** Handles exclusive opening of tollium screens (or locking for actions), with support for
    stealing access by other users
*/
PUBLIC STATIC OBJECTTYPE ExclusivityHandlerBase
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /** @type(record) User data
      @cell entityid User entity id
      @cell login User login name
      @cell realname User real name
  */
  RECORD userdata;

  /// Identifier (for encoding multiple instances, eg for edited entities)
  STRING identifier;

  /// Port (name dependent on identifier, so 1-port-per-process policy handles exclusivity)
  OBJECT port;

  /// List of connected comm links
  OBJECT ARRAY links;

  /// List of close requests
  RECORD ARRAY pvt_requests;

  /// Counter for close request ids
  INTEGER requestid_counter;

  /// Opened 'can we close?' dialog
  BOOLEAN askclosedialog_active;

  /** Options
      @cell(function ptr) onbeforesteal Callback called just before the screen is stolen
      @cell(function ptr) onsteal Callback called after the screen has been stolen
      @cell(record) subscreensonly Close only subscreens of the opening screen
  */
  RECORD options;

  /** Steal exitmessage
      @cell(boolean) showmessage Message to show
      @cell(record) closedby Identity data for the user that stole the screen (login, realname)
      @cell(string) closedby.login Login name of user
      @cell(string) closedby.realname Real name of user
  */
  RECORD exitmessage;

  /// UID for the current lock
  STRING pvt_lockuid;

  // ---------------------------------------------------------------------------
  //
  // Public properties
  //

  /** List of current takeover requests
      @cell id Id of request
      @cell login Login name of takeover user
      @cell realname Real name of takeover user
      @cell date Date of takeover request
      @cell deadline Time the requester will take over automatically
  */
  PUBLIC PROPERTY requests(GetRequests, -);

  /** @type(string) UID for the current lock
  */
  PUBLIC PROPERTY lockuid(pvt_lockuid, -);

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  /** Constructs a new ExclusivityHandler object
    @param identifier Identifies the unique resource to protect
    @param entityid Entity id of user locking the resource
    @param options Options
    @cell(function ptr) options.onbeforesteal Callback called just before the screen is stolen
    @cell(function ptr) options.onsteal Callback called after the screen has been stolen
    @cell(record) options.subscreensonly Close only subscreens of the opening screen
    @cell(string) options.login Login name of the user
    @cell(string) options.realname Real name of the user
  */
  MACRO NEW(STRING identifier, INTEGER entityid, RECORD options)
  {
    this->identifier := identifier;
    this->options :=  ValidateOptions(
        [ onstolen :=         DEFAULT FUNCTION PTR
        , onbeforesteal :=    DEFAULT FUNCTION PTR
        , subscreensonly :=   FALSE
        , login :=            ""
        , realname :=         ""
        ], options);

    this->userdata := CELL
        [ this->options.login
        , this->options.realname
        , entityid
        ];
  }

  // ---------------------------------------------------------------------------
  //
  // Getters & setters
  //

  RECORD ARRAY FUNCTION GetRequests()
  {
    RETURN SELECT *, DELETE link, DELETE msgid FROM this->pvt_requests;
  }

  // ---------------------------------------------------------------------------
  //
  // Callbacks
  //

  /// Got an incoming comm link
  MACRO OnLink()
  {
    OBJECT link := this->port->Accept(DEFAULT DATETIME);
    IF (ObjectExists(link))
    {
      link->userdata :=
          [ cb :=         RegisterHandleReadCallback(link->handle, PTR this->OnLinkMessage(link))
          , requestid :=  0
          ];

      INSERT link INTO this->links AT END;
    }
  }

  /** Incoming comm link is signalled
      Warning: this can be called from WITHIN the signaltakeover dialog!!!
      @param link New link
  */
  MACRO OnLinkMessage(OBJECT link)
  {
    RECORD rec := link->ReceiveMessage(DEFAULT DATETIME);
    IF (rec.status = "timeout")
      RETURN; // Spurious wakeup, ignore

    // Other link is gone.
    IF (rec.status = "gone")
    {
      this->CloseLink(link);
      RETURN;
    }

    SWITCH (rec.msg.type)
    {
      CASE "identify"
      {
        // Identification request
        link->SendReply(CELL
            [ this->userdata.login
            , this->userdata.realname
            , this->userdata.entityid
            , groupid :=  GetCurrentGroupId()
            ], rec.msgid);
      }
      CASE "verifylockuid"
      {
        // Identification request
        link->SendReply(CELL
            [ success :=    rec.msg.lockuid = this->pvt_lockuid
            ], rec.msgid);
      }
      CASE "close"
      {
        // Already winding down for a forced close? Ignore next close requests
        IF (RecordExists(this->exitmessage))
          RETURN;

        // Set the exit message - this will prevent closes from other sources
        this->exitmessage := [ showmessage := TRUE, closedby := rec.msg ];

        IF (CellExists(this->options, "ONBEFORESTEAL") AND this->options.onbeforesteal != DEFAULT FUNCTION PTR)
          this->options.onbeforesteal();

        // forced close by other side
        link->SendReply([ success := TRUE ], rec.msgid);

        // Give the other side an (little) advantage to the other links
        this->ClosePort();

        // Close the handler, show messages and do callback
        this->Close();
      }
      CASE "requestclose"
      {
        // Only one pending request per link
        IF (link->userdata.requestid != 0)
        {
          link->SendExceptionReply(NEW Exception("Already a close request pending on this link"));
          this->CloseLink(link);
          RETURN;
        }

        // Allocate a request id
        this->requestid_counter := this->requestid_counter + 1;
        link->userdata.requestid := this->requestid_counter;

        // Register the request
        INSERT CELL
            [ id :=       this->requestid_counter
            , date :=     GetCurrentDateTime()
            , rec.msg.login
            , rec.msg.realname
            , rec.msg.deadline
            , rec.msgid
            , link :=     link
            ] INTO this->pvt_requests AT END;

        // Open the ask dialog if not open yet (if already open, we called from within its modal loop!)
        IF (NOT this->askclosedialog_active)
        {
          // Run the askclosedialog
          this->askclosedialog_active := TRUE;
          STRING dres := this->DisplaySignalTakeoverDialog();
          this->askclosedialog_active := FALSE;

          // Did the user consent to the takeover (say ok, or let it timeout)
          BOOLEAN close_screen := dres IN [ "ok", "timeout" ];

          // Yes: give a positive answer to the first request (if present)
          IF (close_screen AND LENGTH(this->pvt_requests) != 0)
          {
            IF (CellExists(this->options, "ONBEFORESTEAL") AND this->options.onbeforesteal != DEFAULT FUNCTION PTR)
              this->options.onbeforesteal();

            this->pvt_requests[0].link->SendReply([ success := TRUE ], this->pvt_requests[0].msgid);
            this->CloseLink(this->pvt_requests[0].link);

            // Give the other side an (little) advantage to the other links
            Sleep(100);
          }

          // Deny all other requests
          FOREVERY (RECORD req FROM this->pvt_requests)
            req.link->SendReply([ success := FALSE ], req.msgid);

          // Close the screen anyway (even if no requests are pending. We want to be predictable, not have users
          // expect that the screen will stay open anyway when they have clicked to close)
          IF (close_screen)
          {
            this->exitmessage := [ showmessage := dres = "timeout", closedby := rec.msg ];

            // Relinquish exclusivity
            this->Close();
          }
        }
        ELSE // Got another request, update the signaltakeover dialog
        {
          this->UpdateSignalTakeoverDialog();
        }
      }
    }
  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  /// Close the exclusivity port
  MACRO ClosePort()
  {
    IF (ObjectExists(this->port))
    {
      this->pvt_lockuid := "";
      UnregisterCallback(this->port->userdata.cb);
      this->port->Close();
      this->port := DEFAULT OBJECT;
    }
  }

  /** Close a comm link
      @param link Link to close
  */
  MACRO CloseLink(OBJECT link)
  {
    // Cancel requests from this link
    DELETE
      FROM this->pvt_requests
     WHERE id = link->userdata.requestid;

    // Remove callback, close link & remove from links list
    UnregisterCallback(link->userdata.cb);
    link->Close();
    INTEGER pos := SearchElement(this->links, link);
    IF (pos != -1)
      DELETE FROM this->links AT pos;

    // If the dialog is open, update its state
    IF (this->askclosedialog_active)
      this->UpdateSignalTakeoverDialog();
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  /** Starts locking
      @return TRUE if lock was succesfull
  */
  PUBLIC BOOLEAN FUNCTION Start()
  {
    STRING portname := "tollium:exclusive_access." || this->identifier;

    // Wait loop
    WHILE (TRUE)
    {
      OBJECT lock := OpenLockManager()->LockMutex(portname);
      TRY
      {
        // Try to register our port. Only one port with the same name can be active in a process (webserver).
        // Throws upon failure
        this->port := CreateIPCPort(portname);
        lock->Close();

        // Listen to incoming takeover requests
        this->port->userdata := [ cb := RegisterHandleReadCallback(this->port->handle, PTR this->OnLink) ];

        this->pvt_lockuid := GenerateUFS128BitId();
        RETURN TRUE;
      }
      CATCH (OBJECT e)
      {
        lock->Close();

        // Port already existed. Connect to it.
        OBJECT link := ConnectToIPCPort(portname);
        IF (NOT ObjectExists(link)) // Might have gone already.
          CONTINUE;

        TRY
        {
          // Ask for the identity of the other side (ADDME: timeout for unresponse processes?)
          // Do an async call to make sure an exclusivity handler in the same job doesn't deadlock the system
          RECORD res := WaitForPromise(link->AsyncDoRequest([ type := "identify" ]));

          // If the request failed, the other side is probably gone. Retry getting the port.
          IF (res.status = "ok")
          {
            IF (res.msg.groupid = GetCurrentGroupId())
              THROW NEW Exception("Recursive exclusive access lock detected!");

            // Is this a tab of the current user?
            IF (res.msg.entityid = this->userdata.entityid AND res.msg.entityid > 0)
            {
              // Ask what to do
              STRING dres := this->DisplayStealDialog(CELL[ res.msg, link, isown := TRUE ]);
/*
              OBJECT dialog := this->screen->tolliumcontroller->LoadScreen("tollium:exclusive.stealdialog",
                  [ msg := res.msg
                  , link := link
                  , isown := TRUE
                  , title := this->screen->frame->title
                  ]);

              STRING dres := dialog->RunModal();
*/
              // Other side disappeared? Retry loop
              IF (dres = "linkbroken")
                CONTINUE;

              // Got a license to kill from the user?
              IF (dres = "ok")
              {
                link->DoRequest(
                    [ type := "close"
                    , login :=    this->userdata.login
                    , realname := this->userdata.realname
                    ]);
                CONTINUE;
              }
            }
            ELSE
            {
              // Ask if user wants to take over dialog from other user
              STRING dres := this->DisplayStealDialog(CELL[ res.msg, link, isown := FALSE ]);
/*
              OBJECT dialog := this->screen->tolliumcontroller->LoadScreen("tollium:exclusive.stealdialog",
                  [ msg := res.msg
                  , link := link
                  , isown := FALSE
                  , title := this->screen->frame->title
                  ]);

              STRING dres := dialog->RunModal();
*/
              IF (dres = "ok")
              {
                // Yes, user wants to. Run screen waitforreply, it will do the request
                dres := this->DisplayRunTakeoverRequest(CELL[ res.msg, link ]);
/*
                dialog := this->screen->tolliumcontroller->LoadScreen("tollium:exclusive.runtakeoverrequest",
                    [ msg := res.msg
                    , link := link
                    , title := this->screen->frame->title
                    ]);
                dres := dialog->RunModal();
*/
                IF (dres = "ok")
                  CONTINUE;
                ELSE IF (dres = "denied")
                {
                  this->DisplayTakeoverDenied();
                  RETURN FALSE;
                }
              }
              ELSE IF (dres = "linkbroken")
                CONTINUE;
            }

            // Cleanup resources
            this->Close();
            RETURN FALSE;
          }
        }
        FINALLY
          link->Close();
      }
    }
    RETURN FALSE;
  }

  /** Checks if the resource is still owned. Throws OperationCancelledException if not
  */
  PUBLIC MACRO Check()
  {
    IF (RecordExists(this->exitmessage))
      THROW NEW OperationCancelledException();
  }

  /// Close the exclusivity handler (release lock)
  PUBLIC MACRO Close()
  {
    this->ClosePort();

    // If forced closing, give our closer a bit of head-start to open its own port
    IF (RecordExists(this->exitmessage))
      Sleep(100);
    ELSE IF (IsValueSet(this->links))
    {
      // Give the first waiter a head-start
      this->CloseLink(this->links[0]);
      Sleep(100);
    }

    WHILE (LENGTH(this->links) != 0)
      this->CloseLink(this->links[0]);

    IF (RecordExists(this->exitmessage))
    {
      this->DisplayStolenMessage(CELL[ this->exitmessage.showmessage, this->exitmessage.closedby ]);

      IF (CellExists(this->options, "ONSTOLEN") AND this->options.onstolen != DEFAULT FUNCTION PTR)
        this->options.onstolen();

      this->exitmessage := DEFAULT RECORD;
    }
  }

  /** Get a lock token, which can be used to get a mutex
      @return Lock token
  */
  PUBLIC STRING FUNCTION GetLockToken()
  {
    IF (this->pvt_lockuid = "")
      THROW NEW Exception(`Lock is currently not taken`);

    RETURN EncryptForThisServer("tollium:exclusivity", CELL[ i := this->identifier, t := this->pvt_lockuid ]);
  }

  // ---------------------------------------------------------------------------
  //
  // Callbacks to override
  //

  /** Display a dialog that a resource is already locked
      @param data Message data
      @cell data.msg Owner of the resource
      @cell data.msg.login Login name
      @cell data.msg.realname Real name
      @cell data.msg.entityid Entity ID
      @cell data.msg.groupid GroupId of the job with the lock
      @cell data.link Link to the exclusivityhandler of the owner
      @cell data.isown Whether the current user is the user who owns the lock
      @return 'linkbroken', 'ok' (to steal from other user), 'cancel'/''
  */
  STRING FUNCTION DisplayStealDialog(RECORD data)
  {
    THROW NEW Exception(`Display steal dialog`);
  }

  /** Display a dialog that shows the progress of a takeover request
      @param data Message data
      @cell data.msg Owner of the resource
      @cell data.msg.login Login name
      @cell data.msg.realname Real name
      @cell data.msg.entityid Entity ID
      @cell data.msg.groupid GroupId of the job with the lock
      @cell data.link Link to the exclusivityhandler of the owner
      @return 'ok', 'denied', 'cancel'/''
  */
  STRING FUNCTION DisplayRunTakeoverRequest(RECORD data)
  {
    THROW NEW Exception(`Display run takeoverrequest screen`);
  }

  /** Display a dialog that the request to take over the resource has been denied
  */
  MACRO DisplayTakeoverDenied()
  {
    THROW NEW Exception(`Display takeover denied screen`);
  }

  /** Display a dialog that a user wants to take over the dialog
      @return 'ok', 'cancel'/'', 'timeout'
  */
  STRING FUNCTION DisplaySignalTakeoverDialog()
  {
    THROW NEW Exception(`Display steal dialog (resource is locked by other user`);
  }

  /// Called when lock info changes during showing of signal takeover dialog
  MACRO UpdateSignalTakeoverDialog()
  {
  }

  /** Display that the resource got stolen (and close the screens)
      @param exitmessage Exit message data
      @cell exitmessage.showmessage Whether a message should be displayed
      @cell exitmessage.closedby Who took the resource
      @cell exitmessage.closedby.login Login name
      @cell exitmessage.closedby.realname Real name
  */
  MACRO DisplayStolenMessage(RECORD exitmessage)
  {
    THROW NEW Exception(`Display resource stolen screen`);
  }
>;

PUBLIC STATIC OBJECTTYPE TolliumExclusivityHandler EXTEND ExclusivityHandlerBase
<
  OBJECT screen;
  OBJECT askclosedialog;

  MACRO NEW(OBJECT screen, STRING identifier, RECORD options)
  : ExclusivityHandlerBase(identifier, screen->tolliumuser->entityid, CELL
      [ ...options
      , screen->tolliumuser->login
      , screen->tolliumuser->realname
      ])
  {
    this->screen := screen;
  }

  UPDATE STRING FUNCTION DisplaySignalTakeoverDialog()
  {
    this->askclosedialog := this->screen->tolliumcontroller->LoadScreen("tollium:exclusive.signaltakeover", CELL[ control := this ]);
    STRING dres := this->askclosedialog->RunModal();
    this->askclosedialog := DEFAULT OBJECT;
    RETURN dres;
  }

  UPDATE MACRO UpdateSignalTakeoverDialog()
  {
    // If the dialog is open, update its state
    IF (ObjectExists(this->askclosedialog))
      this->askclosedialog->GotUpdate();
  }

  UPDATE STRING FUNCTION DisplayStealDialog(RECORD data)
  {
    OBJECT dialog := this->screen->tolliumcontroller->LoadScreen("tollium:exclusive.stealdialog", CELL
        [ data.msg
        , data.link
        , data.isown
        , title := this->screen->frame->title
        ]);

    RETURN dialog->RunModal();
  }

  UPDATE STRING FUNCTION DisplayRunTakeoverRequest(RECORD data)
  {
    OBJECT dialog := this->screen->tolliumcontroller->LoadScreen("tollium:exclusive.runtakeoverrequest", CELL
        [ data.msg
        , data.link
        , title := this->screen->frame->title
        ]);
    RETURN dialog->RunModal();
  }

  UPDATE MACRO DisplayTakeoverDenied()
  {
    this->screen->tolliumcontroller->RunMessageBox("tollium:exclusive.takeoverdenied", this->screen->frame->title);
  }

  UPDATE MACRO DisplayStolenMessage(RECORD exitmessage)
  {
    IF (exitmessage.showmessage)
      this->screen->RunMessageBox("tollium:exclusive.dialogstolen", exitmessage.closedby.login, exitmessage.closedby.realname);

    // Close the relevant screens
    IF (this->options.subscreensonly)
      this->screen->tolliumcontroller->MarkChildrenScreensAsStolen(this->screen);
    ELSE
      this->screen->tolliumcontroller->MarkScreenAsStolen(this->screen);

  }

  UPDATE PUBLIC BOOLEAN FUNCTION Start()
  {
    BOOLEAN result := ExclusivityHandlerBase::Start();

    // Asked for lock, but didn't get it
    IF (NOT result AND NOT this->options.subscreensonly)
      this->screen->tolliumcontroller->MarkScreenAsStolen(this->screen);

    RETURN result;
  }

>;

/** Returns TRUE when a specific tag has a tollium screen exclusive lock. Only
    works within the webserver process!
    @param tag Exclusive lock tag
    @return TRUE if an exclusive lock is present for that tag
    @public
    @loadlib mod::tollium/lib/applications.whlib
    @topic tollium/appdev
*/
PUBLIC BOOLEAN FUNCTION TestExclusiveLocked(RECORD tag)
{
  STRING identifier := EncodeUFS(GetSHA1Hash(EncodeHSON(tag)));
  OBJECT link := ConnectToIPCPort("tollium:exclusive_access." || identifier);
  IF (ObjectExists(link))
  {
    link->Close();
    RETURN TRUE;
  }

  RETURN FALSE;
}

/** Status object returned by [GetBulkExclusiveLock](#GetBulkExclusiveLock)
    @public
    @topic tollium/appdev
*/
STATIC OBJECTTYPE BulkExclusiveKeeper
< // ---------------------------------------------------------------------------
  //
  // Variables
  //

  /// Options
  RECORD _options;

  /// Cancel token source used to stop all async functions
  OBJECT canceltokensource;

  /** status per tag
      @cell tag Tag
      @cell status @includecelldef #pertagstatus.status
      @cell id @includecelldef #pertagstatus.id
  */
  RECORD ARRAY _status;

  /// Time that lock request was started
  DATETIME _requesttime;

  /// Deadline for close requests
  DATETIME _deadline;

  /// Deferred promise for lock waits
  RECORD _defer;

  /// Deferred promise for close waits
  RECORD _closedefer;

  // ---------------------------------------------------------------------------
  //
  // Properties
  //

  /** @type(string) Wait mode:
      - 'try': Try to lock, give back identifications of locksed items
      - 'requestclose': Ask for locks, allow users to deny
      - 'forceclose': Close screen locks (won't close bulk exclusive locks)
  */
  PUBLIC PROPERTY mode(this->_options.mode, -);

  /** @type(string) Status
      - "none": None of the locks succeeded
      - "partial": Not all locks succeeded
      - "ok": all locks are taken
      - "closed": Closed
  */
  PUBLIC PROPERTY status(GetStatus, -);

  /** Status per tag
      @cell(record) tag Lock tag
      @cell(string) status Lock status
          - "acquiring": Acquiring lock
          - "identified": Acquiring lock, got identification about the party holding the lock
          - "denied": Lock acquire denied by current holder
          - "ok": Lock acquired
          - "closed": keeper closed
      @cell(record) id Identification (only when status is "identified" or "denied")
  */
  PUBLIC PROPERTY pertagstatus(GetPerTagStatus, -);

  /** @type(datetime) Deadline for releasing the lock (for mode "requestclose")
  */
  PUBLIC PROPERTY deadline(_deadline, -);

  /** @type(datetime)
  */
  PUBLIC PROPERTY requesttime(_requesttime, -);

  // ---------------------------------------------------------------------------
  //
  // Constructor
  //

  MACRO NEW(RECORD ARRAY tags, RECORD options)
  {
    this->canceltokensource := NEW CancellationTokenSource;
    this->_options := options;
    this->_status :=
        SELECT identifier
             , tag
             , identity :=      DEFAULT RECORD
             , orgdata
             , status := "acquiring" //initial status
          FROM tags;

    INTEGER waitsecs := options.takeovertime ?? ReadRegistryKey("system.backend.exclusiveaccess.takeovertime");
    this->_defer := CreateDeferredPromise();
    this->_closedefer := CreateDeferredPromise();
    this->_requesttime := GetCurrentDateTime();
    this->_deadline := AddTimeToDate(waitsecs * 1000, this->_requesttime);

    FOREVERY (RECORD rec FROM this->_status)
      this->TryGetPort(#rec);
  }

  // ---------------------------------------------------------------------------
  //
  // Getters / setters
  //

  RECORD ARRAY FUNCTION GetPerTagStatus()
  {
    RETURN
        SELECT ...orgdata
             , status
             , identity
          FROM this->_status;
  }

  STRING FUNCTION GetStatus()
  {
    STRING retval := "none";
    FOREVERY (RECORD rec FROM this->_status)
      IF (rec.status != "ok")
        RETURN retval;
      ELSE
        retval := "partial";
    RETURN "locked";
  }

  // ---------------------------------------------------------------------------
  //
  // Helper functions
  //

  MACRO UpdateStatus(INTEGER pos, RECORD updates)
  {
    this->_status[pos] := CELL[ ...this->_status[pos], ...updates ];
    this->ProcessWaits();

  }

  MACRO ProcessWaits()
  {
    BOOLEAN allfinished := TRUE, allidentified := TRUE, alllocked := TRUE, allclosed := TRUE;
    FOREVERY (RECORD rec FROM this->_status)
    {
      IF (rec.status = "acquiring")
      {
        allfinished := FALSE;
        allidentified := FALSE;
      }
      IF (rec.status = "identified")
        allfinished := FALSE;
      IF (rec.status != "ok")
        alllocked := FALSE;
      BOOLEAN closed := rec.status = "closed" OR (this->mode = "try" AND rec.status = "identified");
      IF (NOT closed)
        allclosed := FALSE;
    }

    IF (allfinished OR (this->mode = "try" AND allidentified))
      this->_defer.resolve(alllocked);
    IF (allclosed)
      this->_closedefer.resolve(TRUE);
  }

  ASYNC MACRO HandlePort(INTEGER pos, OBJECT port)
  {
    (AWAIT GetAsyncControl())->autolinkcancel := TRUE;
    OBJECT canceltoken := (AWAIT GetAsyncControl())->canceltoken;
    TRY
    {
      WHILE (TRUE)
      {
        OBJECT link := AWAIT port->AsyncAccept(MAX_DATETIME);
        this->HandlePortLink(link)->LinkToken(canceltoken);
      }
    }
    CATCH (OBJECT< OperationCancelledException > e)
    {
      this->UpdateStatus(pos, [ status := "closed" ]);
    }
    FINALLY
      port->Close();
  }

  ASYNC MACRO HandlePortLink(OBJECT link)
  {
    TRY
    {
      (AWAIT GetAsyncControl())->autolinkcancel := TRUE;

      WHILE (TRUE)
      {
        RECORD rec := AWAIT link->AsyncReceiveMessage(MAX_DATETIME);
        IF (rec.status != "ok")
          RETURN;

        SWITCH (rec.msg.type)
        {
          CASE "identify"
          {
            // Identification request
            link->SendReply(
                [ login :=    this->_options.user->login
                , realname := this->_options.user->realname
                , entityid := this->_options.user->entityid
                , groupid :=  GetCurrentGroupId()
                ], rec.msgid);
          }
          CASE "verifyuid"
          {
            link->SendReply(
                [ success :=  FALSE
                ], rec.msgid);

          }
          CASE "close", "requestclose"
          {
            // Ignoring, assume that a bulk operation is speedy
          }
        }
      }
    }
    CATCH (OBJECT< OperationCancelledException > e)
    {
    }
    FINALLY
      link->Close();
  }

  ASYNC MACRO HandleExtLink(OBJECT link, INTEGER pos)
  {
    TRY
    {
      (AWAIT GetAsyncControl())->autolinkcancel := TRUE;
      RECORD rec := AWAIT link->AsyncDoRequest([ type := "identify" ]);

      this->UpdateStatus(pos, [ status := "identified", id := rec.msg ]);
      IF (this->mode = "try")
        RETURN;

      rec := AWAIT link->AsyncDoRequest(CELL
          [ type :=     this->mode = "requestclose" ? "requestclose" : "close"
          , login :=    this->_options.user->login
          , realname := this->_options.user->realname
          , this->deadline
          ]);

      // try to open the port before the link is closed
      this->TryGetPort(pos);
      RETURN;
    }
    CATCH (OBJECT< OperationCancelledException > e)
    {
      this->UpdateStatus(pos, [ status := "closed" ]);
    }
    FINALLY
    {
      link->Close();
    }
  }

  MACRO TryGetPort(INTEGER pos)
  {
    OBJECT canceltoken := this->canceltokensource->canceltoken;
    this->UpdateStatus(pos, [ status := "acquiring" ]);

    STRING portname := "tollium:exclusive_access." || this->_status[pos].identifier;
    WHILE (TRUE)
    {
      OBJECT lock;
      TRY
      {
        lock := OpenLockManager()->LockMutex(portname);
        OBJECT port := CreateIPCPort(portname);
        lock->Close();
        this->UpdateStatus(pos, [ status := "ok", id := DEFAULT RECORD ]);
        this->HandlePort(pos, port)->LinkToken(canceltoken);
        BREAK;
      }
      CATCH (OBJECT e)
      {
        IF (ObjectExists(lock))
          lock->Close();
        OBJECT link := ConnectToIPCPort(portname);
        IF (ObjectExists(link))
        {
          this->HandleExtLink(link, pos)->LinkToken(canceltoken);
          BREAK;
        }
      }
    }

    this->ProcessWaits();
  }

  // ---------------------------------------------------------------------------
  //
  // Public API
  //

  /** Returns a promise that will be resolved when the locked status is known. Returns TRUE
      when all locks are taken
  */
  PUBLIC ASYNC FUNCTION AsyncWaitLocked()
  {
    this->ProcessWaits();
    RETURN AWAIT this->_defer.promise;
  }

  /** Close the lock and cancels takeover requests - warning: executes asynchronous code.
  */
  PUBLIC MACRO Close()
  {
    this->canceltokensource->Cancel();
    WaitForPromise(this->_closedefer.promise);
    this->_defer.resolve(FALSE);
  }
>;

/** Get an exclusive lock on multiple objects. Only works within the webserver process!
    @param tags List of tags
    @cell(record) tags.tag The tag as passed to %TolliumScreenBase::GetExclusiveAccess
    @cell(string) options.mode Mode:
        - "try": Try to get the locks, take only the free locks
        - "requestclose": Request the locks from the users
        - "forceclose": Force close screens of users holding the lock
    @cell(integer) options.takeovertime Override default takeover time
    @cell(object %TolliumUser) options.user User object (required)
    @return(object #BulkExclusiveKeeper) Status object
    @public
    @loadlib mod::tollium/lib/applications.whlib
    @topic tollium/appdev
*/
PUBLIC OBJECT FUNCTION GetBulkExclusiveLock(RECORD ARRAY tags, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  IF (IsDefaultValue(tags))
    THROW NEW Exception(`At least one tag expected`);

  FOREVERY (RECORD tag FROM tags)
  {
    tags[#tag] := ValidateOptions(
        [ tag :=        DEFAULT RECORD
        ], tag,
        [ title := `tags[${#tag}]`
        , passthrough := TRUE
        , required := ["tag"]
        ]);
  }

  tags :=
      SELECT identifier :=  EncodeUFS(GetSHA1Hash(EncodeHSON(tag)))
           , tag
           , orgdata :=     tags
        FROM tags;

  options := ValidateOptions(
      [ mode :=           ""
      , takeovertime :=   0
      , user :=           DEFAULT OBJECT
      ], options,
      [ enums := [ mode := [ "try", "requestclose", "forceclose" ] ]
      , required := [ "user" ]
      ]);

  RECORD pos := SELECT pos := Any(#tags) FROM tags GROUP BY identifier HAVING Count(*) > 1;
  IF (RecordExists(SELECT FROM tags GROUP BY identifier HAVING Count(*) > 1))
    THROW NEW Exception(`Tag #${pos.pos} is mentioned more than once`);

  RETURN NEW BulkExclusiveKeeper(tags, options);
}


/** Get a mutex usable in RPC context. The exclusive lock won't be opened by
    another user while this mutex is locked.
    @param locktoken Lock token
    @return(object mod::system/lib/internal/cluster/lockmanager.whlib#MutexLock) Mutex lock. Do not
    forget to call 'Close' on the lock when done (throws if the lock wasn't taken)
    @public
    @loadlib mod::tollium/lib/applications.whlib
    @topic tollium/appdev
*/
PUBLIC OBJECT FUNCTION GetExclusiveLockRPCMutex(STRING locktoken)
{
  RECORD parts := DecryptForThisServer("tollium:exclusivity", locktoken);

  STRING portname := "tollium:exclusive_access." || parts.i;
  OBJECT lock := OpenLockManager()->LockMutex(portname);
  OBJECT link := ConnectToIPCPort(portname);
  IF (ObjectExists(link))
  {
    RECORD rec := link->DoRequest([ type := "verifylockuid", lockuid := parts.t ]);
    link->Close();
    IF (rec.status = "ok" AND rec.msg.success)
      RETURN lock;
  }

  lock->Close();
  THROW NEW Exception(`The lock was already released`);
}




