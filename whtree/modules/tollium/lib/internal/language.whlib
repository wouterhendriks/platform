<?wh

LOADLIB "wh::adhoccache.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::xml/dom.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/resources.whlib";


RECORD FUNCTION GetLanguageCodesForModule(STRING modulename)
{
  STRING moduledir := GetModuleInstallationRoot(modulename);
  STRING defaultresource := `mod::${modulename}/language/default.xml`;
  STRING ARRAY langcodes;

  IF(moduledir != "")
  {
    RECORD ARRAY langfiles := ReadDiskDirectory(moduledir || "/language","*.xml");
    langcodes :=
           SELECT AS STRING ARRAY GetBasenameFromPath(name)
             FROM langfiles
            WHERE type = 0
                  AND name LIKE "*.xml"
                  AND ToLowercase(name) = name  //verify lowercase
         ORDER BY name = "default.xml" DESC; //make sure default is first

    IF(Length(langcodes) >= 1 AND langcodes[0] = "default")
    {
      OBJECT langfile := MakeXMLDocument(GetWebhareResource(defaultresource));
      IF(ObjectExists(langfile->documentelement))
      {
        STRING actuallang := langfile->documentelement->GetAttribute("xml:lang");
        IF(actuallang != "")
          langcodes[0] := actuallang;
      }
    }
  }

  RETURN [ value := langcodes
         , ttl := 60 * 60 * 1000
         , eventmasks := GetResourceEventMasks([ defaultresource ])
                         CONCAT
                         [ "system:modulesupdate" ]
         ];
}

PUBLIC STRING ARRAY FUNCTION GetSupportedLanguageCodes(STRING module)
{
  RETURN GetAdhocCached(CELL[module], PTR GetLanguageCodesForModule(module));
}

/** @short Get the 'best' matching language code for the given language preference
*/
PUBLIC STRING FUNCTION GetBestLanguageFor(STRING module, STRING langcode)
{
  IF(langcode = "debug")
    RETURN langcode;

  STRING ARRAY actuallanguagecodes := GetSupportedLanguageCodes(module);
  IF(langcode NOT IN actuallanguagecodes AND Length(actuallanguagecodes) > 0 AND actuallanguagecodes[0] IN ["nl","en"])
    RETURN actuallanguagecodes[0];
  RETURN langcode;
}
