/* @mod-tollium/js/internal/debuginterface exposes an API for the debug module. It allows us to offer
   a slight bit of stability and type checking */
export { ToddCompBase } from "@mod-tollium/web/ui/js/componentbase";
