/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

import * as component from '@mod-tollium/web/ui/js/componentbase';

const ActionableBase = component.ActionableComponent;
export default ActionableBase;
