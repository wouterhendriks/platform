<?wh
LOADLIB "wh::witty.whlib";
LOADLIB "wh::internet/webbrowser.whlib";

LOADLIB "mod::consilio/lib/api.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";

LOADLIB "mod::publisher/lib/webdesign.whlib";
LOADLIB "mod::publisher/lib/publisher.whlib";
LOADLIB "mod::publisher/lib/widgets.whlib";
LOADLIB "mod::publisher/lib/forms/api.whlib";
LOADLIB "mod::publisher/lib/forms/base.whlib";
LOADLIB "mod::publisher/lib/webtools/forum.whlib";

LOADLIB "mod::system/lib/cache.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/webserver.whlib";
LOADLIB "mod::system/lib/whfs.whlib";
LOADLIB "mod::system/lib/webserver/whdebug.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";


//stuff shared between site:: and mod:: based designs
PUBLIC STATIC OBJECTTYPE BetaSharedDesign EXTEND WebDesignBase
<
  MACRO NEW()
  {
    IF(IsRequest())
      ProcessWHDebugVariablesUnchecked();

    IF(this->IsStaticPublication() AND this->targetobject->fullpath NOT LIKE "*/abtestfolder/*") //staticfiles are not compatible with abtesting.
    {
      STRING deeperlink := this->GetStaticFileLink("test.txt");
      STRING compareroot := Substring(this->targetsite->webroot, this->targetsite->webroot LIKE "https:*" ? 6 : 5);
      IF(this->targetobject->name="headings.doc" AND compareroot || "headings/test.txt" != deeperlink) //expect the link without 'http:'
        ABORT("Incorrect deeperlink, got '" || deeperlink ||"' want '" || compareroot || "headings/test.txt'");
      IF(this->targetobject->name="static.html" AND compareroot || "%5Estatic.html/test.txt" != deeperlink) //expect the link without 'http:'
        ABORT("Incorrect deeperlink, got '" || deeperlink ||"' want '" || compareroot || "%5Estatic.html/test.txt'");
      this->CreateStaticFile("test.txt", PTR Print(RepeatText("dit was een test\n", 10)));
    }

    IF(this->targetobject->typens = "http://www.webhare.net/xmlns/publisher/formwebtool")
    {
      INSERT CELL rpcformtarget := OpenWebtoolForm(this->targetobject->id)->GetFormTarget() INTO this->htmldata;
    }

    IF(this->targetobject->name LIKE "explicitly-no-title.*")
    {
      this->pagetitle := "";
    }

    IF(this->targetobject->name = "testfile6.rtd")
    {
      this->consiliofields := CELL[ ...this->consiliofields
                                  , pagekeywords := [ "by-name " || this->targetobject->name, "</script>", "by-id " || this->targetobject->id ]
                                  ];
    }
  }

  UPDATE PUBLIC MACRO PrepareErrorPage(INTEGER errorcode, RECORD harescriptinfo, STRING url)
  {
    IF(url LIKE "*throw=exception*")
      THROW NEW Exception("throw is exception");
    WebDesignBase::PrepareErrorPage(errorcode, harescriptinfo, url);
  }

  UPDATE PUBLIC RECORD FUNCTION GetPageConfig()
  {
    // get the widgets as specified on the file that is being published
    OBJECT sharedblockstype := OpenWHFSType("http://www.webhare.net/xmlns/webhare_testsuite/sharedblockstest");
    RECORD ARRAY usewidgets := sharedblockstype->GetSharedBlocks((this->targetobject ?? this->targetfolder)->id, "BLOCKS");

    IF(this->pagedescription = "" AND this->targetobject->typens = "http://www.webhare.net/xmlns/publisher/richdocumentfile")
    {
      this->pagedescription := this->ExtractRTDExcerpt(this->targetobject->id, "p", [ limitutf8bytes := 150 ]);
    }

    IF(this->targetobject->fullpath LIKE "*crash*")
      ABORT("'crash' requested");

    RECORD wittydata := [ deeperlink := this->IsStaticPublication() ? this->GetStaticFileLink("test.txt") : ""
                        , whfspath := this->targetobject->whfspath
                        , dynamiclink := this->GetRelURL(this->targetsite->webroot || "dynamic.shtml")
                        , widget := DEFAULT MACRO PTR
                        , wrdauthplugin := DEFAULT RECORD
                        , webdesign_prepareforrendering := 0
                        , have_widgetimg := ObjectExists(this->targetsite->OpenByPath("design/img/img.jpg"))
                        , dynamicpageparameters := GetDynamicPageParameters()
                          //we'll enable comments if we see the plugin
                        , comments := ObjectExists(GetForumPluginForWebdesign(this)) ? PTR GetForumPluginForWebdesign(this)->EmbedComments() : DEFAULT MACRO PTR
                        , sharedblocks := (SELECT AS MACRO PTR ARRAY PTR this->RenderSharedBlock(usewidgets) FROM usewidgets)
                        , targetobjectpath := ObjectExists(this->targetobject) ? this->targetobject->whfspath : ""
                        , contentobjectpath := ObjectExists(this->contentobject) ? this->contentobject->whfspath : ""
                        , navigationobjectpath := ObjectExists(this->navigationobject) ? this->navigationobject->whfspath : ""
                        , betatestinstance := this->contentobject->GetInstanceData("http://www.webhare.net/xmlns/beta/test")
                        ];

    RETURN wittydata;
  }
  UPDATE PUBLIC RECORD FUNCTION UpdatePageConfig(RECORD pageconfig)
  {
    OBJECT authplugin := this->GetWRDAuthPlugin();
    IF(ObjectExists(authplugin))
      pageconfig.wrdauthplugin := authplugin->GetWittyData();
    RETURN pageconfig;
  }
  UPDATE PUBLIC MACRO PrepareForRendering()
  {
    IF(this->pageconfig.webdesign_prepareforrendering = 1)
      THROW NEW Exception("Duplicate PrepareForRendering invoked");
    this->pageconfig.webdesign_prepareforrendering := 1;
  }
>;

OBJECTTYPE BetaWebDesign EXTEND BetaSharedDesign
<
  MACRO NEW()
  {
    IF(this->targetobject->name IN ["loadlib.html","loadlib.shtml"])
      this->AddLoadlibToOutput(this->designfolder || "lib/basetest-loadlib.whlib");

    TestEq("mod::webhare_testsuite/webdesigns/basetest/", this->designfolder);
    TestEq("webhare_testsuite:basetest", this->assetpack);
    TestEqLike("20??????T??????Z", GetAssetPackBuildVersion(this->assetpack));

    INSERT CELL consiliotoken := GetConsilioRPCToken("webhare_testsuite:testsitecatalog", [ autosuggest := TRUE ])
           INTO this->jssiteconfig;
  }

  UPDATE PUBLIC RECORD FUNCTION GetPageConfig()
  {
    RECORD pageconfig := BetaSharedDesign::GetPageConfig();
    this->contentbody := PTR EmbedWittyComponent("basetest.witty:wrapbody", [ origbody := this->contentbody ]);

    OBJECT bobimage := this->targetsite->OpenByPath("bob.jpg");
    pageconfig := CELL[...pageconfig
                      , bobimagelink := ObjectExists(bobimage) ? WrapCachedImage(bobimage->GetWrapped(), [ method := "none" ]) : DEFAULT RECORD
                      ];

    RETURN pageconfig;
  }


/*
FIXME
      wordpublisher->SetToclevelAsRawHTML(99);
      wordpublisher->maxoutputwidth := 580; //constrain both tables and images to this width
      wordpublisher->deletetextstyles := ["font-size:*"];
*/
>;

PUBLIC OBJECTTYPE BaseTestDesign EXTEND BetaWebDesign
<
  UPDATE PUBLIC STRING FUNCTION RewriteHyperlink(STRING href)
  {
    IF (href LIKE "x-rewritelink:*")
    {
      SWITCH (Substring(href, 14))
      {
        CASE "1"
        {
          RETURN "https://example.org/first.html";
        }
        CASE "2"
        {
          RETURN "https://example.org/second.html";
        }
        CASE "3"
        {
          RETURN ""; // remove link
        }
      }
    }
    IF (href = "x-go-rewrite:#no-really") //this is passed here to verify RewriteHyperlink ALSO gets a shot at rewriting links
      RETURN "x-webdesign-takes-over-link:";
    RETURN href;
  }

  UPDATE PUBLIC STRING FUNCTION GetIntExtLinkTarget(RECORD linkrecord)
  {
    IF(RecordExists(linkrecord) AND linkrecord.internallink = whconstant_whfsid_private_rootsettings AND linkrecord.append = "#goto-nowhere")
      linkrecord := MakeIntExtExternalLink("x-go-rewrite:#no-really");

    RETURN BetaWebDesign::GetIntExtLinkTarget(linkrecord);
  }
>;


PUBLIC STATIC OBJECTTYPE Level1Widget EXTEND WidgetBase
<
  OBJECT rtd;

  MACRO NEW()
  {
    this->rtd := this->OpenRTD(this->data.richdoc);
    this->__pvt_renderwide := TRUE; //FIXME we should invoke a proper Preview function, but this is just here for the tests...
  }
  UPDATE PUBLIC MACRO Render()
  {
    STRING ARRAY libraries_temp;
    IF(ObjectExists(this->context->targetapplytester)) //eg. testrtecomponent triggers this path without an applytester as it doesn't use a site
      FOREVERY(INTEGER lib FROM this->context->targetapplytester->GetLibrary("temp"))
        INSERT (SELECT AS STRING whfspath FROM system.fs_objects WHERE id=lib) ?? "#" || lib INTO libraries_temp AT END;

    Print('<div class="level1widget" data-lang="' || this->context->languagecode || '" data-mytype="' || this->widgettype || '" data-rtdtype="' || this->rtdtype
           || '" data-targetsite="'   || (ObjectExists(this->context->targetsite) ? this->context->targetsite->id : -1)
           || '" data-targetfolder="' || (ObjectExists(this->context->targetfolder) ? this->context->targetfolder->id : -1)
           || '" data-whfssettingid="' || this->whfssettingid
           || '" data-whfsfileid="' || this->whfsfileid
           || '" data-libraries-temp="' || EncodeValue(Detokenize(libraries_temp,' ')) || '"'
           // not exposed yet|| '" data-targetobject="' || (ObjectExists(this->context->targetobject) ? this->context->targetobject->id : -1)
           || '">');
    Print("</div>");
  }
>;

PUBLIC STATIC OBJECTTYPE Level2Widget EXTEND WidgetBase
<
  OBJECT rtd;
  MACRO NEW()
  {
    this->rtd := this->OpenRTD(this->data.richdoc);
  }

  UPDATE PUBLIC MACRO Render()
  {
    IF(Length(this->data.testrenderinstance_array) > 0 AND NOT CellExists(this->data.testrenderinstance_array[0],"memberb"))
      ABORT("RenderWidgetInstance did not restructure the 'deeper' members");

    this->EmbedComponent( [ targetsite := (ObjectExists(this->context->targetsite) ? this->context->targetsite->id : -1)
                          , targetfolder := (ObjectExists(this->context->targetfolder) ? this->context->targetfolder->id : -1)
                          , whfssettingid := this->whfssettingid
                          , whfsfileid := this->whfsfileid
                          , rtdtype := this->rtdtype
                          , mytype := this->widgettype
                          , rtd := PTR this->rtd->RenderAllObjects()
                          ]);
  }
>;

PUBLIC STATIC OBJECTTYPE Level2Widget_OverrideSearch EXTEND Level2Widget
<
  UPDATE PUBLIC MACRO RenderSearchPreview()
  {
    Print(`<div class="level2-searchpreview">This is the level2 search preview</div>`);
  }
>;

PUBLIC OBJECTTYPE CustomPlugin EXTEND WebDesignPluginBase
<
>;

PUBLIC BOOLEAN FUNCTION SkipCaptcha(OBJECT webcontext)
{
  RETURN GetFormWebVariable("skipcaptcha") = "1";
}

PUBLIC STATIC OBJECTTYPE CaptchaFormHooks EXTEND WebtoolFormHooks
<
  MACRO NEW()
  {
    IF(IsRequest()) //allows ?wh-debug=nsc to work in the captcha tests - this use to be in BetaSharedDesign but using webtoolformhooks= now routes around the webdesign
      ProcessWHDebugVariablesUnchecked();
  }

  UPDATE PUBLIC MACRO Validate(OBJECT work)
  {
    //Note that we can't do this in Submit() - recaptcha validation will have run before us
    RECORD namefield := SELECT * FROM this->form->ListFields() WHERE GetTid(title)="First name";
    IF(namefield.obj->value = "reject")
      work->AddErrorFor(namefield.obj, "Rejected!");
  }
>;

PUBLIC STATIC OBJECTTYPE BaseTestProps EXTEND TolliumTabsExtensionBase
<
  MACRO OnObjectLibraryChange()
  {
    ^librarysource->fslibrary := ^objectlibrary->value;
  }
>;

PUBLIC BOOLEAN FUNCTION CheckTestpagesURLHistoryAccess(RECORD urlinfo)
{
  RETURN urlinfo.url LIKE "*/i-have-got-the-power";
}

PUBLIC STATIC OBJECTTYPE WidgetBlockWidget EXTEND WidgetBase
<
  UPDATE PUBLIC MACRO Render()
  {
    Print(`WidgetBlockWidget`);
  }

  UPDATE PUBLIC MACRO RenderLive()
  {
    RECORD ARRAY items;
    FOREVERY( INTEGER widget FROM this->data.widgets )
      INSERT [ contents := PTR this->context->RenderSharedBlock( ResolveWidgetAsSharedBlock(widget)) ] INTO items AT END;

    this->EmbedComponent( CELL[ items
                              ] );
  }
>;
