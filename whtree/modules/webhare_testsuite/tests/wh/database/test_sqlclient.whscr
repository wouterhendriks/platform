<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/configure.whlib";

STRING FUNCTION GetOutputUntilTermination(OBJECT proc)
{
  proc->Start();
  STRING alloutput;
  DATETIME deadline := AddTimeToDate(60*1000,GetCurrentDatetime());
  WHILE(TRUE)
  {
    IF(NOT proc->Wait(MAX_DATETIME))
      ABORT("Process timeout");

    STRING output := ReadFrom(proc->output_handle,16384);
    IF (output="" AND NOT proc->IsRunning())
      BREAK;

    alloutput := alloutput || output;
    Print(output);
  }
  proc->Close();
  RETURN alloutput;
}

MACRO WithArguments()
{
  DATETIME start := GetCurrentDatetime();

  STRING runscript := GetWebhareConfiguration().installationroot || "bin/runscript";
  OBJECT proc;
  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr","--showmode","json","? 1 + 1"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq("2",TrimWhitespace(GetOutputUntilTermination(proc)));
  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "--showmode","json","select from system.fs_objects limit 1"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq("[{}]",TrimWhitespace(GetOutputUntilTermination(proc))); //one empty record..
  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "--showmode","json","select * from system.fs_objects limit 1"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEqLike("[{*highestparent*}]",TrimWhitespace(GetOutputUntilTermination(proc))); //one empty record..
  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "--showmode","json","select from system.fs_objects where filelink = 0 limit 1"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq("[{}]",TrimWhitespace(GetOutputUntilTermination(proc))); //one empty record..
  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "--showmode","json","select * from wrd.entity_settings limit 1"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  RECORD res := DecodeJSON(GetOutputUntilTermination(proc));
  TestEq(TRUE, RecordExists(res));

  //test raw modes - they print strings directly and fall back to JSON for other values (which still works nicely for booleans and integers)
  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "--showmode","raw","select id from system.fs_objects where id=1"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq('[{"id":1}]\n',GetOutputUntilTermination(proc));

  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "--showmode","raw","select as integer id from system.fs_objects where id=1"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq("1\n",GetOutputUntilTermination(proc));

  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "-r","?DecodeBase64('SGkh')"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq("Hi!\n",GetOutputUntilTermination(proc));

  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "-r","?TRUE"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq("true\n",GetOutputUntilTermination(proc));

  proc := CreateProcess(runscript, ["mod::system/scripts/whcommands/sql.whscr", "-r","?[1]"],FALSE,TRUE,TRUE,TRUE);
  proc->SetEnvironment(RECORD[ ...(SELECT * FROM GetEnvironment() WHERE name != "WEBHARE_CLI_USER"), [ name := "WEBHARE_CLI_USER", value := "webhare_testsuite_user1" ] ]);
  TestEq("[1]\n",GetOutputUntilTermination(proc));

  DATETIME testend := GetCurrentDatetime();

  // Should be flushed immediately
  OBJECT stream := OpenWebHareLogStream("audit", start, testend);

  RECORD ARRAY items;
  WHILE (TRUE)
  {
    RECORD rec := stream->ReadRecord();
    IF (NOT RecordExists(rec))
      BREAK;

    IF (rec.parts[0] = "system:sqlclient" AND rec.parts[2] LIKE "*webhare_testsuite_user1*")
      INSERT rec INTO items AT END;
  }
  stream->Close();

  TestEQMembers(
      [ user_entityid := -1
      , user_login := 'CLI:webhare_testsuite_user1'
      ], DecodeHSON(items[0].parts[2]), "*");
  TestEQ('hson:{\"query\":\"? 1 + 1\"}', items[0].parts[3]);
  TestEQ('hson:{\"query\":\"select from system.fs_objects limit 1\"}', items[1].parts[3]);
}

RunTestFramework([ PTR WithArguments ]);
