<?wh
LOADLIB "mod::publisher/lib/dialogs.whlib";
LOADLIB "mod::publisher/lib/siteprofiles.whlib";

LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::webhare_testsuite/lib/system/tests.whlib";
LOADLIB "mod::tollium/lib/testframework.whlib";

ASYNC MACRO TestThumbnailBrowse()
{
  RECORD res := AWAIT ExpectScreenChange(+1, PTR RunBrowseForFSObjectDialog(GetTestController(),
    [ acceptfiles := TRUE
    , acceptfolders := FALSE
    , acceptunpublished := TRUE
    , accepttypes := [ "http://www.webhare.net/xmlns/publisher/imagefile" ]
    , type := "thumbnails"
    , roots := [INTEGER(OpenTestsuiteSite()->id)]
    ]));

  OBJECT imgeditfile := OpenTestsuiteSite()->OpenByPath("/testpages/imgeditfile.jpeg");
  TT("folders")->selection := SELECT * FROM TT("folders")->rows WHERE name="TestPages";
  TT("filesthumbs")->value := imgeditfile->id;
  topscreen->frame->focused := TT("filesthumbs");
  TestEq("imgeditfile.jpeg", TT("filesthumbs")->selection.name);

  AWAIT ExpectScreenChange(-1, PTR TT("submitaction")->TolliumClick());

  TestEq(imgeditfile->id, AWAIT res.expectcallreturn());

  AWAIT ExpectScreenChange(+1, PTR RunBrowseForFSObjectDialog(GetTestController(),
    [ acceptfiles := TRUE
    , folder := imgeditfile->parent
    ]));

  TestEq("TestPages", TT("folders")->selection.name);
  TestEq(0, Length(SELECT FROM TT("folders")->rows WHERE name="Trash"), "No recyclebin by default");

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel());

  AWAIT ExpectScreenChange(+1, PTR RunBrowseForFSObjectDialog(GetTestController(),
    [ acceptfiles := TRUE
    ]));

  TestEq("TestPages", TT("folders")->selection.name, "Should have remembered testpages");

  AWAIT ExpectScreenChange(-1, PTR topscreen->TolliumExecuteCancel());

  res := AWAIT ExpectScreenChange(+1, PTR RunBrowseForFSObjectDialog(GetTestController(),
    [ folder := -1
    , recyclebinid := -5
    , acceptfiles := FALSE
    ]));

  TestEq("WebHare Publisher", TT("folders")->selection.name);
  TestEq(1, Length(SELECT FROM TT("folders")->rows WHERE name="Trash"), "Should have a bin (and no other root folders named trash please, not going to have tests deal with that)");
  TT("folders")->selection := SELECT * FROM TT("folders")->rows WHERE name="Trash";
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));

  TestEq(-5, AWAIT res.expectcallreturn());
}

ASYNC MACRO TestBrowseForObject()
{
  testfw->BeginWork();
  OBJECT testfile := GetTestsuiteTempFolder()->CreateFile([ name := "deleteme.txt" ]);
  testfw->CommitWork();

  OBJECT scr := GetTestController()->LoadScreen(InlineScreens(
    `<screen name="test" implementation="none" xmlns:p="http://www.webhare.net/xmlns/publisher/components">
    <body>
      <p:browseforobject name="comp" site="webhare_testsuite.testsite" fullpath="/portal1/"/>
    </body>
  </screen>`, "#test"));

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR TTClick("comp->browseaction"));
  TestEq("portal1", TT("folders")->selection.name);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);
  AWAIT ExpectScreenChange(-1, PTR TTEscape);

  scr := GetTestController()->LoadScreen(InlineScreens(
    `<screen name="test" implementation="none" xmlns:p="http://www.webhare.net/xmlns/publisher/components">
    <body>
      <p:browseforobject name="comp" site="webhare_testsuite.testsite" fullpath="/portal1/"/>
    </body>
  </screen>`, "#test"));
  scr->comp->value := testfile->id;

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  TestEq("webhare_testsuite.testsite:/tmp/deleteme.txt", scr->comp->^path->value);
  AWAIT ExpectScreenChange(+1, PTR TTClick("comp->browseaction"));
  AWAIT ExpectScreenChange(-1, PTR TTClick(":Ok"));
  TestEq("webhare_testsuite.testsite:/tmp/deleteme.txt", scr->comp->^path->value);
  AWAIT ExpectScreenChange(-1, PTR TTEScape);

  testfw->BeginWork();
  testfile->RecycleSelf();
  testfw->CommitWork();

  scr := GetTestController()->LoadScreen(InlineScreens(
    `<screen name="test" implementation="none" xmlns:p="http://www.webhare.net/xmlns/publisher/components">
    <body>
      <p:browseforobject name="comp" site="webhare_testsuite.testsite" fullpath="/portal1/"/>
    </body>
  </screen>`, "#test"));
  scr->comp->value := testfile->id;

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  TestEq("deleteme.txt (deleted)", scr->comp->^path->value);
  AWAIT ExpectScreenChange(-1, PTR TTEScape);

  scr := GetTestController()->LoadScreen(InlineScreens(
    `<screen name="test" implementation="none" xmlns:p="http://www.webhare.net/xmlns/publisher/components">
    <body>
      <p:browseforobject name="comp" fullpath="/portal1/"/>
    </body>
  </screen>`, "#test"), DEFAULT RECORD, [ contexts := [ applytester := GetApplyTesterForObject(GetTestsuiteTempFolder()->id)
                                        ]]);

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR TTClick("comp->browseaction"));
  TestEq("portal1", TT("folders")->selection.name);
  AWAIT ExpectScreenChange(-1, PTR TTEScape);
  AWAIT ExpectScreenChange(-1, PTR TTEScape);

  scr := GetTestController()->LoadScreen(InlineScreens(
    `<screen name="test" implementation="none" xmlns:p="http://www.webhare.net/xmlns/publisher/components">
    <body>
      <p:browseforobject name="comp" fullpath="/"/>
    </body>
  </screen>`, "#test"), DEFAULT RECORD, [ contexts := [ applytester := GetApplyTesterForObject(GetTestsuiteTempFolder()->id)
                                        ]]);

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());
  AWAIT ExpectScreenChange(+1, PTR TTClick("comp->browseaction"));
  TestEq("webhare_testsuite.testsite", TT("folders")->selection.name);
  AWAIT ExpectScreenChange(-1, PTR TTEScape);
  AWAIT ExpectScreenChange(-1, PTR TTEScape);
}

ASYNC MACRO TestBrowseForObjectArray()
{
  OBJECT imgeditfile := OpenTestsuiteSite()->OpenByPath("/testpages/imgeditfile.jpeg");
  OBJECT staticpage := OpenTestsuiteSite()->OpenByPath("/testpages/staticpage");

  // https://my.webhare.dev/?app=webhare_testsuite:anycomponent(http://www.webhare.net/xmlns/publisher/components:browseforobjectarray)
  OBJECT scr := GetTestController()->LoadScreen("webhare_testsuite:tests/anycomponent.anycomponent", [ component := "http://www.webhare.net/xmlns/publisher/components:browseforobjectarray" ]);
  scr->comp->browsetype := "thumbnails";
  scr->comp->site := OpenTestsuiteSite()->name;
  scr->comp->fullpath := "/testpages/";
  scr->comp->accepttypes := [ "http://www.webhare.net/xmlns/publisher/imagefile" ];

// onchange="mod::webhare_testsuite/lib/tollium/handlers.whlib#IncCallCounter"

  AWAIT ExpectScreenChange(+1, PTR scr->RunModal());

  AWAIT ExpectScreenChange(+1, PTR TTClick(":Add"));
  //INSERT TT("folders")->rows[0].rowkey INTO TT("folders")->expanded AT END;
  TestEq("TestPages", TT("folders")->selection.name);
  TT("filesthumbs")->value := [INTEGER(imgeditfile->id), INTEGER(staticpage->id)];
  topscreen->frame->focused := TT("filesthumbs");

  AWAIT ExpectScreenChange(-1, PTR TTClick(":OK"));
  TestEq(SortArray([INTEGER(imgeditfile->id), INTEGER(staticpage->id)]), SortArray(scr->comp->value));

  TestEq(1, scr->onchangecount->value, 'Expecting 1 change');

  AWAIT ExpectScreenChange(-1, PTR TTEscape);
}

RunTestFramework(
    [ PTR TestThumbnailBrowse
    , PTR TestBrowseForObject
    , PTR TestBrowseForObjectArray
    ], [ testusers := [[ login := "sysop", grantrights := ["system:sysop"]]
                      ]
       ]);
