/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

import * as test from '@mod-system/js/wh/testframework';
import * as dompack from 'dompack';

function getFormRPCRequests() {
  return Array.from(test.getWin().performance.getEntriesByType('resource')).filter(node => node.name.includes("/wh_services/publisher/forms/"));
}

function testNoLookup(fieldname) {
  test.eq(0, test.qSA(`[data-wh-form-group-for^="${CSS.escape(fieldname + ".")}"]`).filter(el => el.classList.contains("wh-form__fieldgroup--addresslookup")).length);
}
function testHasLookup(fieldname) {
  test.assert(test.qSA(`[data-wh-form-group-for^="${CSS.escape(fieldname + ".")}"]`).filter(el => el.classList.contains("wh-form__fieldgroup--addresslookup")).length > 0);
}

test.registerTests(
  [
    'Check UX',
    async function () {
      await test.invoke('mod::webhare_testsuite/lib/internal/testsite.whlib#SnoozeRateLimits');
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=2');

      test.eq(0, getFormRPCRequests().length, "Verify initial state");

      //just changing country on an empty field used to trigger a validation, and then a "Ongeldige postcode"
      test.fill("#addressform-address\\.country", "BE");
      testNoLookup("address");
      await test.sleep(50);

      //ensure nothing has the lookup class
      testNoLookup("address");

      test.fill("#addressform-address\\.country", "NL");
      testNoLookup("address");
      await test.sleep(50);

      //still on lookups
      testNoLookup("address");

      //set a zipcode and housenumber, bring the NL validator into error mode
      test.eq(0, getFormRPCRequests().length, "Still no lookups please..");
      test.fill("#addressform-address\\.nr_detail", "100");
      test.fill("#addressform-address\\.zip", "1000");
      await test.wait('ui');

      test.eq(1, getFormRPCRequests().length, "ONE lookup allowed to reject 1000-100");
      test.assert(test.qS('[data-wh-form-group-for="address.zip"]').classList.contains("wh-form__fieldgroup--error"), "ZIP should now be in error mode");

      //STORY: Switching to BE should immediately clear the zip error state. let validation confirm issues first..
      test.fill("#addressform-address\\.country", "BE");
      test.assert(!test.qS('[data-wh-form-group-for="address.zip"]').classList.contains("wh-form__fieldgroup--error"), "ZIP should be out of error mode");
    },

    'Check UX - BE',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=2');
      test.fill("#addressform-address\\.country", "BE");
      test.fill("#addressform-address\\.nr_detail", "100");
      test.fill("#addressform-address\\.zip", "1000");
      testNoLookup("address");

      await test.sleep(50);
      testNoLookup("address");
    },

    'Check required subfields',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=2');

      test.assert(test.canClick("#addressform-address\\.country"));
      test.eq('', test.qS("#addressform-address\\.country").value);
      test.assert(!test.canClick("#addressform-address\\.city"));

      // NL (no province, street and city disabled, rest required)
      test.fill("#addressform-address\\.country", "NL");
      test.assert(test.canClick("#addressform-address\\.street"));
      test.assert(test.canClick("#addressform-address\\.nr_detail"));
      test.assert(test.canClick("#addressform-address\\.zip"));
      test.assert(test.canClick("#addressform-address\\.city"));
      test.assert(test.qS("#addressform-address\\.street").disabled);
      test.assert(!test.qS("#addressform-address\\.nr_detail").disabled);
      test.assert(!test.qS("#addressform-address\\.zip").disabled);
      test.assert(test.qS("#addressform-address\\.city").disabled);
      test.assert(!test.qS("#addressform-address\\.province").required);
      test.assert(!test.qS("#addressform-address\\.street").required);
      test.assert(test.qS("#addressform-address\\.nr_detail").required);
      test.assert(test.qS("#addressform-address\\.zip").required);
      test.assert(!test.qS("#addressform-address\\.city").required);

      // BE (+province, province and nr_detail not required)
      test.fill("#addressform-address\\.country", "BE");
      test.assert(test.canClick("#addressform-address\\.street"));
      test.assert(test.canClick("#addressform-address\\.province"));
      test.assert(test.canClick("#addressform-address\\.nr_detail"));
      test.assert(test.canClick("#addressform-address\\.zip"));
      test.assert(test.canClick("#addressform-address\\.city"));
      test.assert(!test.qS("#addressform-address\\.province").required);
      test.assert(test.qS("#addressform-address\\.street").required);
      test.assert(test.qS("#addressform-address\\.nr_detail").required);
      test.assert(test.qS("#addressform-address\\.zip").required);
      test.assert(test.qS("#addressform-address\\.city").required);

      //Test that the third unconfigured address field simply shows all countries
      const address3country = test.qS("#addressform-address3\\.country");
      test.assert(address3country, 'selector should be visible');
      //make sure it sorted right. ie NL countrynames didn't get EN sorted

      test.eq(["Afghanistan", "Åland"], Array.from(address3country.options).slice(1, 3).map(el => el.textContent));

      //Test that selecting NL does not disable the or reorder the zip field
      test.fill(address3country, 'NL');
      await test.wait('tick'); //just in case someone delays reodering

      const zipfield = test.qS("#addressform-address3\\.zip");
      const streetfield = test.qS("#addressform-address3\\.street");
      test.assert(!streetfield.disabled);
      test.assert(zipfield.getBoundingClientRect().top > streetfield.getBoundingClientRect().bottom, "Zip MUST be below country");
    },

    'Check recursive enable/visible',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=1');

      // initially not visible
      test.assert(!test.canClick("#addressform-address\\.nr_detail"));
      test.assert(!test.qS("#addressform-address\\.street").required);
      test.assert(!test.qS("#addressform-address\\.nr_detail").required);

      test.fill("#addressform-address\\.country", "NL");
      test.assert(test.qS("#addressform-address\\.nr_detail").required);
      test.assert(!test.qS("#addressform-address\\.nr_detail").disabled);

      test.fill("#addressform-enablefields", false);
      test.assert(test.qS("#addressform-address\\.nr_detail").disabled);

      test.fill("#addressform-visiblefields", false);
      test.assert(!test.canClick("#addressform-address\\.nr_detail"));

      test.fill("#addressform-enablefields", true);
      test.fill("#addressform-visiblefields", true);
      test.assert(!test.qS("#addressform-address\\.nr_detail").disabled);
      test.assert(test.canClick("#addressform-address\\.nr_detail"));

      test.fill("#addressform-enablegroup", false);
      test.assert(test.qS("#addressform-address\\.nr_detail").disabled);

      test.fill("#addressform-visiblegroup", false);
      test.assert(!test.canClick("#addressform-address\\.nr_detail"));
    },

    'Check address validation',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=1');

      // set country to NL
      test.fill("#addressform-address\\.country", "NL");
      // fill nr and zip
      test.fill("#addressform-address\\.nr_detail", "296");
      test.fill("#addressform-address\\.zip", "7521AM");
      // address validation/completion should be triggered now

      await test.wait("ui");
      test.eq("Hengelosestraat", test.qS("#addressform-address\\.street").value);
      test.eq("Enschede", test.qS("#addressform-address\\.city").value);

      test.fill("#addressform-address\\.country", "NL");

      //ensure nothing has the lookup class
      testNoLookup("address");
      test.fill("#addressform-address\\.zip", "7500 OO");

      //no visible element should not have the lookup class
      test.eq([], test.qSA('[data-wh-form-group-for^="address."]:not(.wh-form__fieldgroup--hidden)').filter(el => !el.classList.contains("wh-form__fieldgroup--addresslookup")));
      await test.wait("ui");
      //lookup should be done again
      testNoLookup("address");

      test.fill("#addressform-address\\.nr_detail", "2");
      //no visible element should not have the lookup class
      test.eq([], test.qSA('[data-wh-form-group-for^="address."]:not(.wh-form__fieldgroup--hidden)').filter(el => !el.classList.contains("wh-form__fieldgroup--addresslookup")));
      await test.wait("ui");
      //lookup should be done again
      testNoLookup("address");

      test.eq("Combinatie van postcode en huisnummer komt niet voor.", test.qS('[data-wh-form-group-for="address.zip"] .wh-form__error').textContent);

      // address_not_found should place an error on the first field (here: 'zip')
      test.fill("#addressform-address\\.nr_detail", "3");
      await test.wait("ui");
      test.eq("Het adres kon niet worden gevonden.", test.qS('[data-wh-form-group-for="address.zip"] .wh-form__error').textContent);
    },

    async function CheckAddressFieldReordering() {
      const countryoptions = [...test.qR("#addressform-address2\\.country").options].map(el => el.value);
      test.eq(["", "NL", "BE", "DE", "", "AF", "AX", "AL"], countryoptions.slice(0, 8));
      test.fill("#addressform-address2\\.country", "NL");
      test.eq(["country", "zip", "nr_detail", "street", "city"], test.qSA(`.wh-form__fieldgroup:not(.wh-form__fieldgroup--hidden) [name^="address2."]`).map(node => node.name.replace("address2.", "")));
      test.fill("#addressform-address2\\.country", "BE");
      test.eq(["country", "province", "street", "nr_detail", "zip", "city"], test.qSA(`.wh-form__fieldgroup:not(.wh-form__fieldgroup--hidden) [name^="address2."]`).map(node => node.name.replace("address2.", "")));
      test.fill("#addressform-address2\\.country", "DE");
      test.eq(["country", "street", "nr_detail", "zip", "city"], test.qSA(`.wh-form__fieldgroup:not(.wh-form__fieldgroup--hidden) [name^="address2."]`).map(node => node.name.replace("address2.", "")));
      test.fill("#addressform-address2\\.country", "NL");
      test.eq(["country", "zip", "nr_detail", "street", "city"], test.qSA(`.wh-form__fieldgroup:not(.wh-form__fieldgroup--hidden) [name^="address2."]`).map(node => node.name.replace("address2.", "")));
    },

    "Regression: addressfield clearing itself when receiving events that don't change anything",
    async function () {
      test.eq("NL", test.qS("[id='addressform-address.country']").value);
      test.eq("7500 OO", test.qS("[id='addressform-address.zip']").value);
      test.fill("[id='addressform-visiblegroup']", false); //hide the addreses
      dompack.dispatchDomEvent(test.qS("[id='addressform-address.country']"), 'change'); //this used to trigger a reset because the field was hidden
      test.eq("NL", test.qS("[id='addressform-address.country']").value);
    },
    // */
    "Regression: modify during validation",
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=1');
      //fill in address2 just so we can submit...
      test.fill("#addressform-address2\\.country", "BE");
      test.fill("#addressform-address2\\.city", "Brussel");
      test.fill("#addressform-address2\\.street", "Rue de la Loi");
      test.fill("#addressform-address2\\.nr_detail", "6");
      test.fill("#addressform-address2\\.zip", "1000");

      test.fill("#addressform-address\\.country", "NL");
      test.fill("#addressform-address\\.zip", "1000");
      test.fill("#addressform-address\\.nr_detail", "6");
      testHasLookup("address");
      //now while the lookup is flying... switch country to one where the above address is VALID
      test.fill("#addressform-address\\.country", "BE");
      test.fill("#addressform-address\\.city", "Brussel");
      test.fill("#addressform-address\\.street", "Rue de la Loi");
      test.click("button[type=submit]");

      await test.wait("ui");
      test.assert(!test.qS('[data-wh-form-group-for="address.zip"]').classList.contains("wh-form__fieldgroup--error"), "ZIP was valid for BE so should NOT report an error");
      test.eq("1000", test.qS("#addressform-address\\.zip").value);
      test.assert(!test.qS('[data-wh-form-group-for="address2.zip"]').classList.contains("wh-form__fieldgroup--error"), "ZIP was valid for BE so should NOT report an error");
      test.eq("1000", test.qS("#addressform-address2\\.zip").value);
    },

    "Regression: addressfield nl-optional broke (incorrectly waiting for allrequiredset)",
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=1');
      test.fill("#addressform-address2\\.country", "NL");
      test.fill("#addressform-address2\\.zip", "7521AM");
      test.fill("#addressform-address2\\.nr_detail", "296");
      await test.pressKey('Tab');
      //wait for completion
      await test.wait(() => test.qS("#addressform-address2\\.street").value);
      test.eq("Hengelosestraat", test.qS("#addressform-address2\\.street").value);
      test.eq("Enschede", test.qS("#addressform-address2\\.city").value);
    },

    "Test using (disabled) city field as condition source",
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?address=1');

      test.assert(!test.canClick("#addressform-neighbourhood"));

      test.fill("#addressform-address\\.country", "NL");
      test.fill("#addressform-address\\.zip", "7521AM");
      test.fill("#addressform-address\\.nr_detail", "296");
      await test.pressKey('Tab');
      //wait for completion
      await test.wait(() => test.qS("#addressform-address\\.street").value);

      test.assert(test.canClick("#addressform-neighbourhood"));
    }
  ]);
