/* eslint-disable */
/// @ts-nocheck -- Bulk rename to enable TypeScript validation

import * as test from '@mod-system/js/wh/testframework';
import * as dompack from 'dompack';
import FormBase from '@mod-publisher/js/forms/formbase';

function setRequiredFields() //fill them with a value so we can submit
{
  test.fill(test.qS('#coretest-email'), 'pietje@example.com');
  test.fill(test.qS('#coretest-setvalidator'), 'test');
  test.click(test.qS('#coretest-requiredradio-x'));
  test.fill(test.qS('#coretest-pulldowntest'), '2');
  test.click(test.qS('#coretest-agree'));
  test.fill('#coretest-address\\.country', "NL");
  test.fill("#coretest-address\\.nr_detail", "296");
  test.fill("#coretest-address\\.zip", "7521AM");
  test.fill('#coretest-dateofbirth', '1999-12-31');
  test.fill('#coretest-number', '1');
}

test.registerTests(
  [
    'Test the new native validator',
    async function () {
      if (dompack.debugflags.fdv)
        alert("Disable the 'fdv' debugflag before running a validation test");

      await test.invoke('mod::webhare_testsuite/lib/internal/testsite.whlib#SnoozeRateLimits');
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?customemailvalidator=1');
      test.click('#coretest-email');
      test.eq(0, test.qSA('.wh-form__fieldgroup--error').length, "Form should be initially clean of errors");
      test.eq(0, test.qSA('aria-invalid').length, "Form should not have any aria-invalid attributes yet");

      test.fill('#coretest-email', 'x');
      test.eq(0, test.qSA('.wh-form__fieldgroup--error').length, "No errors if this field never failed #1");
      test.fill('#coretest-email', '');
      test.eq(0, test.qSA('.wh-form__fieldgroup--error').length, "No errors if this field never failed #2");

      await test.pressKey('Tab'); // leave email field

      // The email field should now get validated and give an error because it's required and the value is empty
      const emailfield = test.qS('#coretest-email');
      const emailgroup = emailfield.closest('.wh-form__fieldgroup');

      test.assert(emailfield.getAttribute("aria-invalid"), "Expecting required emailfield to be in error mode now");

      let errornode = emailgroup.querySelector('.wh-form__error');
      test.assert(emailgroup.classList.contains('wh-form__fieldgroup--error'), "Expecting required emailfield to have an error message");
      test.eq('Dit veld is verplicht.', errornode.textContent);
      test.eq(errornode.id, emailfield.getAttribute("aria-describedby"));

      await new Promise(resolve => setTimeout(resolve, 50)); //small delay to make sure no odd event handlers/focus change is interecept us

      await test.pressKey('z'); //this should end up in 'setValidator' field

      await test.pressKey('Tab', { shiftKey: true });
      await test.pressKey('x');

      errornode = emailgroup.querySelector('.wh-form__error');
      test.eq('Dit is geen geldig e-mailadres.', emailgroup.querySelector('.wh-form__error').textContent);
      test.eq("true", emailfield.getAttribute("aria-invalid"));
      test.eq(errornode.id, emailfield.getAttribute("aria-describedby"));

      // Set a correct email and check that errors has gone
      test.fill('#coretest-email', 'advocado@beta.webhare.net');
      await test.wait('ui');
      test.eq('', emailgroup.querySelector('.wh-form__error').textContent);
      test.assert(!emailgroup.classList.contains('wh-form__fieldgroup--error'));
      test.assert(!emailfield.hasAttribute("aria-invalid"));
      test.assert(!emailfield.hasAttribute("aria-describedby"));

      //but now we should again warn immediately about errors
      await test.pressKey('@');
      test.eq('Dit is geen geldig e-mailadres.', emailgroup.querySelector('.wh-form__error').textContent);
      test.assert(emailgroup.classList.contains('wh-form__fieldgroup--error'));

      test.eq("true", emailfield.getAttribute("aria-invalid"));
      errornode = emailgroup.querySelector('.wh-form__error');
      test.eq(errornode.id, emailfield.getAttribute("aria-describedby"));
    },

    'Test required/focus behavior of additional fields inside radio groups',
    async function () {
      test.click("#coretest-radiotest-5");
      test.click("#coretest-opt5_textedit");
      test.assert(!test.qS("#coretest-opt5_textedit").matches(".wh-form__field--error, .wh-form__field--everfailed"), "Should not be in failed state yet");
      test.assert(!test.qS("#coretest-opt5_textedit").closest(".wh-form__fieldgroup").matches(".wh-form__fieldgroup--error"), "Group should not be in failed state yet");
      test.click("#coretest-number"); //focus something else
      //now we should see the error classes appear!
      await test.wait('ui');
      test.assert(test.qS("#coretest-opt5_textedit").matches(".wh-form__field--error.wh-form__field--everfailed"));
      test.assert(test.qS("#coretest-opt5_textedit").closest(".wh-form__fieldgroup").matches(".wh-form__fieldgroup--error"));
    },

    'Test number field',
    async function () {
      const field = test.qS('#coretest-number');
      const numbergroup = test.qS('#coretest-number').closest('.wh-form__fieldgroup');
      test.fill('#coretest-number', '5');
      await test.pressKey('Tab', { shiftKey: true });
      test.assert(field.getAttribute("aria-invalid"));
      test.eq('De waarde mag niet groter zijn dan 2.', numbergroup.querySelector('.wh-form__error').textContent);
      test.fill('#coretest-number', '-5');
      await test.wait('ui');
      test.eq('De waarde mag niet lager zijn dan -2.', numbergroup.querySelector('.wh-form__error').textContent);

      // check ARIA attributes
      const errornode = numbergroup.querySelector('.wh-form__error');
      test.eq(errornode.id, field.getAttribute("aria-describedby"));
    },

    'Test datetime field',
    async function () {
      test.eq('', test.qS('#coretest-dateofbirth').validationMessage || '');

      const dateofbirthgroup = test.qS('#coretest-dateofbirth').closest('.wh-form__fieldgroup');
      const sevendayslater = new Date(Date.now() + 7 * 86400 * 1000).toISOString().substr(0, 10);

      //FIXME date localization
      test.fill('#coretest-dateofbirth', sevendayslater);
      await test.pressKey('Tab', { shiftKey: true });
      test.eq(/De waarde mag niet groter zijn dan ..-..-2...\./, dateofbirthgroup.querySelector('.wh-form__error').textContent);
      test.fill('#coretest-dateofbirth', '1899-12-31');
      await test.wait('ui');
      test.eq('De waarde mag niet lager zijn dan 01-01-1900.', dateofbirthgroup.querySelector('.wh-form__error').textContent);

      test.fill("#coretest-dateofbirth", "");
      test.eq('Dit veld is verplicht.', dateofbirthgroup.querySelector('.wh-form__error').textContent);

      const errornode = dateofbirthgroup.querySelector('.wh-form__error');

      // check ARIA attributes
      const field = test.qS('#coretest-dateofbirth');
      // test.eq("true", field.getAttribute("aria-invalid"));
      test.eq(errornode.id, field.getAttribute("aria-describedby"));
    },

    'Test radio visiblity and checks',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?customemailvalidator=1');

      //      test.assert(!test.qS('[data-wh-form-group-for="requiredradio"]').classList.contains("wh-form__fieldgroup--error"));
      // Checkbox groups (select type="checkbox") must have a groupnode with aria-labels
      const groupnode = test.qS('[data-wh-form-group-for="requiredradio"]');
      test.assert(!groupnode.classList.contains("wh-form__fieldgroup--error"));
      test.eq(false, groupnode.hasAttribute("aria-invalid"));
      test.eq(false, groupnode.hasAttribute("aria-describedby"));

      test.click('#coretest-showradioy'); //hid e Y

      test.assert(!test.qS('[data-wh-form-group-for="requiredradio"]').classList.contains("wh-form__fieldgroup--error"));
      test.click('#coretest-showradioy'); //show Y

      setRequiredFields(); //sets all fields (note: selects X)
      test.click('#coretest-requiredradio-y'); //and select Y again

      test.click('#coretest-showradioy'); //hide Y

      //submit should fail as we've made "Y" disappear
      test.click('#submitbutton');
      await test.wait('ui');

      //the CLIENT should have detected this..
      const errorinfo = test.getPxlLog(/^publisher:form.+/).at(-1);
      test.eq('client', errorinfo.data.ds_formmeta_errorsource);

      // select type="radio" must use a container with role="group"
      // on which to set the ARIA attributes
      test.eq("true", groupnode.getAttribute("aria-invalid"));
      const errornode = groupnode.querySelector('.wh-form__error');
      test.eq(errornode.id, groupnode.getAttribute("aria-describedby"));
    },

    'Test checkboxes min/max',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/?customemailvalidator=1');

      setRequiredFields();
      const formhandler = FormBase.getForNode(test.qS('#coreform'));

      //1 + 3 are now preselected (as defined in the formtest.formdef.xml)
      test.click('#coretest-checkboxes-2'); //adding 2 to the set
      test.click(test.qS('#submitbutton'));
      await test.wait('ui');

      let formevents = test.getPxlLog(/^publisher:form.*$/);
      test.eq(2, formevents.length, "Should be two PXL events now - one for start and one for failure");
      test.eq("publisher:formfailed", formevents[1].event);
      test.eq("checkboxes", formevents[1].data.ds_formmeta_errorfields);
      test.eq("client", formevents[1].data.ds_formmeta_errorsource);

      const checkboxgroup = test.qS('#coretest-checkboxes-2').closest('.wh-form__fieldgroup');
      test.assert(test.hasFocus(test.qS('#coretest-checkboxes-1')), "first focusable checkbox of this group should receive focus");
      test.eq('Kies maximaal 2 items.', checkboxgroup.querySelector('.wh-form__error').textContent);

      // The ARIA attributes are expected on the group container
      test.eq("true", checkboxgroup.getAttribute("aria-invalid"));
      let errornode = checkboxgroup.querySelector('.wh-form__error');
      test.eq(errornode.id, checkboxgroup.getAttribute("aria-describedby"));

      test.click('#coretest-checkboxes-3'); //deselecting #3
      await test.wait('ui'); //checkboxes don't update until UI is open (because it uses a validation handler) so wait for it..

      // Check wether all error indicators have been cleared
      test.eq('', checkboxgroup.querySelector('.wh-form__error').textContent);
      test.eq(false, checkboxgroup.hasAttribute("aria-invalid"));
      test.eq(false, checkboxgroup.hasAttribute("aria-describedby"));

      test.click('#coretest-checkboxes-3'); //selecting #3 - now expecting immediate responses
      await test.wait('ui');

      test.eq('Kies maximaal 2 items.', checkboxgroup.querySelector('.wh-form__error').textContent);
      test.eq("true", checkboxgroup.getAttribute("aria-invalid"));
      errornode = checkboxgroup.querySelector('.wh-form__error');
      test.eq(errornode.id, checkboxgroup.getAttribute("aria-describedby"));

      test.click('#coretest-checkboxes-3'); //deselecting #3 - now expecting immediate responses
      test.click('#coretest-checkboxes-2'); //deselecting #2 - now expecting immediate responses
      test.click('#coretest-checkboxes-1'); //deselecting #1 - now expecting immediate responses
      await test.wait('ui');
      test.eq('Kies minimaal 1 item.', checkboxgroup.querySelector('.wh-form__error').textContent);
      let result = await formhandler.validate(checkboxgroup);
      test.eq(checkboxgroup, result.firstfailed);
      test.assert(result.failed.length == 1);

      delete checkboxgroup.dataset.whMin; // Removing required number of selected checkboxes
      result = await formhandler.validate(checkboxgroup);
      test.eq(null, result.firstfailed);
      test.assert(result.failed.length == 0);

      test.click('#coretest-checkboxesvisible');
      test.qS('#coreformsubmitresponse').textContent = '';
      test.click(test.qS('#submitbutton'));
      await test.wait('ui');

      test.assert(JSON.parse(test.qS('#coreformsubmitresponse').textContent).form.agree, "expected successful submit");

      formevents = test.getPxlLog(/^publisher:form.*$/);
      test.eq(3, formevents.length, "Should be three PXL events now - one for start, one for failure and one for submission");
      test.eq("publisher:formsubmitted", formevents[2].event);
    },

    'Test server fallback error handling',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/');

      // Check for correct labeling
      test.eq('Textarea', test.qS('[for="coretest-textarea"]').textContent);

      test.eq(70, test.qS("textarea").maxLength);
      test.qS("textarea").removeAttribute("maxlength");
      test.fill("textarea", "This text is way too long. Yes it is way too long. Yes it is way too long. Yes it is way too long. 113 characters");
      setRequiredFields();

      test.click(test.qS('#submitbutton'));
      await test.wait('ui');

      test.assert(test.qS('[data-wh-form-group-for="textarea"]').classList.contains("wh-form__fieldgroup--error"), "should have failed serverside");

      // Serverside errors must also trigger the ARIA attributes
      const field = test.qS('#coretest-textarea');
      test.eq("true", field.getAttribute("aria-invalid"));

      const errornode = field.closest('.wh-form__fieldgroup').querySelector('.wh-form__error');
      test.eq(errornode.id, field.getAttribute("aria-describedby"));
    },

    'Test server side errors',
    async function () {
      await test.load(test.getTestSiteRoot() + 'testpages/formtest/');
      setRequiredFields();

      //set a rejected password
      test.fill("#coretest-password", "secret");

      test.click(test.qS('#submitbutton'));
      await test.wait('ui');

      if (test.qS("#coretest-password").value != "secret") {
        console.error('YOUR PASSWORD MANAGER CHANGED THE PASSWORD!\n\n'
          + `  For LastPass: Go to the LastPass Vault > Account Settings > Never URLs\n`
          + `  Add the URL ${test.getWin().location.origin}/webhare-testsuite.site/* to the "Never Do Anything" list\n\n`);
        throw new Error("YOUR PASSWORD MANAGER CHANGED THE PASSWORD! disable it!");
      }

      const field = test.qS("#coretest-password");
      test.eq("true", field.getAttribute("aria-invalid"));

      let errornode = field.closest('.wh-form__fieldgroup').querySelector('.wh-form__error');
      test.eq(errornode.id, field.getAttribute("aria-describedby"));

      test.assert(test.qS('[data-wh-form-group-for="password"]').classList.contains("wh-form__fieldgroup--error"));
      await test.pressKey('Tab');
      test.assert(test.qS('[data-wh-form-group-for="password"]').classList.contains("wh-form__fieldgroup--error"), "should still have error state after tab");
      await test.pressKey('Tab', { shiftKey: true });
      test.qS("#coretest-number").focus();
      test.assert(test.qS('[data-wh-form-group-for="password"]').classList.contains("wh-form__fieldgroup--error"), "should still have error state after focus");

      //change it, immediately clears the error
      test.fill("#coretest-password", "secret!");
      test.assert(!test.qS('[data-wh-form-group-for="password"]').classList.contains("wh-form__fieldgroup--error"));
      test.assert(!field.hasAttribute("aria-invalid"));
      test.assert(!field.hasAttribute("aria-describedby"));

      //submit that, fails again
      test.click(test.qS('#submitbutton'));
      await test.wait('ui');
      test.assert(test.qS('[data-wh-form-group-for="password"]').classList.contains("wh-form__fieldgroup--error"));
      test.assert(field.hasAttribute("aria-invalid"));
      errornode = field.closest('.wh-form__fieldgroup').querySelector('.wh-form__error');
      test.eq(errornode.id, field.getAttribute("aria-describedby"));

      //now set a _different_ field to allow the password
      test.qS("#coretest-number").focus();
      test.fill("#coretest-number", "-2");

      //stil in error, but should go OUT of error after submission
      test.assert(test.qS('[data-wh-form-group-for="password"]').classList.contains("wh-form__fieldgroup--error"));
      test.click(test.qS('#submitbutton'));
      await test.wait('ui');
      test.assert(!test.qS('[data-wh-form-group-for="password"]').classList.contains("wh-form__fieldgroup--error"));
    },

    { loadpage: test.getTestSiteRoot() + 'testpages/formtest/' },
    //the following tests only test the API (and for compatibility with parlsey). We can get through these tests without actually responding to the user (ie no triggers)
    {
      name: 'Test builtin validation API',
      test: async function (doc, win) {
        const formhandler = FormBase.getForNode(test.qS('#coreform'));
        test.eq(0, test.qSA('.wh-form__fieldgroup--error').length, "Form should be initially clean of errors");

        let result;
        result = await formhandler.validate([]); //empty set

        const emailgroup = test.qS('#coretest-email').closest('.wh-form__fieldgroup');
        test.assert(!emailgroup.classList.contains('wh-form__fieldgroup--error'));

        //if we set an error before we start validating...
        formhandler.setFieldError(test.qS('#coretest-email'), 'bad email field');
        //...don't show it. this is more consistent with html5 rendering
        test.assert(!emailgroup.classList.contains('wh-form__fieldgroup--error'));
        test.assert(!emailgroup.querySelector('.wh-form__error'));

        //but if we force show it...
        formhandler.setFieldError(test.qS('#coretest-email'), 'really bad email field', { reportimmediately: true });
        test.assert(emailgroup.classList.contains('wh-form__fieldgroup--error'));
        test.eq('really bad email field', emailgroup.querySelector('.wh-form__error').textContent);

        //and we can hide it
        formhandler.setFieldError(test.qS('#coretest-email'), '');
        test.assert(!emailgroup.classList.contains('wh-form__fieldgroup--error'));
        test.eq('', emailgroup.querySelector('.wh-form__error').textContent);

        result = await formhandler.validate(emailgroup);
        test.eq(test.qS('#coretest-email'), result.firstfailed);
        test.assert(result.failed.length == 1);
        test.assert(emailgroup.classList.contains('wh-form__fieldgroup--error'), 'email group should be marked as error');
        test.eq('Dit veld is verplicht.', emailgroup.querySelector('.wh-form__error').textContent);

        //test HTML5 custom validation
        result = await formhandler.validate(test.qS('#coretest-setvalidator'));
        test.assert(!result.valid, 'setvalidator should be seen as invalid');
        test.eq(test.qS('#coretest-setvalidator'), result.firstfailed);
        test.assert(result.failed.length == 1);

        //test custom errors
        test.qS('#coretest-email').value = 'klaas@example.org';
        result = await formhandler.validate(emailgroup);
        test.assert(result.valid);
        test.assert(!emailgroup.classList.contains('wh-form__fieldgroup--error'), 'email group should not be marked as error');

        formhandler.setFieldError(test.qS('#coretest-email'), 'bad email field', { reportimmediately: true });
        test.assert(emailgroup.classList.contains('wh-form__fieldgroup--error'), 'email group should be marked as error after explicit setFieldError');

        result = await formhandler.validate(emailgroup);
        test.assert(emailgroup.classList.contains('wh-form__fieldgroup--error'), 'revalidation may not clear explicit errors, as they have no callback to restore errors');

        formhandler.setFieldError(test.qS('#coretest-email'), null);
        test.assert(!emailgroup.classList.contains('wh-form__fieldgroup--error'), 'email group should be unmarked as error');
        test.qS('#coretest-email').value = '';

        // Test disabling by condition clearing validation errors
        test.fill("#coretest-condition_not", true);
        test.click(test.qS('#coretest-condition_not_required'));
        test.click(test.qS('#coretest-condition_not_enabled'));
        await test.wait('ui');
        test.assert(test.qS('#coretest-condition_not_required').classList.contains('wh-form__field--error'));
        test.fill("#coretest-condition_not", false);
        await test.wait('ui');
        test.assert(!test.qS('#coretest-condition_not_required').classList.contains('wh-form__field--error'));
      }
    },

    { loadpage: test.getTestSiteRoot() + 'testpages/formtest/?scrollzone=1' },

    {
      name: 'Test built-in validation far away validation',
      test: function (doc, win) {
        win.scrollTo(0, doc.documentElement.scrollHeight - win.innerHeight);
        test.assert(!test.canClick(test.qS('#coretest-email')), '#coretest-email should be out of sight');
        test.click(test.qS('.validatebutton'));
      },
      waits: ['ui']
    },
    {
      test: function (doc, win) {
        test.assert(test.canClick(test.qS('#coretest-email')), '#coretest-email should be back in of sight');
        test.assert(test.hasFocus(test.qS('#coretest-email')), '#coretest-email should have focus');
      }
    },

    {
      name: 'Test built-in validation not validating custom validated fields - SUBMIT button',
      loadpage: test.getTestSiteRoot() + 'testpages/formtest/'
    },

    async function (doc, win) {
      const setvalidatorgroup = test.qS('#coretest-setvalidator').closest('.wh-form__fieldgroup');
      test.assert(!setvalidatorgroup.classList.contains('wh-form__fieldgroup--error'));

      test.fill(test.qS('#coretest-email'), 'pietje@example.com');
      test.click(test.qS('#submitbutton'));
      await test.wait('ui');

      test.assert(setvalidatorgroup.classList.contains('wh-form__fieldgroup--error'), 'setvalidator not marked as failed');
      //make sure parlsey isn't causing injection errors
      test.eq("R<a>am", setvalidatorgroup.querySelector('.wh-form__error').textContent);
    },

    {
      name: 'Test built-in validation not validating custom validated fields - VALIDATE button',
      loadpage: test.getTestSiteRoot() + 'testpages/formtest/'
    },

    async function (doc, win) {
      const setvalidatorgroup = test.qS('#coretest-setvalidator').closest('.wh-form__fieldgroup');
      test.assert(!setvalidatorgroup.classList.contains('wh-form__fieldgroup--error'));

      test.fill(test.qS('#coretest-email'), 'pietje@example.com');
      test.click(test.qS('.validatebutton'));
      await test.wait('ui');

      test.assert(setvalidatorgroup.classList.contains('wh-form__fieldgroup--error'), 'setvalidator not marked as failed');
    },

    {
      name: 'Test rich validation errors',
      loadpage: test.getTestSiteRoot() + 'testpages/formtest/'
    },

    async function (doc, win) {
      const setvalidatorgroup = test.qS('#coretest-setvalidator').closest('.wh-form__fieldgroup');
      test.assert(!setvalidatorgroup.classList.contains('wh-form__fieldgroup--error'));

      test.fill(test.qS('#coretest-email'), 'pietje@example.com');
      test.fill(test.qS('#coretest-setvalidator'), 'richerror');
      test.click(test.qS('.validatebutton'));
      await test.wait('ui');

      test.assert(setvalidatorgroup.classList.contains('wh-form__fieldgroup--error'), 'setvalidator not marked as failed');
      test.eq("Rich Error", setvalidatorgroup.querySelector('.wh-form__error').textContent);
      test.eq("Rich Error", setvalidatorgroup.querySelector('.wh-form__error a').textContent);
    },

    {
      name: 'Test odd radio validation behaviour',
      loadpage: test.getTestSiteRoot() + 'testpages/formtest/'
    },
    async function (doc, win) {
      test.click(test.qS('#submitbutton'));
      await test.wait('ui');
      test.assert(test.qS('[data-wh-form-group-for="requiredradio"]').classList.contains('wh-form__fieldgroup--error'));
      test.click(test.qS('#coretest-requiredradio-y'));
      await test.wait('ui');

      test.assert(!test.qS('[data-wh-form-group-for="requiredradio"]').classList.contains('wh-form__fieldgroup--error'), "Error should be cleared immediately");
    },

    //load the page without initial checkboxes selected
    { loadpage: test.getTestSiteRoot() + 'testpages/formtest/?nocheckboxselect=1' },

    async function () {
      test.assert(!test.qS('[data-wh-form-group-for=checkboxes]').classList.contains("wh-form__fieldgroup--error"));
      test.click(test.qS('#submitbutton'));
      await test.wait('ui');
      test.assert(test.qS('[data-wh-form-group-for=checkboxes]').classList.contains("wh-form__fieldgroup--error"));
    },

    'Test async validation with SetupValidator',
    async function () {
      const setupdata = await test.invoke('mod::webhare_testsuite/lib/internal/testsite.whlib#BuildWebtoolForm',
        {
          jshandler: "webhare_testsuite:customform2",
          which: "custom2"
        });

      await test.load(setupdata.url);

      test.click('button[type=submit]');
      await test.wait('ui');
      test.eq('RPC not called yet', test.qS(`[name=textarea]`).closest('.wh-form__fieldgroup').querySelector(".wh-form__error").textContent);
    }
  ]);
