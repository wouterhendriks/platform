<?wh
/// @short Further record optimizer correctness tests

/*****************************************
 *
 * Does the record optimizer optmize too much?
 *****************************************/

LOADLIB "wh::internal/hsselftests.whlib";
LOADLIB "mod::webhare_testsuite/tests/baselibs/hsengine/testdata_recordoptimizer_loadlib.whlib";

RECORD FUNCTION recordid(RECORD a) { RETURN a; }

MACRO BasicTest()
{
  OpenTest("TestRecordOptimizer: BasicTest");

  RECORD a;
  INSERT CELL name:="test" INTO a;
  TestEq(a.name, "test");

  RECORD b := a;
  TestEq(b.name, "test");

  RECORD c := recordid(a);
  TestEq(c.name, "test");

  RECORD ARRAY d; INSERT c INTO d AT END;
  TestEq(d[0].name, "test");

  RECORD ARRAY e; INSERT c INTO e AT END;
  TestEq(e.name, "test");

  RECORD f;
  INSERT CELL nested := c INTO f;
  TestEq(f.nested.name, "test");

  CloseTest("TestRecordOptimizer: BasicTest");
}

RECORD global1;
RECORD global2;
RECORD global3;
RECORD ARRAY global4;
RECORD ARRAY global5;
RECORD global6;

MACRO GlobalTest_1()
{
  INSERT CELL name := "test" INTO global1;
  global2 := global1;
  global3 := recordid(global2);
  INSERT global3 INTO global4 AT END;
  INSERT global3 INTO global5 AT END;
  INSERT CELL nested := global3 INTO global6;
}

MACRO GlobalTest_2()
{
  TestEq(global1.name, "test");
  TestEq(global2.name, "test");
  TestEq(global3.name, "test");
  TestEq(global4[0].name, "test");
  TestEq(global5.name, "test");
  TestEq(global6.nested.name, "test");
}

MACRO GlobalTest()
{
  OpenTest("TestRecordOptimizer: BasicTest");

  GlobalTest_1();
  GlobalTest_2();

  CloseTest("TestRecordOptimizer: BasicTest");
}

RECORD FUNCTION ReturnTestFunc()
{
  RECORD a;
  INSERT CELL name := "test" INTO a;
  RETURN a;
}

MACRO ReturnTest()
{
  OpenTest("TestRecordOptimizer: ReturnTest");

  TestEq(ReturnTestFunc().name, "test");

  CloseTest("TestRecordOptimizer: ReturnTest");
}

MACRO LoadlibTest()
{
  OpenTest("TestRecordOptimizer: LoadlibTest");

  TestEq(public1.name, "test");
  TestEq(public2.name, "test");
  TestEq(public2[0].name, "test");
  TestEq(public3.nested.name, "test");

  CloseTest("TestRecordOptimizer: LoadlibTest");
}

MACRO ConcatTest()
{
  OpenTest("TestRecordOptimizer: ConcatTest");

  RECORD ARRAY A;
  RECORD C;
  INSERT CELL id := 7 INTO C;
  INSERT C INTO A AT 0;

  RECORD ARRAY B := A CONCAT [[ xkey := 8 ]];

  TestEq(B[0].id, 7);
  TestEq(B[1].xkey, 8);

  CloseTest("TestRecordOptimizer: ConcatTest");
}

PRINT("\n === Running TestRecordOptimizer\n");
BasicTest();
GlobalTest();
ReturnTest();
LoadlibTest();
ConcatTest();
