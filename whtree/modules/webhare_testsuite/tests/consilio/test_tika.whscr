<?wh

LOADLIB "wh::crypto.whlib";
LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";

LOADLIB "mod::consilio/lib/parsers/tika.whlib";

LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/testframework.whlib";


MACRO TestTika()
{
  ClearTikaCache();

  /* to test this yourself:
     /usr/local/Cellar/openjdk/18/bin/java -jar $(wh getmoduledir system)/data/engines/tika-app.jar --config=$(wh getmoduledir system)/data/engines/tika-default-config.xml --html $(wh getmoduledir webhare_testsuite)/tests/baselibs/hsengine/data/testole_plaindoc.doc | less
  */
  OBJECT doc := ParseUsingTIKA(GetWebhareResource("mod::webhare_testsuite/tests/baselibs/hsengine/data/testole_plaindoc.doc"));

  //verify metadata prop extraction
  TestEq("tag 1", doc->QuerySelector(`meta[name="custom:Mijn eigen tag"]`)->GetAttribute("content"));
  //verify text extraction
  TestEq("Handleiding WebHare Professional", TrimWhitespace(doc->QuerySelector("h1")->textcontent));

  //verify separate pages being returned
  BLOB resource := GetWebhareResource("mod::webhare_testsuite/tests/baselibs/hsengine/data/loremipsum.pdf");
  RECORD docrec := ParseTikaPage(resource, TRUE);
  TestEq(10, Length(docrec.pages));

  resource := GetWebhareResource("mod::webhare_testsuite/tests/baselibs/hsengine/data/testole_plaindoc.doc");
  docrec := ParseTikaPage(resource, TRUE);
  TestEq("Handleiding WebHare Professional", docrec.title);
  TestEqLike("*In deze handleiding wordt u stap voor stap door de interface van WebHare Professional geleid.*", docrec.text);
  // No separate pages returned for Word documents
  TestEq(0, Length(docrec.pages));

  // Test if the cache file is present
  STRING hash := ToLowercase(EncodeBase16(GetHashForBlob(resource, "SHA-256")));
  STRING path := MergePath(__GetTikaCacheRoot(), Left(hash, 2) || "/" || SubString(hash, 2) || ".hson");
  TestEq(TRUE, RecordExists(GetDiskFileProperties(path)));

  // Modify the cache file to be sure it is used
  RECORD data := DecodeHSONBlob(GetDiskResource(path));
  data.data.title := "modified title";
  StoreDiskFile(path, EncodeHSONBLob(data), [ overwrite := TRUE ]);

  // Should be read from the cache
  docrec := ParseTikaPage(resource, TRUE);
  TestEq("modified title", docrec.title);

  // Backdate the file to check lastuse being updated
  data.data.title := "modified title #2";
  data.creationdate := AddDaysToDate(-1, data.creationdate);
  data.lastuse := data.creationdate;
  StoreDiskFile(path, EncodeHSONBLob(data), [ overwrite := TRUE ]);

  // Should be read from the cache
  DATETIME lastuse_must_exceed := GetCurrentDatetime();
  docrec := ParseTikaPage(resource, TRUE);
  TestEq("modified title #2", docrec.title);

  data := DecodeHSONBlob(GetDiskResource(path));
  TestEq(TRUE, data.lastuse >= lastuse_must_exceed, "verified lastusedate updated");
}

RunTestframework([ PTR TestTika
                 ]);
