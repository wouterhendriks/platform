<?wh

LOADLIB "wh::datetime.whlib";

LOADLIB "mod::consilio/lib/api.whlib";
LOADLIB "mod::consilio/lib/catalogs.whlib";
LOADLIB "mod::consilio/lib/internal/dbschema.whlib";
LOADLIB "mod::consilio/lib/internal/opensearch.whlib";
LOADLIB "mod::consilio/lib/internal/indexmanager.whlib";
LOADLIB "mod::consilio/lib/internal/support.whlib";
LOADLIB "mod::consilio/lib/internal/updateindices.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/testframework.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

RECORD testindex1, testindex2;
//Views straight to the two separate catalogs..
OBJECT cat_index1_view, cat_index2_view;

MACRO Prepare()
{
  OBJECT cat;

  testfw->BeginWork();
  cat := CreateConsilioCatalog("consilio:testfw_testindex_suffixed", [ fieldgroups := [ "webhare_testsuite:nosuchfieldyet" ]
                                                                     , managed := FALSE
                                                                     , suffixed := TRUE
                                                                     , priority := 9
                                                                     ]);

  INTEGER index1id := cat->AttachIndex(0);
  testindex1 := cat->ListAttachedIndices()[0];
  TestEqLike("c_??*", testindex1.indexname, "test for properly structured builtin indexname");

  STRING index2name := Left(testindex1.indexname, Length(testindex1.indexname) - 3) || "_b2";
  cat->AttachIndex(0, [ indexname := index2name]); //add a second instance.. use initial indexname to still ensure uniqueness
  testindex2 := cat->ListAttachedIndices()[1];

  TestEqMembers([[ id := index1id
                 , searchpriority := 100
                 , indexname := testindex1.indexname
                 ]
                ,[ searchpriority := 0
                 , indexname := index2name
                 , isbuiltin := TRUE
                 ]
                ], cat->ListAttachedIndices(), "*");
  TestEq(RECORD[], cat->ListSuffixes());

  cat_index1_view := CreateConsilioCatalog("consilio:testfw_index1_view", [ managed := FALSE, suffixed := TRUE ]);
  cat_index1_view->AttachIndex(testindex1.indexmanager, [ indexname := testindex1.indexname ]);
  cat_index2_view := CreateConsilioCatalog("consilio:testfw_index2_view", [ managed := FALSE, suffixed := TRUE ]);
  cat_index2_view->AttachIndex(testindex2.indexmanager, [ indexname := testindex2.indexname ]);

  Print(`${cat->GetStorageInfo()}, ${cat_index1_view->GetStorageInfo()}, ${cat_index2_view->GetStorageInfo()}\n`);
  TestEq(testindex1.indexname, cat_index1_view->ListAttachedIndices()[0].indexname);
  TestEq(testindex2.indexname, cat_index2_view->ListAttachedIndices()[0].indexname);
  TestEq(index2name, cat_index2_view->ListAttachedIndices()[0].indexname);

  testfw->CommitWork();
}

MACRO TestUnmanagedActions()
{
  OBJECT cat := OpenConsilioCatalog("consilio:testfw_testindex_suffixed");

  TestEq(RECORD[], cat->ListSuffixes());

  cat->ApplyConfiguration( [ suffixes := ["sfx1","sfx2"] ]);

  //Verify index creation
  TestEq([[ suffix := "sfx1" ], [ suffix := "sfx2"] ], SELECT * FROM cat_index1_view->ListSuffixes() ORDER BY suffix);
  TestEq([[ suffix := "sfx1" ], [ suffix := "sfx2"] ], SELECT * FROM cat_index2_view->ListSuffixes() ORDER BY suffix);
  TestEq([[ suffix := "sfx1" ], [ suffix := "sfx2"] ], SELECT * FROM cat->ListSuffixes() ORDER BY suffix);
}

RunTestframework(
    [ PTR Prepare
    , PTR TestUnmanagedActions
    ]);
