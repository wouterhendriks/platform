import * as test from '@webhare/test';
import * as services from '@webhare/services';

// import * as util from 'node:util';
import * as child_process from 'node:child_process';

async function testChecks() {
  //test.throws should fail if a function did not throw. this will generate noise so tell the user to ignore
  test.throws(/Lemme throw/, () => { throw new Error("Lemme throw"); });
  test.throws(/Expected function to throw/, () => test.throws(/Fourty two/, () => 42));
  console.log("(you can ignore the message above about expecting a fourty two exception)");

  test.eq(new Date("2023-01-01"), new Date("2023-01-01"));
  test.eq({ deep: new Date("2023-01-01") }, { deep: new Date("2023-01-01") });
  test.eqProps({ deep: new Date("2023-01-01") }, { deep: new Date("2023-01-01") });
  test.throws(/Expected date/, () => test.eq(new Date("2023-01-02"), new Date("2023-01-01")));
  test.throws(/Expected date/, () => test.eq({ deep: new Date("2023-01-02") }, { deep: new Date("2023-01-01") }));
  test.throws(/Expected date/, () => test.eqProps({ deep: new Date("2023-01-02") }, { deep: new Date("2023-01-01") }));

  test.eq(/konijntje/, "Heb jij mijn konijntje gezien?");
  test.eqProps(/konijntje/, "Heb jij mijn konijntje gezien?");
  test.eq({ text: /konijntje/ }, { text: "Heb jij mijn konijntje gezien?" });
  test.eqProps({ text: /konijntje/ }, { text: "Heb jij mijn konijntje gezien?" });
  test.throws(/Expected match/, () => test.eq({ text: /Konijntje/ }, { text: "Heb jij mijn konijntje gezien?" }), "We should be case sensitive");
  test.throws(/Expected match/, () => test.eqProps({ text: /Konijntje/ }, { text: "Heb jij mijn konijntje gezien?" }));
  ///@ts-ignore TS also rejects the regexp on the RHS
  test.throws(/Expected type/, () => test.eq({ text: "Heb jij mijn konijntje gezien?" }, { text: /konijntje/ }), "Only 'expect' is allowed to hold regexes");

  const x_ab = { cellA: "A", cellB: "B" };
  const x_abc = { ...x_ab, cellC: "test" };

  test.eqProps(x_ab, x_ab);
  test.eqProps(x_ab, x_abc);
  test.eqProps(x_abc, x_ab, ["cellC"], "shouldn't throw if cellC is explicitly ignored");
  test.throws(/Expected property 'cellC'.*at root/, () => test.eqProps(x_abc, x_ab));

  const x_abc_badb = { ...x_abc, cellB: "BAD" };
  test.eqProps(x_abc, x_abc_badb, ["cellB"], "shouldn't throw if cellB is explicitly ignored");
  test.throws(/Mismatched value at root.cellB/, () => test.eqProps(x_abc, x_abc_badb));

  {

    const v_ts = await test.loadTSType(`@mod-webhare_testsuite/tests/system/nodejs/test_tests.ts#MyInterface`);
    test.throws(/data does not conform to the structure: "\/b" must be string/, () => v_ts.validateStructure({ a: 0, b: 1 }), "wrong type not detected");
    test.throws(/must NOT have additional properties/, () => v_ts.validateStructure({ a: 0, b: "a", c: "1" }), "extra property not detected");
    test.throws(/must have required property 'b'/, () => v_ts.validateStructure({ a: 0 }), "missing property not detected");
    v_ts.validateStructure({ a: 0, b: "a" });
  }

  {
    const v_ts_allow_extra = await test.loadTSType(`@mod-webhare_testsuite/tests/system/nodejs/test_tests.ts#MyInterface`, { noExtraProps: false, required: false });
    v_ts_allow_extra.validateStructure({ a: 0, c: "1" });
  }

  {
    const v_js = await test.loadJSONSchema({ "type": "object", "properties": { "a": { "type": "number" }, "b": { "type": "string" }, "d": { "type": "string", "format": "date-time" } }, "$schema": "http://json-schema.org/draft-07/schema#" });
    v_js.validateStructure({ a: 0, c: "1", d: "2000-01-01T12:34:56Z" });
    test.throws(/data does not conform to the structure: "\/b" must be string/, () => v_js.validateStructure({ a: 0, b: 1 }), "wrong type not detected");
    test.throws(/data does not conform to the structure: "\/d" must match format "date-time"/, () => v_js.validateStructure({ a: 0, d: "test" }), "wrong format not detected");
  }

  {
    const v_jsf = await test.loadJSONSchema("@mod-webhare_testsuite/tests/system/nodejs/data/test.schema.json");
    test.throws(/data does not conform to the structure: "\/b" must be string/, () => v_jsf.validateStructure({ a: 0, b: 1 }), "wrong type not detected");
    test.throws(/must NOT have additional properties/, () => v_jsf.validateStructure({ a: 0, b: "a", c: "1" }), "extra property not detected");
    test.throws(/must have required property 'b'/, () => v_jsf.validateStructure({ a: 0 }), "missing property not detected");
    test.throws(/data does not conform to the structure: "\/d" must match format "date-time"/, () => v_jsf.validateStructure({ a: 0, b: "a", d: "test" }), "wrong format not detected");
    v_jsf.validateStructure({ a: 0, b: "a", d: "2000-01-01T12:34:56Z" });
  }


  {
    const start = Date.now();
    await test.throws(/test.wait timed out after 10 ms/, () => test.wait(() => false, { timeout: 10 }));
    const waited = Date.now() - start;
    //it did fail once with 9ms, perhaps some rounding? take 9 to be safe...
    test.assert(waited >= 9, `test.wait didn't wait at least 10ms, but ${waited}ms`);
  }

  {
    const start = Date.now();
    await test.throws(/test.wait timed out after 10 ms/, () => test.wait(new Promise(() => null), { timeout: 10 }));
    const waited = Date.now() - start;
    test.assert(waited >= 9, `test.wait didn't wait at least 10ms, but ${waited}ms`);
  }

  {
    const start = Date.now();
    await test.throws(/test.wait timed out after 10 ms/, () => test.wait(() => Promise.resolve(false), { timeout: 10 }));
    const waited = Date.now() - start;
    test.assert(waited >= 9, `test.wait didn't wait at least 10ms, but ${waited}ms`);
  }

  await test.wait(new Promise(resolve => resolve({ a: 1 })));
  await test.wait(new Promise(resolve => resolve(false)));
  await test.wait(() => Promise.resolve(true));

  {
    test.typeAssert<test.Assignable<number, 2>>();
    // @ts-expect-error -- Can't assign a number to 2
    test.typeAssert<test.Assignable<2, number>>();

    test.typeAssert<test.Extends<2, number>>();
    // @ts-expect-error -- Number doesn't extend 2
    test.typeAssert<test.Extends<number, 2>>();

    test.typeAssert<test.Equals<1, 1>>();
    test.typeAssert<test.Equals<{ a: 1; b: 2 }, { a: 1; b: 2 }>>();

    // @ts-expect-error -- Can't assign a number to 2
    test.typeAssert<test.Equals<number, 1>>();
    // @ts-expect-error -- Can't assign a number to 2
    test.typeAssert<test.Equals<1, number>>();
    // @ts-expect-error -- Can't assign 2 to 1
    test.typeAssert<test.Assignable<1, 2>>();

    // @ts-expect-error -- Can't assign a number to 2
    test.typeAssert<test.Equals<number, 1>>();
    // @ts-expect-error -- Can't assign a number to 2
    test.typeAssert<test.Equals<1, number>>();
    // @ts-expect-error -- Can't assign 2 to 1
    test.typeAssert<test.Assignable<1, 2>>();

  }
}

// Referenced by file#symbol reference in the loadTSType call above
// eslint-disable-next-line @typescript-eslint/no-unused-vars
interface MyInterface {
  a: number;
  b: string;
}

async function runWHTest(testname: string): Promise<string> {
  /* TODO: a much better approach would use child_process.spawn and pipes, merge the stdout&stderr pipe (so there are no ordering issues) and also watch the exit code */
  return new Promise(resolve =>
    child_process.execFile(services.config.installationroot + "bin/wh", ["runtest", testname], { timeout: 30000 }, function (error, stdout, stderr) {
      // console.log({error, stdout, stderr});
      resolve(stdout + stderr);
    }));
}

async function checkTestFailures() {
  test.eq(/test\.assert failed.*metatest_shouldfail.ts line 4.*Offending test: test\.assert\(Math\.random\(\) === 42\);/s, await runWHTest("system.nodejs.meta.metatest_shouldfail"));
}

test.run([
  testChecks,
  checkTestFailures
]);
