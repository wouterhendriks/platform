import * as test from "@webhare/test";
import * as services from "@webhare/services";
import { HTTPMethod } from "@webhare/router";
import { coreWebHareRouter } from "@webhare/router/src/corerouter";
import { decodeHSON } from "@webhare/hscompat/hscompat";
import { IncomingWebRequest, newForwardedWebRequest } from "@webhare/router/src/request";

interface GetRequestDataResponse {
  method: string;
  webvars: Array<{ ispost: boolean; name: string; value: string }>;
}

function testWebRequest() {
  let req = new IncomingWebRequest(services.config.backendURL);
  test.eq(services.config.backendURL, req.url.toString());
  test.eq(services.config.backendURL, req.baseURL);
  test.eq("", req.localPath);

  req = new IncomingWebRequest(services.config.backendURL + "sub%20URL/dir/f?hungry4=Spicy");
  test.eq("Spicy", req.url.searchParams.get("hungry4"));
  test.eq(null, req.url.searchParams.get("Hungry4"), "On the JS side we're case sensitive");
  test.eq(services.config.backendURL, req.baseURL);
  test.eq("sub url/dir/f", req.localPath);

  test.throws(/original base/, () => newForwardedWebRequest(req, "sub URL/"));
  test.throws(/original base/, () => newForwardedWebRequest(req, "sub url/"));
  test.throws(/original base/, () => newForwardedWebRequest(req, "sub%20url/"));
  test.throws(/search/, () => newForwardedWebRequest(req, "sub%20URL/dir/f?hungry4"));

  const req2 = newForwardedWebRequest(req, "sub%20URL/");
  test.eq("Spicy", req2.url.searchParams.get("hungry4"));
  test.eq(services.config.backendURL + "sub%20URL/", req2.baseURL);
  test.eq("dir/f", req2.localPath);

  const req3 = newForwardedWebRequest(req2, "dir/");
  test.eq("Spicy", req3.url.searchParams.get("hungry4"));
  test.eq(services.config.backendURL + "sub%20URL/dir/", req3.baseURL);
  test.eq("f", req3.localPath);
}

async function testHSWebserver() {
  const testsuiteresources = services.config.backendURL + "tollium_todd.res/webhare_testsuite/tests/";
  let result = await coreWebHareRouter(new IncomingWebRequest(testsuiteresources + "getrequestdata.shtml"));
  test.eq(200, result.status);
  test.eq("application/x-hson", result.getHeader("content-type"));

  let response = decodeHSON(await result.text()) as unknown as GetRequestDataResponse;
  test.eq("GET", response.method);

  result = await coreWebHareRouter(new IncomingWebRequest(testsuiteresources + "getrequestdata.shtml", {
    method: HTTPMethod.POST,
    headers: { "content-type": "application/x-www-form-urlencoded", accept: "application/json" },
    body: "a=1&b=2"
  }));

  test.eq(200, result.status);
  test.eq("application/json", result.getHeader("content-type"));

  response = await result.json() as GetRequestDataResponse;
  test.eq("POST", response.method);
  test.eqProps([{ name: 'a', value: '1' }, { name: 'b', value: '2' }], response.webvars);

  //Get a binary file
  result = await coreWebHareRouter(new IncomingWebRequest(testsuiteresources + "rangetestfile.jpg"));
  //FIXME we also want a blob() interface - and that one to be smart enough to pipe-through huge responses
  test.eq("c72d48d291273215ba66fc473a4075de1de02f94", Buffer.from(await crypto.subtle.digest("SHA-1", await result.arrayBuffer())).toString('hex'));
}

async function testJSBackedURLs() {
  const baseURL = services.config.backendURL + ".webhare_testsuite/tests/js/";
  let fetchresult = await fetch(baseURL);
  let jsonresponse = await fetchresult.json();

  test.eq(400, fetchresult.status);
  test.eq("Invalid request", jsonresponse.error);

  fetchresult = await fetch(baseURL + "?type=debug");
  jsonresponse = await fetchresult.json();

  test.eq(200, fetchresult.status);
  test.eq(true, jsonresponse.debug);
  test.eq(baseURL + "?type=debug", jsonresponse.url);
  test.eq(baseURL, jsonresponse.baseURL);
  test.eq("", jsonresponse.localPath);

  fetchresult = await fetch(baseURL + "Sub%20Url?type=debug");
  jsonresponse = await fetchresult.json();

  test.eq(baseURL + "Sub%20Url?type=debug", jsonresponse.url);
  test.eq(baseURL, jsonresponse.baseURL);
  test.eq("sub url", jsonresponse.localPath);

  fetchresult = await fetch(baseURL + "Sub%20Url?type=debug", { method: "post", headers: { "x-test": "42" }, body: "a=1&b=2" });
  jsonresponse = await fetchresult.json();
  test.eq(baseURL + "Sub%20Url?type=debug", jsonresponse.url);
  test.eq(baseURL, jsonresponse.baseURL);
  test.eq("sub url", jsonresponse.localPath);
  test.eq("42", jsonresponse.headers["x-test"]);
  test.eq("a=1&b=2", jsonresponse.text);

  const mixedcase_baseUrl = services.config.backendURL + ".webhare_Testsuite/TESTs/js/";
  fetchresult = await fetch(mixedcase_baseUrl + "Sub%20Url?type=debug");
  jsonresponse = await fetchresult.json();

  test.eq(mixedcase_baseUrl + "Sub%20Url?type=debug", jsonresponse.url);
  test.eq(mixedcase_baseUrl, jsonresponse.baseURL);
  test.eq("sub url", jsonresponse.localPath);

  //Follow an ambiguous URL due to a caller having its own opinion about URL encoding
  //TODO It would be nice to be able to support it in the JS webserver, but HS webserver mangles the URL too much
  const badbaseUrl = services.config.backendURL + ".webhare_testsuite/tests/j%73/"; //%73=s
  fetchresult = await fetch(badbaseUrl + "Sub%20Url?type=debug");
  test.eq(400, fetchresult.status);
}

test.run([
  testWebRequest,
  testHSWebserver,
  testJSBackedURLs
]);
