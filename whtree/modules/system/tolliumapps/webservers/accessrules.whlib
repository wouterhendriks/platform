﻿<?wh
LOADLIB "wh::internet/tcpip.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::tollium/lib/dialogs.whlib";
LOADLIB "mod::tollium/lib/screenbase.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/internal/moduledefparser.whlib";
LOADLIB "mod::system/lib/internal/resourcemanager.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/webserver/config.whlib";
LOADLIB "mod::system/tolliumapps/webservers/support.whlib";


RECORD ARRAY FUNCTION GetAvailableModuleRoleSets()
{
  RECORD ARRAY retval;

  FOREVERY(RECORD mod FROM (SELECT * FROM GetWebHareModules() ORDER BY name))
  {
    OBJECT xmlfile := GetModuleDefinitionXML(mod.name);
    retval := retval CONCAT ListModuleWebruleSets(GetModuleDefinitionXMLResourceName(mod.name), xmlfile);
  }

  retval := SELECT *, title := GetTid(title) ?? rowkey FROM retval;
  RETURN SELECT * FROM retval ORDER BY ToUppercase(title);
}


PUBLIC STATIC OBJECTTYPE AccessRules EXTEND TolliumScreenBase
<
  INTEGER webserverid;
  //record rule ids which are temporarily forced shown until the filter is modified again
  INTEGER ARRAY addtofilterresults;
  STRING servername;
  RECORD ARRAY rows;

  MACRO Init(RECORD params)
  {
    params := ValidateOptions([ id := 0, openaccessrule := 0 ], params);

    this->webserverid := params.id;
    IF(this->webserverid != 0)
      this->servername := SELECT AS STRING UnpackURL(baseurl).host FROM system.webservers WHERE id = this->webserverid;

    this->Reload();
    IF(params.openaccessrule != 0)
    {
      IF(^accessrulelist->SetValueIfValid(INTEGER[params.openaccessrule]))
        this->DoEdit();
    }
  }

  RECORD ARRAY FUNCTION GetAccessRules()
  {
    // Get all unique accesrule id's which have an ip rule
    INTEGER ARRAY ruleswithips := SELECT AS INTEGER ARRAY DISTINCT(accessid) FROM system.access_ips;
    RECORD ARRAY rolesets := GetAvailableModuleRoleSets();

    RECORD ARRAY rows := SELECT TEMPORARY type := (authtype > 0 OR authlist OR authscript != "" OR id IN ruleswithips) ? "login"
                                                  : hostingsrc = 4 ? "redirect"
                                                  : hostingsrc = 5 ? "redirect-keep-suburl"
                                                  : hostingsrc = 6 ? "module"
                                                  : hostingsrc = 2 ? "folder"
                                                  : ""
                              , rowkey := id
                              , icon := hostingsrc IN [4,5] ? (redirectcode = 301 ? 4 : 5)  //for these, we take icon based on permanent/temp status
                                                            : SearchElement([ "", "login", "folder","module" ], type) //for these we go just by the type
                              , matchtype := matchtype = 0 ? "exact" : matchtype = 1 ? "initial" : matchtype = 2 ? "wildcard" : ""
                              , path
                              , type := type
                              , webhareusers := (authtype = 2)
                              , externalusers := authlist
                              , description
                              , disabled
                              , listrowclasses := disabled ? ["grayedout"] : STRING[]
                              , target := hostingsrc in [2,4,5,6] ? hostingpath : ""
                              , hostingsrc
                           FROM system.access
                          WHERE access.webserver = this->webserverid;

    //Set readable names for module-sourced
    UPDATE rows SET target := (SELECT AS STRING title FROM rolesets WHERE rowkey = rows.target) ?? rows.target
              WHERE hostingsrc = 6;

    RETURN rows;
  }

  MACRO Reload()
  {
    RECORD ARRAY rows := SELECT *
                              , canview := this->webserverid != 0 AND matchtype IN [ "exact", "initial" ]
                              , listrowclasses := disabled ? [ "grayedout" ] : STRING[]
                           FROM this->GetAccessRules();
    this->rows := rows;
    this->Refresh();
  }

  MACRO Refresh()
  {
    RECORD ARRAY toshow := this->rows;
    IF(^filter->value != "")
    {
      toshow := SELECT *
                 FROM toshow
                WHERE rowkey IN this->addtofilterresults
                      OR ToUppercase(path || " " || target || " " || description) LIKE ToUppercase("*" || ^filter->value || "*");
    }

    IF(Length(toshow) = 0)
      ^accessrulelist->empty := ^filter->value != "" ? this->GetTid(".empty-nomatchfor" , ^filter->value) : this->GetTid(".empty-norules");

    ^accessrulelist->rows := toshow;
  }

  MACRO DoAdd()
  {
    INTEGER newrule := this->RunScreen("#updateaccessrule", [ id := 0, webserver := this->webserverid ] );
    IF(newrule != 0)
    {
      INSERT newrule INTO this->addtofilterresults AT END;
      this->Reload();
      ^accessrulelist->value := [ newrule ];
    }
  }

  MACRO DoEdit()
  {
    INTEGER editrule := this->RunScreen("#updateaccessrule", [ id := ^accessrulelist->value[0], webserver := this->webserverid ] );
    IF(editrule != 0)
    {
      INSERT editrule INTO this->addtofilterresults AT END;
      this->Reload();
      ^accessrulelist->value := [ editrule ];
    }
  }

  MACRO DoView(OBJECT urlreceiver)
  {
    STRING webserverroot := SELECT AS STRING baseurl FROM system.webservers WHERE id = this->webserverid;
    urlreceiver->SendURL(webserverroot || Substring(^accessrulelist->selection[0].path,1));
  }

  MACRO DoIPFilters()
  {
    OBJECT screen := this->LoadScreen(".ipfilters", [ id := ^accessrulelist->value[0] ] );

    IF (screen->RunModal() = "ok")
      this->Reload();
  }

  MACRO DoWebhareUsers()
  {
    OBJECT screen := this->LoadScreen(".webhareusers", [ id := ^accessrulelist->value[0] ] );

    IF (screen->RunModal() = "ok")
      this->Reload();
  }

  MACRO DoExternalUsers()
  {
    OBJECT screen := this->LoadScreen(".externalusers", [ id := ^accessrulelist->value[0] ] );

    IF (screen->RunModal() = "ok")
      this->Reload();
  }

  MACRO DoDelete()
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".deleteaccessrule")) != "yes")
      RETURN;

    OBJECT work := this->beginwork();

    DELETE FROM system.access
           WHERE id IN ^accessrulelist->value;

    IF (NOT work->Finish())
      RETURN;

    ReloadWebhareConfig(TRUE, FALSE);

    this->Reload();
  }

  MACRO DoExportRules()
  {
    STRING exporttitle;
    IF(this->webserverid != 0)
      exporttitle := this->GetTid(".export-rulesfor", this->servername);
    ELSE
      exporttitle := this->GetTid(".export-globalrules");

    RECORD ARRAY rows := SELECT *
                              , enabled := NOT disabled
                           FROM this->GetAccessRules()
                       ORDER BY ToUppercase(path), rowkey;

    RunColumnFileExportDialog(this,
         CELL[ rows
             , columns := [ [ name := "type",        type := "string", title := this->GetTid(".columns.type") ]
                          , [ name := "matchtype",   type := "string", title := this->GetTid(".columns.matchtype") ]
                          , [ name := "path",        type := "string", title := this->GetTid(".columns.path") ]
                          , [ name := "target",      type := "string", title := this->GetTid(".columns.target") ]
                          , [ name := "enabled",     type := "boolean", title := this->GetTid(".columns.enabled") ]
                          , [ name := "description", type := "string", title := this->GetTid(".columns.description") ]
                          ]
             , exporttitle
             ]);
  }

  MACRO OnFilterChange()
  {
    this->addtofilterresults := INTEGER[];
    this->Refresh();
  }
>;

PUBLIC STATIC OBJECTTYPE UpdateAccessRule EXTEND TolliumScreenBase
<
  INTEGER accessid;
  INTEGER webserverid;

  MACRO Init(RECORD params)
  {
    this->accessid := params.id;
    this->webserverid := params.webserver;
    ^alternatepath->startfolder := GetWebserverBaseOutputFolder();
    ^singlefilepath->startfolder := GetWebserverBaseOutputFolder();
    ^errorpath->startfolder := GetWebserverBaseOutputFolder();

    RECORD accessrec := SELECT * FROM system.access WHERE id = this->accessid;

    ^hostingsrc->value := "0";
    ^installmodule->options := [[rowkey:="",title:="",invalidselection:=TRUE]] CONCAT GetAvailableModuleRoleSets();

    IF(this->webserverid = 0)
      ^appliesto->value := this->GetTid(".allservers");
    ELSE
      ^appliesto->value := SELECT AS STRING baseurl FROM system.webservers WHERE id=this->webserverid;

    IF (RecordExists(accessrec))
    {
      ^enabled->value := NOT accessrec.disabled;
      ^path->value := accessrec.path;
      ^errorpath->value := FormatOutputPath(accessrec.errorpath);
      ^authtype->value := accessrec.authtype;
      ^authrequirement->value := accessrec.authrequirement;
      this->SetHostingPath(accessrec.hostingsrc, accessrec.hostingpath, accessrec.redirectcode);
      ^authlist->value := accessrec.authlist;
      ^externalscript->value := accessrec.authscript != "";
      ^externalscriptname->value := accessrec.authscript;
      ^description->value := accessrec.description;
      ^matchtype->value := accessrec.matchtype;
      ^disablecaching->value := accessrec.disablecaching;
      ^maxage->value := accessrec.maxage;
      ^fixcase->value := accessrec.fixcase;
    }
  }

  STRING FUNCTION GetHostingPath()
  {
    SWITCH (^hostingsrc->value)
    {
      CASE "2"
      {
        STRING path := UnmapOutputPath(^alternatepath->value);
        IF(path NOT LIKE "*/")
          path := path || "/";
        RETURN path;
      }
      CASE "3" { RETURN UnmapOutputPath(^singlefilepath->value); }
      CASE "4"
      {
        //Add slash if urlpath is empty and doesnt even have the slash to prevent 'http://nu.nl' as redirect (#1162)
        RETURN ^redirectpath->value || (UnpackURL(^redirectpath->value).urlpathslash ? "" : "/");
      }
      CASE "6" { RETURN ^installmodule->value; }
    }

    RETURN "";
  }

  MACRO SetHostingPath(INTEGER hostingsrc, STRING hostingpath, INTEGER redirectcode)
  {
    SWITCH (hostingsrc)
    {
      CASE 2 { ^alternatepath->value := FormatOutputPath(hostingpath); ^hostingsrc->value := "2"; }
      CASE 3 { ^singlefilepath->value := FormatOutputPath(hostingpath); ^hostingsrc->value := "3"; }
      CASE 4,5
      {
        ^redirectpath->value := hostingpath;
        ^hostingsrc->value := "4";
        ^keepsubpath->value := hostingsrc=5;
        IF(^redirectcode->IsValidValue(redirectcode))
          ^redirectcode->value := redirectcode;
      }
      CASE 6
      {
        IF(^installmodule->IsValidValue(hostingpath))
          ^installmodule->value := hostingpath;
        ^hostingsrc->value := "6";
      }
    }
  }

  STRING FUNCTION GetSanitizedPath()
  {
    STRING path := ^path->value;

    IF (^matchtype->value != 2 AND path NOT LIKE "/*") //make sure all non-wildcard paths start with a slash
      path := "/" || path;

    RETURN path;
  }

  INTEGER FUNCTION Submit()
  {
    OBJECT work := this->BeginWork();

    IF (NOT ^authrequirement->value AND ^authtype->value = 0)
      work->AddErrorFor(^authtype, this->GetTid(".login_required"));

    STRING sanitizedpath := this->GetSanitizedPath();

    IF (^hostingsrc->value = "4" AND ^matchtype->value != 1 AND ^keepsubpath->value=TRUE)
    {
      work->AddErrorFor(^hostingsrc, this->GetTid(".errors.cannot_redirect_to_initial_matchtype", this->GetTid(".matchtype"), this->GetTid(".matchtype-initial")));
    }

    IF (work->HasFailed())
    {
      work->Finish();
      RETURN 0;
    }

    INTEGER accessid := this->accessid;
    IF (accessid = 0)
    {
      accessid := MakeAutonumber(system.access, "ID");
      INSERT INTO system.access(id, webserver) VALUES (accessid, this->webserverid);
    }

    RECORD data :=
    [ path := sanitizedpath
    , errorpath := UnmapOutputPath(^errorpath->value)
    , authtype := ^authtype->value
    , authrequirement := ^authrequirement->value
    , hostingsrc := ToInteger(^hostingsrc->value, 0) + (^keepsubpath->value ? 1 : 0)
    , hostingpath := this->GetHostingPath()
    , authlist := ^authlist->value
    , authscript := ^externalscript->value ? ^externalscriptname->value : ""
    , description := ^description->value
    , matchtype := ^matchtype->value
    , redirectcode := ^redirectcode->value
    , disablecaching := ^disablecaching->value
    , maxage := ^maxage->value
    , fixcase := ^fixcase->value
    , disabled := NOT ^enabled->value
    ];

    UPDATE system.access SET RECORD data WHERE id = VAR accessid;

    IF (NOT work->Finish())
      RETURN 0;

    ReloadWebhareConfig(TRUE, FALSE);

    RETURN accessid;
  }
>;

PUBLIC STATIC OBJECTTYPE IPFilters EXTEND TolliumScreenBase
<
  INTEGER accessid;

  MACRO Init(RECORD params)
  {
    this->accessid := params.id;
    this->Refresh();
  }

  BOOLEAN FUNCTION HaveAllowButNoDeny()
  {
    RECORD ARRAY rows := ^ipfilterlist->rows;
    RETURN RecordExists(rows) AND NOT RecordExists(SELECT FROM rows WHERE NOT is_allow);
  }

  MACRO Refresh()
  {
    ^ipfilterlist->rows :=
      SELECT rowkey := mask
           , mask := CanonicalizeIPAddress(mask, TRUE)
           , status := is_allow ? this->GetTid(".allowed") : this->GetTid(".denied")
           , is_allow
        FROM system.access_ips
       WHERE accessid = this->accessid;

    ^warningpanel->visible := this->HaveAllowButNoDeny();
  }

  MACRO DoAdd()
  {
    OBJECT screen := this->LoadScreen(".updateipfilter", [ accessid := this->accessid, mask := "" ] );

    IF (screen->RunModal() = "ok")
      this->Refresh();
  }

  MACRO DoEdit()
  {
    OBJECT screen := this->LoadScreen(".updateipfilter", [ accessid := this->accessid, mask := ^ipfilterlist->value ] );

    IF (screen->RunModal() = "ok")
      this->Refresh();
  }

  MACRO DoDelete()
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".deleteipfilter")) = "yes")
    {
      OBJECT work := this->beginwork();

      DELETE FROM system.access_ips
       WHERE accessid = this->accessid
             AND mask = ^ipfilterlist->value;

      IF (NOT work->Finish())
        RETURN;

      ReloadWebhareConfig(TRUE, FALSE);
      this->Refresh();
    }
  }

  BOOLEAN FUNCTION Submit()
  {
    IF (this->HaveAllowButNoDeny())
      RETURN this->RunSimpleScreen("confirm", this->GetTid(".useipfiltersiwithoutdeny")) = "yes";

    RETURN TRUE;
  }
>;

PUBLIC STATIC OBJECTTYPE UpdateIPFilter EXTEND TolliumScreenBase
<
  INTEGER accessid;
  STRING oldmask;

  MACRO Init(RECORD params)
  {
    this->accessid := params.accessid;
    this->oldmask := params.mask;

    RECORD maskrec :=
      SELECT *
        FROM system.access_ips
       WHERE accessid = this->accessid
             AND mask = params.mask;

    IF (RecordExists(maskrec))
    {
      ^mask->value := CanonicalizeIPAddress(maskrec.mask, TRUE);
      ^allow_access->value := maskrec.is_allow;
    }
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->beginwork();
    STRING standardized_mask := CanonicalizeIPAddress(^mask->value, TRUE);
    IF(standardized_mask = "")
      work->AddErrorFor(^mask, this->GetTid(".errors.invalid_mask"));
    ELSE IF (this->oldmask = "" AND RecordExists(SELECT FROM system.access_ips WHERE accessid = this->accessid AND CanonicalizeIPAddress(mask, TRUE) = standardized_mask))
      work->AddErrorFor(^mask, this->GetTid(".errors.duplicate_mask"));

    IF (work->HasFailed())
      RETURN work->Finish();

    IF (this->oldmask = "")
    {
      INSERT INTO system.access_ips(accessid, mask, is_allow)
           VALUES (this->accessid, standardized_mask, ^allow_access->value);
    }
    ELSE
    {
      UPDATE system.access_ips
         SET mask := standardized_mask
           , is_allow := ^allow_access->value
       WHERE accessid = this->accessid
             AND mask = this->oldmask;
    }

    IF (NOT work->Finish())
      RETURN FALSE;

    ReloadWebhareConfig(TRUE, FALSE);
    RETURN TRUE;
  }
>;

PUBLIC STATIC OBJECTTYPE WebhareUsers EXTEND TolliumScreenBase
<
  INTEGER accessruleid;

  MACRO Init(RECORD params)
  {
    ^objectrightslist->InitForCommonDialog();
    ^objectrightslist->SelectObject("system:accessrules", params.id);
  }
>;

PUBLIC STATIC OBJECTTYPE ExternalUsers EXTEND TolliumScreenBase
<
  INTEGER accessruleid;

  MACRO Init(RECORD params)
  {
    this->accessruleid := params.id;
    this->Refresh();
  }

  MACRO Refresh()
  {
    ^userlist->rows :=
      SELECT rowkey := username
           , username
           , description
        FROM system.access_externalusers
       WHERE accessid = this->accessruleid;
  }

  MACRO DoAdd()
  {
    OBJECT screen := this->LoadScreen(".updateexternaluser", [ accessrule := this->accessruleid, user := "" ] );

    IF (screen->RunModal() = "ok")
      this->Refresh();
  }

  MACRO DoEdit()
  {
    OBJECT screen := this->LoadScreen(".updateexternaluser", [ accessrule := this->accessruleid, user := ^userlist->value[0] ] );

    IF (screen->RunModal() = "ok")
      this->Refresh();
  }

  MACRO DoDelete()
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".deleteuser")) != "yes")
      RETURN;

    OBJECT work := this->beginwork();

    DELETE FROM system.access_externalusers WHERE accessid = this->accessruleid AND username IN ^userlist->value;

    IF (NOT work->Finish())
      RETURN;

    ReloadWebhareConfig(TRUE, FALSE);

    this->Refresh();
  }
>;

PUBLIC STATIC OBJECTTYPE UpdateExternalUser EXTEND TolliumScreenBase
< INTEGER accessruleid;
  STRING user;
  STRING oldusername_uc; // used to check for duplicates

  MACRO Init(RECORD params)
  {
    this->accessruleid := params.accessrule;
    this->user := params.user;

    IF (this->user != "")
    {
      RECORD userrec :=
        SELECT *
          FROM system.access_externalusers
         WHERE accessid = this->accessruleid
           AND username = this->user;

      this->oldusername_uc := ToUpperCase(userrec.username);

      ^username->value := userrec.username;
      ^description->value := userrec.description;
      ^password->value := userrec.userpassword;
      ^password_again->value := userrec.userpassword;
    }
  }

  BOOLEAN FUNCTION Submit()
  {
    OBJECT work := this->beginwork();

    // Check password fields
    IF (^password->value != "" AND ^password_again->value != "" AND ^password->value != ^password_again->value)
      work->AddError(this->GetTid(".errors.passwords_dont_match"));

    // Check duplicate usernames
    IF (RecordExists(SELECT FROM system.access_externalusers
                      WHERE ToUpperCase(username) = ToUpperCase(^username->value)
                            AND accessid = this->accessruleid
                            AND ToUpperCase(^username->value) != this->oldusername_uc))
    {
      work->AddError(this->GetTid(".errors.duplicate_username", ^username->value));
    }

    IF (this->user = "")
    {
      INSERT INTO system.access_externalusers(accessid, username, description, userpassword)
        VALUES(this->accessruleid, ^username->value, ^description->value, ^password->value);
    }
    ELSE
    {
      UPDATE system.access_externalusers
         SET username := ^username->value
           , description := ^description->value
           , userpassword := ^password->value
       WHERE accessid = this->accessruleid
         AND username = this->user;
    }

    IF (NOT work->Finish())
      RETURN FALSE;

    ReloadWebhareConfig(TRUE, FALSE);

    RETURN TRUE;
  }
>;
