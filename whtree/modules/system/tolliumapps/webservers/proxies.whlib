<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/internal/dbschema.whlib";
LOADLIB "mod::system/lib/internal/resourcemanager.whlib";
LOADLIB "mod::system/lib/webserver/proxymgmt.whlib";

LOADLIB "mod::tollium/lib/screenbase.whlib";


PUBLIC STATIC OBJECTTYPE Proxies EXTEND TolliumScreenBase
<
  MACRO Init(RECORD params)
  {
    this->ReloadList();
  }

  MACRO GotProxyUpdate(RECORD ARRAY events)
  {
    this->ReloadList();
  }

  STRING FUNCTION GetStatusIcon(STRING status)
  {
    IF (status = "ok")
      RETURN "tollium:status/checked";
    ELSE IF (status = "password")
      RETURN "tollium:security/key";
    RETURN "tollium:status/negative";
  }

  MACRO ReloadList()
  {
    INTEGER currentproxy := this->tolliumcontroller->GetWebserverRequestData().proxyid;
    RECORD ARRAY proxies :=
        SELECT *
             , lastregistration
             , status :=  ^proxylist->GetIcon(this->GetStatusIcon(status))
          FROM system_internal.proxies;

    ^proxylist->rows :=
        SELECT rowkey := id
             , url
             , yourproxy := id = currentproxy
             , lastregistration
             , status
             , description
          FROM proxies;
  }

  MACRO DoAddProxy()
  {
    OBJECT screen := this->LoadScreen(".updateproxy", [ id := 0 ]);
    IF (screen->RunModal() = "ok")
      ^proxylist->value := screen->id;
  }

  MACRO DoEditProxy()
  {
    OBJECT screen := this->LoadScreen(".updateproxy", [ id := ^proxylist->value ]);
    screen->RunModal();
  }

  MACRO DoDeleteProxy()
  {
    IF (this->RunSimpleScreen("confirm", this->GetTid(".deleteproxy", ^proxylist->selection.url)) != "yes")
      RETURN;

    IF(NOT UnregisterWebhareProxy(^proxylist->value))
    {
      IF (this->RunSimpleScreen("confirm", this->GetTid(".unregisterfailedsuredelete")) != "yes")
        RETURN;
    }

    OBJECT work := this->BeginUnvalidatedWork();
    DeleteWebhareProxy(^proxylist->value);
    work->Finish();
  }

  MACRO DoReRegister()
  {
    //refresh 'lastset' so "we" are considered the leading callback server
    OBJECT work := this->BeginUnvalidatedWork();
    UPDATE system_internal.proxies SET lastset := GetCurrentDatetime();
    work->Finish();

    ReloadWebhareConfig(TRUE, FALSE);
  }
>;

PUBLIC STATIC OBJECTTYPE UpdateProxy EXTEND TolliumScreenBase
<
  INTEGER proxyid;

  PUBLIC PROPERTY id(proxyid, -);

  RECORD tested;

  MACRO Init(RECORD data)
  {
    this->proxyid := data.id;
    IF (this->proxyid != 0)
    {
      ^row->value := SELECT * FROM system_internal.proxies WHERE id = data.id;
      this->tested := ^row->value;
    }
    ^reverseaddress->placeholder := "http://.....:" || GetWebHareConfiguration().baseport + 5;
  }

  BOOLEAN FUNCTION DoTest(BOOLEAN hidepositivefeedback DEFAULTSTO FALSE)
  {
    OBJECT feedback := this->BeginFeedback();
    IF(feedback->HasFailed())
      RETURN feedback->Finish();

    RECORD testresult := TestWebHareProxy(^row->value.url, ^row->value.password, ^row->value.reverseaddress);
    IF(testresult.success)
    {
      IF (NOT hidepositivefeedback)
        this->RunSimpleScreen("info", this->GetTid(".testok"));
      this->tested := ^row->value;
      RETURN feedback->Finish();
    }
    ELSE
    {
      feedback->AddError(testresult.errormessage);
    }
    RETURN feedback->Finish();
  }

  BOOLEAN FUNCTION Submit()
  {
    IF (NOT this->BeginFeedback()->Finish())
      RETURN FALSE;

    IF (NOT RecordExists(this->tested) OR NOT RecordLowerBound([ this->tested ], ^row->value,
        [ "URL"
        , "PASSWORD"
        , "REVERSEADDRESS"
        ]).found)
    {
      IF (NOT this->DoTest(TRUE))
        RETURN FALSE;
    }

    OBJECT work := this->BeginWork();
    INTEGER id := SetWebHareProxy(this->proxyid, ^row->value);
    IF (NOT work->Finish())
      RETURN FALSE;

    this->proxyid := id;
    RETURN TRUE;
  }
>;
