﻿<?wh

LOADLIB "wh::util/algorithms.whlib";

LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/internal/rightsmgmt.whlib";
LOADLIB "mod::system/lib/userrights.whlib";

LOADLIB "mod::tollium/lib/gettid.whlib";


PUBLIC OBJECT __usereditcomposition;


/** Returns which specific authobjects (and thus the authobjects within)
    are visible for a specific user
*/
PUBLIC INTEGER ARRAY FUNCTION FilterAuthObjectsVisibleFor(INTEGER ARRAY authobjects, OBJECT wrduser)
{
  RECORD ARRAY paths := GetObjectsPaths([ "system:browseunits" ], wrduser->authobject, authobjects);
  RETURN
      SELECT AS INTEGER ARRAY objectid
        FROM paths
       WHERE LENGTH(path) != 0;
}

/** Returns the ids of all the units that the user may see, but may not see their parents (or don't have parents)
    @param user User to check
    @return List of ids of all root viewable units
*/
PUBLIC INTEGER ARRAY FUNCTION GetRootViewableUnitAuthobjects(OBJECT wrduser)
{
  RETURN GetRootObjectsForRights([ "system:browseunits" ], wrduser->authobject);
}

PUBLIC RECORD ARRAY FUNCTION EnrichWithAuthObjectIds(RECORD ARRAY inrows)
{
  STRING ARRAY getguids := SELECT AS STRING ARRAY wrd_guid FROM inrows;
  IF(Length(getguids)=0)
    RETURN DEFAULT RECORD ARRAY;

  RECORD ARRAY mapping :=
      SELECT id
           , parent
           , wrd_guid :=    guid
        FROM system.authobjects
       WHERE Length(getguids) = 1 ? guid = getguids[0] : TRUE
    ORDER BY guid;

  FOREVERY (RECORD rec FROM inrows)
  {
    RECORD pos := RecordLowerBound(mapping, rec, [ "WRD_GUID" ]);
    INSERT CELL authobjectid := pos.found ? mapping[pos.position].id : 0 INTO inrows[#rec];
    INSERT CELL authobjectparentid := pos.found ? mapping[pos.position].parent : 0 INTO inrows[#rec];
  }

  RETURN inrows;
}

/** @param userlist
    @cell userlist.unit
    @return
    @cell userlist.unitpath
*/
PUBLIC RECORD ARRAY FUNCTION EnrichWithUnitPath(RECORD ARRAY userlist, OBJECT user)
{
  INTEGER ARRAY no_rights_for;
  INTEGER ARRAY rights_for;

  RECORD ARRAY paths := GetObjectsPaths([ "system:browseunits" ], user->authobject, (SELECT AS INTEGER ARRAY authobjectparentid FROM userlist));

  INTEGER ARRAY all_unitids;
  FOREVERY (RECORD path FROM paths)
    all_unitids := all_unitids CONCAT path.path;

  all_unitids := SELECT AS INTEGER ARRAY DISTINCT id FROM ToRecordArray(all_unitids, "ID");

  RECORD ARRAY names :=
      SELECT id
           , name
        FROM system.authobjects
       WHERE id IN all_unitids
    ORDER BY id;

  FOREVERY (RECORD rec FROM userlist)
  {
    INTEGER unitid := rec.authobjectparentid;
    STRING path;

    IF (unitid = 0)
      path := GetTid("system:userrights.unitroot");
    ELSE
    {
      STRING ARRAY items;

      INTEGER ARRAY pathobjs := paths[RecordLowerBound(paths, [ objectid := unitid ], [ "OBJECTID" ]).position].path;
      FOREVERY (INTEGER unit FROM pathobjs)
      {
        RECORD pathrec := RecordLowerBound(names, [ id := unit ], [ "ID" ]);
        IF (pathrec.found)
          INSERT names[pathrec.position].name INTO items AT END;
      }

      path := Detokenize(items, "/");
    }
    INSERT CELL unitpath := path INTO userlist[#rec];
  }

  RETURN userlist;
}

// ---------------------------------------------------------------------------
//
// Unit trees
//


PUBLIC STRING FUNCTION GetAuthObjectPresentationName(OBJECT context, OBJECT wrdauthobj)
{
  // displays things such as "User 'sysop' (unit: 'WebHare')"
  RETURN GetTid("system:userrights.commondialogs.rights.authobjectfullid",
        ToString(wrdauthobj->type),
        wrdauthobj->GetUserRightsName(),
        GetUnitPath(context->controller->userapi, context->controller->userapi->GetUnitOf(wrdauthobj), context->user));
}

STRING FUNCTION GetUnitPath(OBJECT authapi, OBJECT unitobj, OBJECT browseuser)
{
  // Gather list of units toward root
  RECORD ARRAY units;
  INTEGER unitid := unitobj->entityid;
  WHILE (unitid != 0)
  {
    RECORD data := authapi->wrd_unit->GetEntityFields(unitid, [ "WRD_ID", "WRD_LEFTENTITY", "WRD_TITLE", "WRD_GUID" ]);
    IF (NOT RecordExists(data))
      BREAK;

    INSERT data INTO units AT 0;
    unitid := data.wrd_leftentity;
  }

  // Get the authobject ids
  units := EnrichWithAuthObjectIds(units);

  // Get the roots of visible units
  INTEGER ARRAY roots := GetRootViewableUnitAuthobjects(browseuser);
  IF (0 NOT IN roots)
  {
    // Remove invisible units
    WHILE (LENGTH(units) != 0 AND units[0].authobjectid NOT IN roots)
      DELETE FROM units AT 0;
  }

  IF (LENGTH(units) = 0 AND unitobj->entityid = 0)
    RETURN GetTid("system:userrights.common.rootunit");

  RETURN Detokenize((SELECT AS STRING ARRAY wrd_title FROM units), "/");
}

PUBLIC STRING ARRAY FUNCTION GetFieldsFor(STRING ARRAY setfields, STRING type, OBJECT controller)
{
  IF(type = "person")
  {
    //We're used for wildcard searches in ConstructSearchFilters so just limit to the common fields (we used to seek through almost a dozen...)
    RETURN GetSortedSet([ "wrd_fullname", "wrd_lastname", controller->userapi->wrdschema->accountlogintag, controller->userapi->wrdschema->accountemailtag]);
  }

  type := ToLowerCase(type);
  STRING tag := "WHUSER_" || ToUppercase(type); //role | unit  TODO should probably limit the search too
  STRING ARRAY availablefields := SELECT AS STRING ARRAY localtag FROM controller->userapi->wrdschema->GetType(tag)->ListAttributes(0) WHERE attributetype IN [ 2, 4 ];
  STRING ARRAY fields;
  FOREVERY(STRING field FROM setfields)
    IF(field IN availablefields)
      INSERT field INTO fields AT END;

  RETURN fields;
}

PUBLIC RECORD ARRAY FUNCTION EnrichWithOpenAs(RECORD ARRAY usersroles, OBJECT tolliumuser)
{
  BOOLEAN wearesysop := tolliumuser->HasRight("system:sysop");
  INTEGER ARRAY canopenas;
  IF(NOT wearesysop)
  {
    INTEGER ARRAY visibleobjects := (SELECT AS INTEGER ARRAY DISTINCT authobjectparentid FROM usersroles)
                                    CONCAT
                                    (SELECT AS INTEGER ARRAY authobjectid FROM usersroles);
    canopenas := tolliumuser->HasRightOnMultiple("system:openas", visibleobjects);
  }

  BOOLEAN ismagicsysopuser := wearesysop AND (SELECT AS STRING guid FROM system.authobjects WHERE id = tolliumuser->authobjectid) = "<overrideuser>";

  usersroles := SELECT *
                     , canopenas := issysop ? ismagicsysopuser : wearesysop OR authobjectparentid IN canopenas OR authobjectid IN canopenas
                 FROM usersroles;
  RETURN usersroles;
}

/** Returns a filtered list of users
    @param(object %TolliumControllerBase) controller Tollium controller
    @param filters WRD filters (for the WRD login type)
    @param(object %TolliumUser) tolliumuser Tollium user to calculate visiblity/rights for
    @return List of roles
    @cell(integer) return.rowkey WRD id of the user
    @cell(integer) return.wrd_id WRD id of the user
    @cell(integer) return.unit WRD id of the parent unit
    @cell(integer) return.authobjectid Authobject id of the user
    @cell(integer) return.authobjectidparentid Authobject id of the parent unit
    @cell(datetime) return.lastlogin Last login date
    @cell(string) return.fullname Full name
    @cell(string) return.comment Comment
    @cell(string) return.name Login name of the user
    @cell(string) return.email E-mail address
    @cell(integer) return.type Always 1 (user)
    @cell(integer) return.icon Icon (1: normal user, 2: role, 3: unit, 5: disabled user, 6: sysop)
    @cell(boolean) return.isrole Always FALSE
    @cell(boolean) return.canedit Whether the passed user can edit this user
    @cell(boolean) return.candelete Whether the passed user can delete this user
    @cell(boolean) return.canopenas Whether the passed user can login as this user
    @cell(record) return.draginfo Drag info (for drag and dropping this user)
*/
PUBLIC RECORD ARRAY FUNCTION GetUsersByFilter(OBJECT controller, RECORD ARRAY filters, OBJECT tolliumuser)
{
  RECORD outcols := [ wrd_id :=       "WRD_ID"
                    , disabletype :=  "WHUSER_DISABLE_TYPE"
                    , realname :=     "WRD_TITLE"
                    , email :=        controller->userapi->wrdschema->accountemailtag
                    , login :=        controller->userapi->wrdschema->accountlogintag
                    , wrd_guid :=     "WRD_GUID"
                    , unit :=         "WHUSER_UNIT"
                    , comment :=      "WHUSER_COMMENT"
                    , lastlogin :=    "WHUSER_LASTLOGIN"
                    ];

  RECORD ARRAY userlist := controller->userapi->wrdschema->accounttype->RunQuery(
        [ outputcolumns := outcols
        , filters :=       filters
       ]);
  userlist := EnrichWithAuthObjectIds(userlist);

  INTEGER ARRAY visible_authobjects := FilterAuthObjectsVisibleFor((SELECT AS INTEGER ARRAY authobjectid FROM userlist), tolliumuser);
  DELETE FROM userlist WHERE authobjectid NOT IN visible_authobjects;

  BOOLEAN wearesysop := tolliumuser->HasRight("system:sysop");
  INTEGER ARRAY canmanage;
  IF(NOT wearesysop)
    canmanage := tolliumuser->HasRightOnMultiple("system:manageunit", visible_authobjects CONCAT SELECT AS INTEGER ARRAY authobjectparentid FROM userlist);

  INTEGER ARRAY allsysops := GetGrantedAuthObjects("system:sysop", 0);
  userlist := SELECT TEMPORARY candelete := wearesysop OR authobjectparentid IN canmanage
                   , TEMPORARY canedit := candelete OR authobjectid IN canmanage
                   , TEMPORARY issysop := authobjectid IN allsysops
                   , issysop :=         issysop
                   , rowkey :=          wrd_id
                   , wrd_id
                   , unit
                   , authobjectid
                   , authobjectparentid
                   , disabled :=        disabletype != ""
                   , lastlogin
                   , fullname :=        realname
                   , comment :=         comment
                   , name :=            login
                   , email
                   , type :=            1 // user
                   , icon :=            disabletype != "" ? "tollium:objects/lock" : issysop ? "tollium:users/manager" : "tollium:users/user"
                   , isrole :=          FALSE
                   , canedit :=         canedit
                   , candelete :=       candelete
                   //FIXME can we merge our canopenas formula with userapi.whlib's impersonation validation?
                   , draginfo :=        [ type := "system:user"
                                        , data := [ id := wrd_id, candelete := candelete ]
                                        ]
              FROM userlist;
  RETURN userlist;
}

/** Returns a filtered list of roles
    @param(object %TolliumControllerBase) controller Tollium controller
    @param filters WRD filters (for the WRD type 'wrd_role')
    @param(object %TolliumUser) tolliumuser Tollium user to calculate visiblity/rights for
    @return List of roles
    @cell(integer) return.rowkey WRD id of the role
    @cell(integer) return.wrd_id WRD id of the role
    @cell(integer) return.unit WRD id of the parent unit
    @cell(integer) return.authobjectid Authobject id of the role
    @cell(integer) return.authobjectidparentid Authobject id of the parent unit
    @cell(datetime) return.lastlogin Last login date, always DEFAULT DATETIME
    @cell(string) return.fullname Full name, always empty
    @cell(string) return.comment Comment
    @cell(string) return.name Name of the role
    @cell(string) return.email E-mail address
    @cell(integer) return.type Always 3 (role)
    @cell(integer) return.icon Icon (1: normal user, 2: role, 3: unit, 5: disabled user, 6: sysop), always 2
    @cell(boolean) return.isrole Always TRUE
    @cell(boolean) return.canedit Whether the passed user can edit this role
    @cell(boolean) return.candelete Whether the passed user can delete this role
    @cell(boolean) return.canopenas Whether the passed user can login as this role (always FALSE)
    @cell(record) return.draginfo Drag info (for drag and dropping this role)
*/
PUBLIC RECORD ARRAY FUNCTION GetRolesByFilter(OBJECT controller, RECORD ARRAY filters, OBJECT tolliumuser)
{
  RECORD ARRAY rolelist := EnrichWithAuthObjectIds(controller->userapi->wrd_role->RunQuery(
        [ outputcolumns :=  [ wrd_id :=     "WRD_ID"
                            , wrd_title :=  "WRD_TITLE"
                            , wrd_guid :=   "WRD_GUID"
                            , unit :=       "WRD_LEFTENTITY"
                            , comment :=    "WHUSER_COMMENT"
                            ]
        , filters :=        filters
        ]));

  INTEGER ARRAY visible_authobjects := FilterAuthObjectsVisibleFor((SELECT AS INTEGER ARRAY authobjectid FROM rolelist), tolliumuser);
  DELETE FROM rolelist WHERE authobjectid NOT IN visible_authobjects;

  INTEGER ARRAY canmanageroles := tolliumuser->HasRightOnMultiple("system:manageroles", visible_authobjects CONCAT SELECT AS INTEGER ARRAY authobjectparentid FROM rolelist);

  INTEGER ARRAY allsysops := GetGrantedAuthObjects("system:sysop", 0, [expandroles := FALSE, inherited := FALSE]);
  RETURN
      SELECT rowkey :=          wrd_id
           , issysop :=         authobjectid IN allsysops
           , wrd_id
           , unit
           , authobjectid
           , authobjectparentid
           , lastlogin := DEFAULT DATETIME
           , fullname :=        ""
           , comment :=         comment
           , name :=            wrd_title
           , email :=           ""
           , type :=            3 // role
           , icon :=            "tollium:users/mask"
           , isrole :=          TRUE
           , canedit :=         authobjectparentid IN canmanageroles OR authobjectid IN canmanageroles
           , candelete :=       authobjectparentid IN canmanageroles
           , draginfo :=        [ type := "system:role"
                                , data := [ id := wrd_id, candelete := authobjectparentid IN canmanageroles ]
                                ]
        FROM rolelist;
}

/** Returns a filtered list of units
    @param(object %TolliumControllerBase) controller Tollium controller
    @param filters WRD filters (for the WRD type 'wrd_unit')
    @param(object %TolliumUser) tolliumuser Tollium user to calculate visiblity/rights for
    @return List of roles
    @cell(integer) return.rowkey WRD id of the unit
    @cell(integer) return.wrd_id WRD id of the unit
    @cell(integer) return.unit WRD id of the parent unit
    @cell(integer) return.authobjectid Authobject id of the unit
    @cell(integer) return.authobjectidparentid Authobject id of the parent unit
    @cell(string) return.fullname Full name, always empty
    @cell(string) return.comment Comment
    @cell(string) return.name Name of the unit
    @cell(string) return.email E-mail address (always empty)
    @cell(integer) return.type Always 2 (unit)
    @cell(integer) return.icon Icon (1: normal user, 2: role, 3: unit, 5: disabled user, 6: sysop), always 3.
    @cell(boolean) return.isrole Always FALSE
    @cell(boolean) return.canedit Whether the passed user can edit this unit
    @cell(boolean) return.candelete Whether the passed user can delete this unit
    @cell(boolean) return.canopenas Whether the passed user can login as this unit (always FALSE)
    @cell(record) return.draginfo Drag info (for drag and dropping this unit)
*/
PUBLIC RECORD ARRAY FUNCTION GetUnitsByFilter(OBJECT controller, RECORD ARRAY filters, OBJECT tolliumuser)
{
  RECORD ARRAY unitlist := EnrichWithAuthObjectIds(controller->userapi->wrd_unit->RunQuery(
        [ outputcolumns := [ wrd_id :=      "WRD_ID"
                           , wrd_title :=   "WRD_TITLE"
                           , wrd_guid :=    "WRD_GUID"
                           , comment :=     "WHUSER_COMMENT"
                           ]
        , filters := filters
        ]));

  INTEGER ARRAY visible_authobjects := FilterAuthObjectsVisibleFor((SELECT AS INTEGER ARRAY authobjectid FROM unitlist), tolliumuser);
  DELETE FROM unitlist WHERE authobjectid NOT IN visible_authobjects;

  INTEGER ARRAY canmanageunits := tolliumuser->HasRightOnMultiple("system:manageunit", visible_authobjects CONCAT SELECT AS INTEGER ARRAY authobjectid FROM unitlist);

  RETURN
      SELECT rowkey :=          wrd_id
           , wrd_id
           , unit :=            wrd_id
           , authobjectid
           , authobjectparentid
           , fullname :=        ""
           , comment :=         comment
           , name :=            wrd_title
           , email :=           ""
           , type :=            2 // unit
           , icon :=            "tollium:users/users"
           , isrole :=          FALSE
           , canedit :=         authobjectid IN canmanageunits OR wrd_id IN canmanageunits
           , candelete :=       authobjectid IN canmanageunits
           , draginfo :=        [ type := "system:unit"
                                , data :=
                                      [ wrd_id :=     wrd_id
                                      , candelete :=  authobjectid IN canmanageunits
                                      ]
                                ]
        FROM unitlist;

}

// -----------------------------------------------------------------------------
//
// Rights querying
//

BOOLEAN FUNCTION UserHasDirectGrantFor(STRING rightname, OBJECT grantee, INTEGER objectid, BOOLEAN onlywithgrantoption)
{
  INTEGER ARRAY grantobjids := GetObjectIdsOfDirectGrantsTo(rightname, grantee->authobject, onlywithgrantoption);
  RETURN (objectid IN grantobjids);
}

PUBLIC MACRO TolliumUpdateRoleGrant(OBJECT contexts, OBJECT work, STRING action, OBJECT roleobj, OBJECT ARRAY grantees, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  IF(NOT MemberExists(roleobj,"EnsureAuthObject"))
    THROW NEW Exception(`Expecting a WRDAuthObject derived role`);

  IF (NOT contexts->user->HasRightOn("system:manageroles", roleobj->authobjectid))
  {
    work->AddError(GetTid("system:userrights.commondialogs.errors.norighttomanagerole", roleobj->GetUserRightsName()));
    RETURN;
  }

  IF (NOT roleobj->ExistsInDatabase())
  {
    work->AddError(GetTid("system:userrights.commondialogs.errors.rolehasbeendeleted", roleobj->GetUserRightsName()));
    RETURN;
  }

  FOREVERY (OBJECT obj FROM grantees)
    IF (NOT obj->ExistsInDatabase())
    {
      IF (obj->type = 3)
        work->AddError(GetTid("system:userrights.commondialogs.errors.rolehasbeendeleted", obj->GetUserRightsName()));
      ELSE
        work->AddError(GetTid("system:userrights.commondialogs.errors.userhasbeendeleted", obj->GetUserRightsName()));
    }

  IF (work->HasFailed())
    RETURN;

  OBJECT roleunit := contexts->userapi->GetUnitOf(roleobj);

  FOREVERY (OBJECT obj FROM grantees)
  {
    // User either should have the right to browse the new role, or should get it with the new role
    IF (NOT roleunit->IsVisibleFor(obj) AND NOT roleunit->IsVisibleFor(roleobj))
      work->AddWarning(GetTid("system:userrights.commondialogs.errors.usercannotviewunitofrole", obj->GetUserRightsName(), roleobj->GetUserRightsName()));
  }

  FOREVERY (OBJECT obj FROM grantees)
  {
    LogAuditEvent("system:userrights", CELL[ action :=  action || "-role"
                                           , grantee := obj->GetUserDataForLogging()
                                           , roleauthobjectid := roleobj->authobjectid
                                           , roleentityid := roleobj->entityid
                                           , rolename := roleobj->GetUserRightsName()
                                           , options
                                           ]);
    TRY
    {
      contexts->user->UpdateRoleGrant(action, roleobj->authobjectid, obj, options);
    }
    CATCH (OBJECT e)
    {
      work->AddError(e->what);
    }
  }
}

PUBLIC BOOLEAN FUNCTION IsRevokeDangerous(OBJECT grantor, OBJECT grantee, STRING right_name, INTEGER objectid)
{
  RECORD ARRAY other_grantor_grants :=
      SELECT *
        FROM ExplainRightGrantedToOn(right_name, grantor->authobject, objectid, FALSE)
       WHERE withgrantoption
         AND (grantedright != right_name
           OR grantedobject != objectid
           OR COLUMN grantee != grantee->id);

  RETURN LENGTH(other_grantor_grants) = 0;
}

PUBLIC MACRO TolliumUpdateGrant(OBJECT work, OBJECT rightsinfo, OBJECT grantor, STRING action, STRING right_name, INTEGER ARRAY objectids, OBJECT grantee, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  IF(NOT MemberExists(grantee,"EnsureAuthObject"))
    THROW NEW Exception(`Expecting a WRDAuthObject derived grantee`);

  //TODO The Grant/Revoke APIs should give us properly metadata-d exceptions which we can then translate for the user again, instead of us re-litigating the same checks..
  BOOLEAN right_is_global := IsRightGlobal(right_name);
  IF (right_is_global)
  {
    IF (NOT grantor->CanManageRight(right_name))
      work->AddError(GetTid('system:userrights.errors.notallowedtomanageright', rightsinfo->GetRightTitle(right_name)));
  }
  ELSE
  {
    FOREVERY (INTEGER objectid FROM objectids)
      IF (NOT grantor->CanManageRightOn(right_name, objectid))
        work->AddError(GetTid('system:userrights.errors.notallowedtomanageright', rightsinfo->GetRightTitle(right_name)));
  }

  IF (work->HasFailed())
    RETURN;

  //FIXME move this deeper into the userrights API!
  LogAuditEvent("system:userrights", CELL[ action :=  action || "-right"
                                         , grantee := grantee->GetUserDataForLogging()
                                         , right := right_name
                                         , objectids
                                         , options
                                         ]);

  TRY
  {
    FOREVERY(INTEGER objectid FROM objectids)
      grantor->UpdateGrant(action, right_name, objectid, grantee, options);
  }
  CATCH(OBJECT e)
  {
    work->AddError(e->what);
  }
}

PUBLIC RECORD ARRAY FUNCTION ConstructSearchFilters(STRING term, STRING type, STRING ARRAY fields)
{
  RECORD ARRAY topfilters;

  FOREVERY(STRING line FROM Tokenize(term,'\n'))
  {
    line := TrimWhitespace(line);
    IF(line = "")
      CONTINUE;

    RECORD ARRAY linefilters;
    FOREVERY(STRING part FROM Tokenize(line,' '))
    {
      IF(part = "")
         CONTINUE;

      RECORD ARRAY partfilters;
      FOREVERY(STRING field FROM fields)
        INSERT [ field := field, matchtype := "LIKE", value := "*" || part || "*", matchcase := FALSE ] INTO partfilters AT END;

      IF(Length(partfilters) = 1)
        INSERT partfilters[0] INTO linefilters AT END;
      ELSE
        INSERT [ type := "OR", filters := partfilters ] INTO linefilters AT END;
    }

    IF(Length(linefilters) = 1)
      INSERT linefilters[0] INTO topfilters AT END;
    ELSE
      INSERT [ type := "AND", filters := linefilters ] INTO topfilters AT END;
  }

  RETURN Length(topfilters) = 1 ? [ topfilters[0] ] : [ [ type := "OR", filters := topfilters ] ];
}

PUBLIC RECORD FUNCTION CollectUserPreferenceOptions(OBJECT tolliumcontroller)
{
  // Fill language options
  RECORD ARRAY languages;
  FOREVERY (STRING lang FROM ["en", "nl"])
    INSERT [ rowkey := lang, title := GetTid(`tollium:common.languages.${lang}`) ] INTO languages AT END;

  // Decimal separators
  RECORD ARRAY decimalseps := [[ rowkey := ".", title := GetTid("system:userrights.edituser.dot") ]
                              ,[ rowkey := ",", title := GetTid("system:userrights.edituser.comma") ]
                              ,[ rowkey := ":", title := GetTid("system:userrights.edituser.colon") ]
                              ];

  // Thousand separators
  RECORD ARRAY thousandseps := [[ rowkey := "", title := GetTid("system:userrights.edituser.none") ]
                               ,[ rowkey := " ", title := GetTid("system:userrights.edituser.space") ]
                               ,[ rowkey := ".", title := GetTid("system:userrights.edituser.dot") ]
                               ,[ rowkey := ",", title := GetTid("system:userrights.edituser.comma") ]
                               ,[ rowkey := "'", title := GetTid("system:userrights.edituser.apostrophe") ]
                               ];

  RETURN [ languages := languages
         , decimalseps := decimalseps
         , thousandseps := thousandseps
         ];
}

/** Get the user edit policy for a user
    @param(object mod::tollium/lib/internal/contexts.whlib#TolliumContexts) contexts Tollium contexts
    @param user User entityid
    @cell(integer) options.defaultunit Unit to use when the user = 0
    @return @includecelldef %WRDAuthPlugin::GetUserEditPolicyForUser.return
*/
PUBLIC RECORD FUNCTION GetPolicyForUser(OBJECT contexts, INTEGER user, RECORD options DEFAULTSTO DEFAULT RECORD)
{
  options := ValidateOptions(
      [ defaultunit :=    0 // for when user = 0
      ], options);

  RETURN contexts->controller->wrdauthplugin->GetUserEditPolicyForUser(user, options);
}

PUBLIC MACRO ConfigurePasswordFieldForPolicy(OBJECT pwdfield, RECORD policy)
{
  pwdfield->visible := policy.haspassword;
  pwdfield->validationchecks := policy.passwordvalidationchecks;
}
