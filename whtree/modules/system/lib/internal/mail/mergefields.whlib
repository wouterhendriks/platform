<?wh


/** Merge fields for use in (mail) widgets
*/
PUBLIC STATIC OBJECTTYPE MergeFieldsContext
<
  RECORD source;
  STRING language;

  BOOLEAN mergefieldsinitialized;
  RECORD ARRAY pvt_mergefields;

  /** @short Get a list of merge fields
      @cell(string) name Field name
      @cell(string) title Field title
      @cell(record) value See FormBase::PrepareFormDataForComposer
      @cell(string) type The fully qualified type name (namespace and local name) of the field's node
  */
  PUBLIC PROPERTY mergefields(GetMergeFields, -);

  PUBLIC RECORD extradata;

  MACRO NEW(RECORD source, STRING language)
  {
    this->source := source;
    this->language := language;
  }

  PUBLIC MACRO Invalidate()
  {
    this->mergefieldsinitialized := FALSE;
    this->pvt_mergefields := RECORD[];
  }

  RECORD ARRAY FUNCTION GetMergeFields()
  {
    IF (this->mergefieldsinitialized)
      RETURN this->pvt_mergefields;

    SWITCH (this->source.type)
    {
      CASE "formdef"
      {
        RECORD ARRAY data := CellExists(this->source, "data") ? this->source.data.allfields : RECORD[];
        RECORD ARRAY sources;
        RECORD ARRAY fields := this->source.formdef->ListFields(CELL[ languagecode := this->language, this->source.applytester ]);
        FOREVERY (RECORD field FROM fields)
        {
          RECORD value :=
              SELECT *
                FROM data
               WHERE fieldguid = field.name
                     OR ToUppercase(fieldname) = ToUppercase(field.name);

          RECORD fielddescr := this->source.formdef->DescribeField(field.guid, [ languagecode := this->language, __listfields := fields ]);
          STRING title := RecordExists(fielddescr) ? fielddescr.pathtitle : field.obj->title;
          INSERT
              [ name := field.guid
              , title := title
              , value := value
              , type := field.obj->qname
              , isadditional := FALSE
              , hidden := FALSE
              ] INTO sources AT END;

          FOREVERY (RECORD extrafield FROM field.obj->GetExtraMergeFields(value))
          {
            INSERT
                [ name := field.guid || "$" || extrafield.name
                , title := title || " – " || extrafield.title
                , value := extrafield.value
                , type := field.obj->qname
                , isadditional := TRUE
                , hidden := CellExists(extrafield, 'hidden') AND extrafield.hidden //hidden in the userinterface
                ] INTO sources AT END;
          }
        }
        this->pvt_mergefields := sources;
        this->mergefieldsinitialized := TRUE;
        RETURN sources;
      }
    }
    THROW NEW Exception(`Unknown merge fields source '${this->source.type}'`);
  }
>;
