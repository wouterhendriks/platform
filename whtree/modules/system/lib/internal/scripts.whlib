﻿<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::files.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::os.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/resources.whlib";
LOADLIB "mod::system/lib/internal/webhareconstants.whlib";

PUBLIC RECORD FUNCTION LaunchScriptJS(STRING scriptname, STRING ARRAY args)
{
  TRY
  {
    OBJECT process := CreateProcess(`${GetWebHareConfiguration().installationroot}/modules/system/scripts/whcommands/node.sh`,
          [ GetWebHareResourceDiskPath(scriptname), ...args ], [ take_output := TRUE, take_errors := TRUE, merge_output_errors := TRUE ]);

    process->Start();
    STRING output;
    INTEGER exitcode := -1;
    OBJECT itr := MakeProcessAsyncIterator(process, [ autoclose := FALSE ]);
    WHILE (TRUE)
    {
      RECORD rec := WaitForPromise(itr->Next());
      IF (rec.done)
        BREAK;
      SWITCH (rec.value.type)
      {
        CASE "output"   { output := output || rec.value.line || "\n"; }
        CASE "close"    { exitcode := rec.value.exitcode; }
      }
    }
    process->Close();
    RETURN [ error := exitcode > 0, output := TrimWhitespace(output || "\n") || "\nScript returned exit code " || exitcode ];
  }
  CATCH(OBJECT e) //catches GetWebHareResourceDiskPath issues. other steps shouldn't throw
  {
    RETURN [ error := TRUE, output := e->what ];
  }
}

PUBLIC RECORD FUNCTION LaunchScript(STRING scriptname, STRING ARRAY args)
{
  IF(GetExtensionFromPath(scriptname) IN whconstant_javascript_extensions)
    RETURN LaunchScriptJS(scriptname, args);

  RECORD jobdata := CreateJob(scriptname);
  IF (NOT RecordExists(jobdata))
    ABORT("Don't have permission to start scripts"); // FATAL!

  IF (NOT ObjectExists(jobdata.job))
  {
    IF(Length(jobdata.errors)>0)
      RETURN [ error := TRUE, output := AnyToString(jobdata.errors,'boxed') ];
    ELSE
      RETURN [ error := TRUE, output := "Unknown error" ];
  }

  jobdata.job->SetConsoleArguments(args);
  INTEGER outputpipe := jobdata.job->CaptureOutput();
  jobdata.job->Start();

  STRING output;
  WHILE(TRUE)
  {
    INTEGER res := WaitForMultipleUntil([outputpipe, jobdata.job->handle], DEFAULT INTEGER ARRAY, MAX_DATETIME);

    STRING outputdata := ReadFrom(outputpipe,-16384);
    IF(outputdata!="")
      output := output || outputdata;
    ELSE IF(jobdata.job->IsSignalled()) //it's over?
      BREAK;
  }

  IF(output != "" AND output NOT LIKE "*\n")
    output := output || "\n";

  RECORD ARRAY errors := jobdata.job->GetErrors();
  IF(Length(errors)>0)
  {
    jobdata.job->Close();
    LogHarescriptErrors(errors, [ script := scriptname ]);
    RETURN [ error := TRUE, output := output || FormatHarescriptErrors(errors) ];
  }
  INTEGER exitcode := jobdata.job->GetConsoleExitCode();
  IF(exitcode != 0)
  {
    jobdata.job->Close();
    RETURN [ error := TRUE, output := (output = "" ? "" : output || "\n") || `Script returned exit code ${exitcode}` ];
  }
  jobdata.job->Close();
  RETURN [ error := FALSE, output := output ];
}
