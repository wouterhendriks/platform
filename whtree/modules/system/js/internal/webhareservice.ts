import bridge, { IPCMessagePacket, IPCMarshallableData } from "@mod-system/js/internal/whmanager/bridge";
import { ServiceBase } from "@webhare/services/src/backendservice";
import { createDeferred } from "@webhare/std";
import { ServiceInitMessage, ServiceCallMessage, WebHareServiceDescription, WebHareServiceIPCLinkType } from './types';

interface WebHareServiceOptions {
  autorestart?: boolean;
  restartimmediately?: boolean;
  //TODO __droplistenerreference is now a hack for the incoming bridgemgmt service which should either become permanent or go away once bridge uses IPClinks for that
  __droplistenerreference?: boolean;
}

/** Convert the return type of a function to a promise
 * Inspired by https://stackoverflow.com/questions/50011616/typescript-change-function-type-so-that-it-returns-new-value
*/
// eslint-disable-next-line @typescript-eslint/no-explicit-any -- using any is needed for this type definition
type PromisifyFunctionReturnType<T extends (...a: any) => any> = (...a: Parameters<T>) => ReturnType<T> extends Promise<any> ? ReturnType<T> : Promise<ReturnType<T>>;

/** Converts the interface of a WebHare service to the interface used by a client.
 * Removes the "close" method and all methods starting with `_`, and converts all return types to a promise. Readds "close" as added by ServiceBase
 * @typeParam BackendHandlerType - Type definition of the service class that implements this service.
*/
export type ConvertBackendServiceInterfaceToClientInterface<BackendHandlerType extends object> = {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any -- using any is needed for this type definition
  [K in Exclude<keyof BackendHandlerType, `_${string}` | "close"> as BackendHandlerType[K] extends (...a: any) => any ? K : never]: BackendHandlerType[K] extends (...a: any[]) => void ? PromisifyFunctionReturnType<BackendHandlerType[K]> : never;
} & ServiceBase;

//Describe a JS public interface in a HS compatible way
export function describePublicInterface(inobj: object): WebHareServiceDescription {
  const methods = [];

  //iterate to top and discover all methods
  for (; inobj; inobj = Object.getPrototypeOf(inobj)) {
    const propnames = Object.getOwnPropertyNames(inobj);
    if (propnames.includes("toString"))
      break; //we've reached the root

    for (const name of propnames) {
      if (name === 'constructor' || name[0] === '_')
        continue; //no need to explain the constructor, it's already been invoked. and skip 'private' functions

      // eslint-disable-next-line @typescript-eslint/no-explicit-any -- cleanup later, creating interfaces this way is ugly anyway
      const method = (inobj as any)[name];
      if (typeof method !== 'function')
        continue; //we only expose real functions, not variables, constants etc

      const params = [];
      for (let i = 0; i < method.length; ++i) //iterate arguments of method
        params.push({ type: 1, has_default: true }); //pretend all arguments to be VARIANTs in HareScript

      methods.push({
        signdata: {
          returntype: 1,  //variant return value
          params,
          excessargstype: -1
        },
        name
      });
    }
  }
  return { isjs: true, methods };
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any -- never[] doesn't work here, it confuses the actual calls to runBackendService
export type ConnectionConstructor = (...args: any[]) => object;

interface ServiceConnection {
  [key: string]: (...args: unknown[]) => unknown;
}

class LinkState {
  handler: object | null;
  link: WebHareServiceIPCLinkType["AcceptEndPoint"];
  initdefer = createDeferred<boolean>();

  constructor(handler: object | null, link: WebHareServiceIPCLinkType["AcceptEndPoint"]) {
    this.handler = handler;
    this.link = link;
  }
}

class WebHareService { //EXTEND IPCPortHandlerBase
  private _port: WebHareServiceIPCLinkType["Port"];
  private _constructor: ConnectionConstructor;
  private _links: LinkState[];
  private _options: WebHareServiceOptions;

  constructor(port: WebHareServiceIPCLinkType["Port"], servicename: string, constructor: ConnectionConstructor, options: WebHareServiceOptions) {
    this._port = port;
    this._constructor = constructor;
    this._port.on("accept", link => this._onLinkAccepted(link));
    this._links = [];
    this._options = options;
  }

  async _onLinkAccepted(link: WebHareServiceIPCLinkType["AcceptEndPoint"]) {
    try {
      const state = new LinkState(null, link);
      link.on("close", () => this._onClose(state));
      link.on("message", _ => this._onMessage(state, _));
      link.on("exception", () => false);
      if (this._options.__droplistenerreference)
        link.dropReference();

      await link.activate();
    } catch (e) {
      link.close();
    }
  }

  _onClose(state: LinkState) {
    if (state.handler && "_gotClose" in state.handler && typeof state.handler._gotClose == "function")
      state.handler._gotClose();
  }

  async _onMessage(state: LinkState, msg: WebHareServiceIPCLinkType["AcceptEndPointPacket"]) {
    if (!state.handler) {
      try {
        if (!this._constructor)
          throw new Error("This service does not accept incoming connections");

        const initdata = msg as IPCMessagePacket<ServiceInitMessage>;
        const handler = await this._constructor(...initdata.message.__new);
        if (!state.handler)
          state.handler = handler;
        if (!state.handler)
          throw new Error(`Service handler initialization failed`);

        state.link.send(describePublicInterface(state.handler), msg.msgid);
        state.initdefer.resolve(true);
      } catch (e) {
        state.link.sendException(e as Error, msg.msgid);
        state.link.close();
        state.initdefer.resolve(false);
      }
      return;
    }
    if (!await state.initdefer.promise) {
      state.link.sendException(new Error(`Service has not been properly initialized`), msg.msgid);
      state.link.close();
      return;
    }

    try {
      const message = msg.message as ServiceCallMessage;
      //const pos = this._links.findIndex(_ => _.link === state.link);
      const args = message.jsargs ? JSON.parse(message.jsargs) : message.args; //javascript string-encodes messages so we don't lose property casing due to DecodeJSON/EncodeJSON
      const result = await (state.handler as ServiceConnection)[message.call].apply(state.handler, args) as IPCMarshallableData;
      state.link.send({ result: message.jsargs ? JSON.stringify(result) : result }, msg.msgid);
    } catch (e) {
      state.link.sendException(e as Error, msg.msgid);
    }
  }

  async _onException(link: WebHareServiceIPCLinkType["AcceptEndPoint"], msg: WebHareServiceIPCLinkType["ExceptionPacket"]) {
    // ignore exceptions, not sent by connecting endpoints
  }

  close() {
    this._port.close();
  }
}

/** Launch a WebHare service.
    Starts a WebHare service and pass the constructed objects to every incoming connection. The constructor
          can also be left empty - the service will then simply run until its shutdown or requires an autorestart.

    @param servicename - Name of the service (should follow the 'module:tag' pattern)
    @param constructor - Constructor to invoke for incoming connections. This object will be marshalled through %OpenWebhareService
    @param options - Options.<br>
     - autorestart: Automatically restart the service if the source code has changed. Defaults to TRUE
     - restartimmediately: Immediately restart the service even if we stil have open connections. Defaults to FALSE
*/
export default async function runBackendService(servicename: string, constructor: ConnectionConstructor, options?: WebHareServiceOptions) {
  options = { autorestart: true, restartimmediately: false, __droplistenerreference: false, ...options };
  if (!servicename.match(/^.+:.+$/))
    throw new Error("A service should have a <module>:<service> name");

  const hostport = bridge.createPort<WebHareServiceIPCLinkType>("webhareservice:" + servicename, { global: true });
  const service = new WebHareService(hostport, servicename, constructor, options);

  if (options.__droplistenerreference)
    hostport.dropReference();

  await hostport.activate();

  return service;
}
