# 4.33 - 4 November 2021

## Incompatibilities and deprecations
- The WebHare backend no longer supports Internet Explorer, including IE11. So long, and thanks for all the (Active)X.
- The WebHare backend is now protected by a Content-Security-Policy on development installations (in preparation for
  enabling this everywhere)
- We have dropped support for old `@webhare-` and `@__webhareroot-` JavaScript/CSS import paths.
  - Replace `@__webhareroot-` with just `@mod-`
  - Replace `@webhare-<mod>/` with either `@mod-<mod>/js/...` or `@mod-<mod>/nodejs/...`
- TestFramework::ExtractAllMailFor will now only return the exact number of mails requested, and not pop off extra mails. If
  you want this, you will need to explicitly opt in using the `returnallmails` option. This makes test behavior more consistent
  when the same mailbox is extracted repeatedly.
- `WebBrowser::GotoWebPage` no longer accepts passing an empty string as the URL to go to.
- Development `wh` commands such as `wh up` and `wh umic` have been moved to the 'dev' module
- `AmazonAWSS3Interface` has been deprecated, users should switch to `AmazonAWSS3Service` which offers the same API
  expect that it supports an `AmazonAWSConfig` object to initialize it.
- Removes `system/lib/commondialogs.whlib`. All APIs there already have replacements in `system/lib/dialogs.whlib` and the
  existing commondialogs.whlib screens broke in WebHare 4.32.
- "DATE" values in WRD are now properly truncated to dates on read and write. The existing values in the database aren't
  updated so you can still change a field to be a DATETIME if that was your intention all along.
- Removes `hint`, `hinttid` and `icon` from select `<option>`s. These probably haven't done anything since WebHare 4.
- The outputtools (reload/watch widget) have been moved to the `dev` module.

## Things that are nice to know
- Adds an experimental and much faster esbuild based build pipeline. Optin by setting any of the `es2016`..`es2021` or `esnext`
  values as your assetpacks 'compatibility' value. (4.32 will still compile these as 'modern', but 4.31 will not support this
  property at all).
  - Keep in mind that esbuild does not properly support `url()` in SCSS @imports done from other SCSS files (it resolves them
    relative to the highest level SCSS entrypoint). Moving the SCSS @import statement to a JS import often fixes this.
  - esbuild is *much* stricter than the original webpack setup we provided and has no way to transpile your code back to IE11 compatible code
  - For testing, you can force *all* assetpack builds to use esbuild instead of webpack by starting WebHare as follows:
    `WEBHARE_ASSETPACK_FORCE_COMPATIBILITY=es2018 wh console`. (future versions may remove this option)
- WebHare now records a timestamp 'when' proxy settings were applied. This helps proxies figure out whether an incoming
  connection is from an older/restored WebHare and allows them to ignore this server.

## Things you should do
- Proxy servers should be upgraded to 3.2.0 to enable protection against 'old' servers coming online.
