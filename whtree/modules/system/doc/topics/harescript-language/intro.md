The HareScript language is the core of our WebHare products. The conversion templates, web applications and the WebHare interface are all written in this language.

HareScript is a web scripting language optimized for quick and efficient data access and manipulation.

