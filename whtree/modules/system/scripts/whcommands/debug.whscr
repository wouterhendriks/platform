<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::money.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "wh::promise.whlib";
LOADLIB "wh::util/algorithms.whlib";
LOADLIB "wh::internal/debug.whlib";
LOADLIB "wh::internal/interface.whlib";

LOADLIB "mod::system/lib/configure.whlib";
LOADLIB "mod::system/lib/logging.whlib";
LOADLIB "mod::system/lib/resources.whlib";
LOADLIB "mod::system/lib/internal/whconfig.whlib";
LOADLIB "mod::system/lib/internal/debugger/connector.whlib";


RECORD args;

MACRO ShowSyntax(STRING error)
{
  IF (error != "")
    PRINT(`Error: ${error}\n\n`);

  PRINT(
`Syntax: wh debug [command]
Commands:
  mark [text]                         Write a mark with optional text to all primary logfiles
  processlist                         Show the list of processes
  joblist [-v] [process] [mask]       Show the (optionally filtered) list of jobs in a process
  alljobs                             Show all jobs of all processes
  terminate [process] [mask]          Terminate jobs in a process
  listflags                           List available debug flags
  enable <flag>...                    Enable the specified debug flag
  disable <flag>...                   Disable the specified debug flag
  getconfig                           Show currently enabled debug features
  setconfig [tags]                    Set enabled debug features (eg. 'apr', 'cov')
  getsecret <name>                    Get a secret
  setsecret <name=value>...           Setup one or more test secrets
  findjob jobmask                     Locate the process with the specified job
  stacktrace process jobmask          Get a stack trace of the specified job
  jobstats process jobmask            Get job statistics (eg memory usage) for the specified job
  jobhandles process jobmask          Get job handle list for the specified job
  jobadhoccache process jobmask       List adhoccache items of the process of the specified job

See https://www.webhare.dev/reference/installation/environment for available debug flags
`);
  SetConsoleExitCode(error = "" ? 0 : 1);
  TerminateScript();
}

RECORD ARRAY FUNCTION GatherDebugFlags()
{
  RECORD ARRAY allflags;
  FOREVERY(RECORD flag FROM GetCustomModuleSettings("http://www.webhare.net/xmlns/system/moduledefinition", "debugflag"))
  {
    STRING name := flag.node->GetAttribute("name");
    IF(flag.module != "system" )
      name := flag.module || ":" || name;

    INSERT CELL[ name
               , description := flag.node->GetAttribute("description")
               ] INTO allflags AT END;
  }
  RETURN allflags;
}

MACRO ExitWithError(STRING error)
{
  PRINT(error || "\n");
  SetConsoleExitCode(1);
  TerminateScript();
}

STRING FUNCTION BuildURL(RECORD data)
{
  STRING uri := (data.secure ? "https://" : "http://");
  IF (data.host LIKE "*:*")
    uri := uri || data.host;
  ELSE
  {
    IF (data.host = "")
      uri:= uri || data.ip;
    ELSE
      uri:= uri || data.host;
    IF (data.port != (data.secure ? 443 : 80))
      uri := uri || ":" || data.port;
  }
  RETURN uri || data.url;
}

STRING FUNCTION GetScriptShow(RECORD authrec, STRING script)
{
  IF (CellExists(authrec, "TOLLIUM") AND recordExists(authrec.tollium.app))
    RETURN "Tollium: " || authrec.tollium.app.name || " (" || authrec.tollium.user.login || ")";
  IF (CellExists(authrec, "WEBSERVERREQUEST"))
  {
    IF (authrec.webserverrequest.url LIKE "/wh_services/*")
    {
      STRING res := "Remoting: " || SubString(authrec.webserverrequest.url, 13);
      IF (CellExists(authrec, "REMOTING"))
        res := res || " (" || authrec.remoting.functionname || ")";
      RETURN res;
    }
    RETURN BuildURL(authrec.webserverrequest);
  }
  RETURN script;
}

STRING ARRAY FUNCTION GetDebugTagStringParts(RECORD debugconfig)
{
  IF (NOT RecordExists(debugconfig) OR IsDefaultValue(debugconfig.tags))
    RETURN STRING[];

  STRING ARRAY tags := debugconfig.tags;
  IF(debugconfig.outputsession != "default")
    INSERT "session=" || debugconfig.outputsession INTO tags AT END;
  IF(debugconfig.context != "")
    INSERT "context=" || debugconfig.context INTO tags AT END;
  RETURN tags;
}

MACRO ShowDebugConfig(RECORD debugconfig)
{
  IF (NOT RecordExists(debugconfig) OR IsDefaultValue(debugconfig.tags))
  {
    PRINT(`All debugging features are disabled\n`);
  }
  ELSE
  {
    PRINT(`Enabled debug features: ${Detokenize(GetDebugTagStringParts(debugconfig), ",")}\n`);
  }
}

args := ParseArguments(GetConsoleArguments(),
    [ [ name := "debug", type := "switch" ]
    , [ name := "command", type := "param" ]
    , [ name := "params", type := "paramlist"]
    ]);

IF (NOT RecordExists(args))
  ShowSyntax("Invalid command");

RECORD FUNCTION ParseSubArgs(RECORD ARRAY subparams)
{
  RECORD subargs;
  IF (NOT IsDefaultValue(args.params) OR NOT IsDefaultValue(subparams))
  {
    subargs := ParseArguments(args.params, subparams);
    IF (NOT RecordExists(subargs))
      ShowSyntax("Invalid command arguments");
  }
  args := CELL[ ...args, ...subargs ];
  RETURN args;
}

OBJECT connection := ConnectToDebugManager();

ASYNC FUNCTION GetProcessByParam(STRING mask)
{
  OBJECT processlistener := connection->GetProcessListener();
  FOR (INTEGER i := 10; i >= 0; i := i - 1)
  {
    RECORD ARRAY processes := AWAIT processlistener->RequestUpdate();

    RECORD ARRAY matches :=
        SELECT *
          FROM processes
         WHERE args.process = ToString(processcode)
            OR args.process = clientname;

    IF (LENGTH(matches) > 1)
      ExitWithError(`Multiple matches for ${args.process}`);
    ELSE IF (RecordExists(matches))
      RETURN matches[0].obj;
    IF (i != 0)
      WaitUntil(DEFAULT RECORD, AddTimeToDate(10, GetCurrentDateTime()));
  }
  ExitWithError(`Could not find process ${args.process}`);
  RETURN DEFAULT OBJECT;
}

ASYNC FUNCTION SearchJobByParam(STRING prefix)
{
  RECORD ARRAY result;

  OBJECT processlistener := connection->GetProcessListener();
  RECORD ARRAY processlist := AWAIT processlistener->RequestUpdate();

  FOREVERY (RECORD processrec FROM processlist)
  {
    OBJECT process := processrec.obj;
    OBJECT listener := process->GetJobListener();

    RECORD ARRAY joblist := AWAIT listener->RequestUpdate();
    listener->Close();

    FOREVERY (RECORD rec FROM joblist)
      IF (rec.groupid LIKE prefix || "*")
      {
        INSERT CELL
            [ processrec.processcode
            , processrec.clientname
            , rec.groupid
            , rec.script
            ] INTO result AT END;
      }
  }
  processlistener->Close();
  RETURN result;
}

ASYNC FUNCTION GetJobList(OBJECT process)
{
  TRY
  {
    OBJECT joblistener := process->GetJobListener();
    RECORD ARRAY joblist := AWAIT joblistener->RequestUpdate();
    joblistener->Close();
    RETURN joblist;
  }
  CATCH
    RETURN RECORD[];
}

STRING FUNCTION PadTrunc(STRING str, INTEGER len)
{
  RETURN Left(str || RepeatText(" ", len), len);
}

STRING FUNCTION LeftPad(STRING str, INTEGER len)
{
  IF (LENGTH(str) >= len)
    RETURN str;
  RETURN Right(RepeatText(" ", len) || str, len);
}

ASYNC MACRO RunCommand()
{
  SWITCH (args.command)
  {
    CASE "processlist"
    {
      ParseSubArgs(RECORD[]);

      OBJECT processlistener := connection->GetProcessListener();
      WaitUntil(DEFAULT RECORD, AddTimeToDate(100, GetCurrentDateTime()));
      RECORD ARRAY processes := AWAIT processlistener->RequestUpdate();
      processlistener->Close();

      DumpValue((SELECT processcode, clientname FROM processes WHERE present), [ name := "List of processes", format := "boxed" ]);
    }
    CASE "joblist"
    {
      ParseSubArgs(
            [ [ name := "v", type := "switch" ]
            , [ name := "process", type := "param", required := TRUE ]
            , [ name := "mask", type := "param"]
            ]);

      OBJECT process := AWAIT GetProcessByParam(args.process);
      OBJECT joblistener := process->GetJobListener();

      RECORD ARRAY joblist := AWAIT joblistener->RequestUpdate();

      joblist :=
          SELECT *
               , scriptshow :=    GetScriptShow(authenticationrecord, script)
            FROM joblist
           WHERE status != "Terminated"
        ORDER BY creationdate;

      IF (args.mask != "")
      {
        joblist :=
            SELECT *
              FROM joblist
             WHERE scriptshow LIKE `${args.mask}`
                OR groupid LIKE `${args.mask}*`;
      }

      IF (args.v)
        DumpValue(joblist, [ name := `Job list for process ${process->processcode} ('${process->clientname}')`, format := "tree" ]);
      ELSE
      {
        DumpValue((SELECT groupid, scriptshow, creationdate, status, total_running := FormatDateTime("%H:%M:%S.%Q", total_running) FROM joblist), [ name := `Job list for process ${process->processcode} ('${process->clientname}')`, format := "boxed" ]);
      }
    }
    CASE "alljobs"
    {
      ParseSubArgs(
            [ [ name := "sort", type := "stringopt" ]
            ]);

      IF (args.sort NOT IN [ "", "processcode", "creationdate"])
        THROW NEW Exception(`Illegal sort flag, allowed: 'processcode', 'creationdate'`);

      OBJECT processlistener := connection->GetProcessListener();
      WaitUntil(DEFAULT RECORD, AddTimeToDate(100, GetCurrentDateTime()));
      RECORD ARRAY processes := AWAIT processlistener->RequestUpdate();
      processlistener->Close();

      OBJECT ARRAY joblist_promises;
      FOREVERY (RECORD rec FROM processes)
        INSERT rec.present ? GetJobList(rec.obj) : CreateResolvedPromise(VARIANT[]) INTO joblist_promises AT END;

      PRINT(`${PadTrunc("Process", 80)} ${LeftPad(`code`, 6)} `);
      PRINT(` job creationdate     groupid               `);
      PRINT(` ${LeftPad("objs", 6)} ${LeftPad("hndl", 4)}`);
      PRINT(` ${LeftPad("memuse", 11)}`);
      PRINT(` jobscript\n`);

      VARIANT ARRAY joblists := AWAIT CreatePromiseAll(joblist_promises);
      RECORD ARRAY alljobs;

      FOREVERY (RECORD process FROM processes)
      {
        IF (NOT process.present)
          CONTINUE;
        FOREVERY (RECORD job FROM SELECT * FROM joblists[#process] ORDER BY creationdate)
          INSERT CELL[ process, job ] INTO alljobs AT END;
      }

      IF (args.sort != "processcode")
        alljobs := SELECT * FROM alljobs ORDER BY job.creationdate;

      FOREVERY (RECORD rec FROM alljobs)
      {
        RECORD handlelist;
        RECORD stats;

        IF (rec.job.groupid != GetCurrentGroupId())
        {
          TRY
          {
            OBJECT jobconnector := rec.process.obj->ConnectToJob(rec.job.groupid);
            OBJECT job := AWAIT jobconnector->promise;
            IF (RecordExists(job->laststatus.statistics))
              stats := job->laststatus.statistics;
            AWAIT job->Pause();
            handlelist := AWAIT job->GetHandleList();
            job->Close();
          }
          CATCH;
        }

        PRINT(`${PadTrunc(rec.process.clientname, 80)} ${LeftPad(`${rec.process.processcode}`, 6)} `);
        PRINT(` ${FormatISO8601DateTime(rec.job.creationdate)} ${rec.job.groupid}`);
        PRINT(` ${LeftPad(RecordExists(stats) ? ToString(stats.objectcount) : "-", 6)} ${LeftPad(RecordExists(handlelist) ? ToString(LENGTH(handlelist.rawdata.items)) : "-", 4)}`);
        PRINT(` ${LeftPad(RecordExists(stats) ? FormatMoney(MONEY(stats.backingstore + stats.blobstore + stats.heap + stats.stack), 0, ".", "_", FALSE) || "KB": "-", 11)}`);
        PRINT(` ${rec.job.script}\n`);
      }
    }


    CASE "stacktrace", "jobstats", "jobhandles", "jobadhoccache"
    {
      ParseSubArgs(
            [ [ name := "v", type := "switch" ]
            , [ name := "process", type := "param", required := TRUE ]
            , [ name := "mask", type := "param", required := TRUE ]
            ]);

      OBJECT process := AWAIT GetProcessByParam(args.process);
      OBJECT joblistener := process->GetJobListener();

      RECORD ARRAY joblist := AWAIT joblistener->RequestUpdate();

      joblist :=
          SELECT *
               , scriptshow :=    GetScriptShow(authenticationrecord, script)
            FROM joblist
           WHERE status != "Terminated"
        ORDER BY creationdate;

      joblist :=
          SELECT *
            FROM joblist
           WHERE scriptshow LIKE `${args.mask}`
              OR groupid LIKE `${args.mask}*`;

      IF (LENGTH(joblist) > 1)
        ExitWithError(`Multiple matches for job ${args.mask}`);
      ELSE IF (NOT RecordExists(joblist))
        ExitWithError(`No matches for job ${args.mask}`);

      OBJECT jobconnector := process->ConnectToJob(joblist[0].groupid);
      OBJECT job := AWAIT jobconnector->promise;

      SWITCH (args.command)
      {
        CASE "stacktrace"
        {
          DumpValue((SELECT filename, line, col, func FROM job->laststatus.stacktrace), [ name := `Stack trace for job ${joblist[0].groupid} at process ${process->processcode} ('${process->clientname}')`, format := "boxed" ]);
        }
        CASE "jobstats"
        {
          RECORD stats := RecordExists(job->laststatus.statistics) ? job->laststatus.statistics :
              [ backingstore := "N/A"
              , blobstore := "N/A"
              , heap := "N/A"
              , instructions := "N/A"
              , objectcount := "N/A"
              , stack := "N/A"
              ];

          PRINT(`Statistics for job ${joblist[0].groupid} at process ${process->processcode} ('${process->clientname}')

Creationdate:       ${FormatISO8601DateTime(job->laststatus.creationdate, "", "milliseconds")}
Running time:       ${GetDayCount(job->laststatus.total_running)-1} days, ${FormatDateTime("%H:%M:%S.%Q", job->laststatus.total_running)}
Stack size:         ${job->laststatus.stacksize} elements
Backing store:      ${stats.backingstore} KB
Blob store:         ${stats.blobstore} KB
Heap store:         ${stats.heap} KB
stack store:        ${stats.stack} KB
Object count:       ${stats.objectcount}
Executed instrs.:   ${stats.instructions}
`);
        }
        CASE "jobhandles"
        {
          AWAIT job->Pause();
          RECORD handlelist := AWAIT job->GetHandleList();
          DumpValue((SELECT name, id FROM handlelist.rawdata.items ORDER BY id, name), [ name := `Handle list for job ${joblist[0].groupid} at process ${process->processcode} ('${process->clientname}')`, format := "boxed" ]);
        }
        CASE "jobadhoccache"
        {
          AWAIT job->Pause();
          RECORD handlelist := AWAIT job->GetAdhocCacheList();
          DumpValue((SELECT * FROM handlelist.rawdata.items ORDER BY library), [ name := `Adhoccache list for job ${joblist[0].groupid} at process ${process->processcode} ('${process->clientname}')`, format := "boxed" ]);
        }
        DEFAULT
        {
          ShowSyntax(`Invalid job command '${args.jobcmd}`);
        }
      }
    }

    CASE "findjob"
    {
      ParseSubArgs(
            [ [ name := "prefix", type := "param", required := TRUE ]
            ]);

      RECORD ARRAY results := AWAIT SearchJobByParam(args.prefix);
      IF (IsDefaultValue(results))
        PRINT(`No job found with prefix ${args.prefix}\n`);
      ELSE
        DumpValue(results, [ name := "Matching jobs", format := "boxed" ]);
    }

    CASE "terminate"
    {
      ParseSubArgs(
            [ [ name := "v", type := "switch" ]
            , [ name := "process", type := "param", required := TRUE ]
            , [ name := "mask", type := "param", required := TRUE ]
            ]);

      OBJECT processlistener := connection->GetProcessListener();
      RECORD ARRAY processes := AWAIT processlistener->RequestUpdate();

      OBJECT process := AWAIT GetProcessByParam(args.process);

      OBJECT joblistener := process->GetJobListener();
      RECORD ARRAY joblist := AWAIT joblistener->RequestUpdate();

      joblist :=
          SELECT *
               , scriptshow :=    GetScriptShow(authenticationrecord, script)
            FROM joblist
           WHERE status != "Terminated"
        ORDER BY creationdate;

      joblist :=
          SELECT *
            FROM joblist
           WHERE scriptshow LIKE `${args.mask}`
              OR groupid LIKE `${args.mask}*`;

      IF (NOT RecordExists(joblist))
        ExitWithError(`Could not find match for mask ${args.mask}`);

      FOREVERY (RECORD rec FROM joblist)
      {
        PRINT(`Terminate ${rec.groupid}: ${rec.scriptshow}\n`);
        process->TerminateJob(rec.groupid);
      }
    }
    CASE "getconfig"
    {
      ParseSubArgs(RECORD[]);

      ShowDebugConfig(__debugconfig);
    }
    CASE "mark"
    {
      ParseSubArgs(
            [ [ name := "text", type := "param" ]
            ]);

      WriteLogMarker(args.text);
    }
    CASE "getsecret"
    {
      ParseSubArgs(
            [ [ name := "secret", type := "param", required := TRUE ]
            ]);

      STRING secret := GetTestSecret(args.secret);
      IF(secret = "")
        TerminateScriptWithError(`Secret '${args.secret} not set`);
      Print(secret || "\n");
    }
    CASE "setsecret"
    {
      ParseSubArgs(
            [ [ name := "secrets", type := "paramlist" ]
            ]);

      RECORD currentconfig := GetSystemConfigurationRecord();
      RECORD testsecrets := CellExists(currentconfig,'testsecrets') ? currentconfig.testsecrets : CELL[];
      FOREVERY(STRING toset FROM args.secrets)
      {
        IF(toset NOT LIKE "?*=*")
          TerminateScriptWithError(`Invalid secret syntax - use 'name=value' pairs`);

        STRING secretname := Left(toset, SearchSubstring(toset,'='));
        STRING secretvalue := Substring(toset, Length(secretname) + 1);
        testsecrets := CellDelete(testsecrets, secretname);
        testsecrets := CellInsert(testsecrets, secretname, secretvalue);
      }
      UpdateSystemConfigurationRecord(CELL[testsecrets]);
    }
    CASE "enable", "disable"
    {
      ParseSubArgs(
            [ [ name := "flags", type := "paramlist" ]
            ]);

      //Validate first
      RECORD ARRAY allflags := SELECT * FROM GatherDebugFlags() ORDER BY name;
      RECORD ARRAY updateflags;
      FOREVERY(STRING flag FROM args.flags)
      {
        RECORD match := SELECT * FROM allflags WHERE allflags.name = VAR flag;
        IF(NOT RecordExists(match))
          THROW NEW Exception(`Unknown flag '${flag}'`);

        INSERT match INTO updateflags AT END;
      }

      //Then proceed to setting
      IF (GetEnvironmentVariable("WEBHARE_DEBUG") != "")
        Print("Warning: The WEBHARE_DEBUG environment variable is currently set, the debug configuration only has effect on processes that don't have it set.\n");

      STRING ARRAY tags := GetDebugTagStringParts(__debugconfig);
      FOREVERY(RECORD flag FROM SELECT DISTINCT * FROM updateflags ORDER BY name)
      {
        Print(`${args.command = "enable" ? "Enabling" : "Disabling"} ${flag.name} (${flag.description})\n`);
        IF(args.command = "enable")
          tags := tags CONCAT STRING[flag.name];
        ELSE
          tags := ArrayDelete(tags, STRING[flag.name]);
      }

      RECORD debugconfig := ReadDebugSetting(Detokenize(GetSortedSet(tags),','));
      UpdateSystemConfigurationRecord(CELL[ debugconfig ]);
    }
    CASE "setconfig"
    {
      ParseSubArgs(
            [ [ name := "tags", type := "paramlist" ]
            ]);

      //we'll accept both space and comma seperated
      RECORD debugconfig := ReadDebugSetting(Detokenize(args.tags,','));
      IF (GetEnvironmentVariable("WEBHARE_DEBUG") != "")
        Print("Warning: The WEBHARE_DEBUG environment variable is currently set, the debug configuration only has effect on processes that don't have it set.\n");

      UpdateSystemConfigurationRecord(CELL[ debugconfig ]);
      ShowDebugConfig(debugconfig);
    }
    CASE "listflags"
    {
      RECORD ARRAY allflags := SELECT * FROM GatherDebugFlags() ORDER BY name;
      INTEGER maxflaglength := SELECT AS INTEGER MAX(Length(name)) FROM allflags;
      FOREVERY(RECORD flag FROM allflags)
      {
        STRING padflag := flag.name || RepeatText(" ", maxflaglength - Length(flag.name));
        Print(`${padflag}  ${flag.description}\n`);
      }
    }
    CASE "", "help"
    {
      ShowSyntax("");
    }
    DEFAULT
    {
      ShowSyntax(`Unknown command ${args.command}`);
    }
  }
}

WaitForPromise(RunCommand());
