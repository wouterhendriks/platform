<?wh
LOADLIB "wh::ipc.whlib";
LOADLIB "wh::os.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/services.whlib";

OBJECT trans;
OBJECT link;

MACRO ListServices()
{
  RECORD report := WaitForPromise(link->GetReport());
  DumpValue( (SELECT name, enabled, running, since FROM report.report ORDER BY name),'boxed');
}

MACRO RestartService(STRING servicename)
{
  RECORD res := WaitForPromise(link->Restart(servicename));
  Print("Service scheduled for restart: " || servicename || "\n");
}

MACRO EnableService(STRING servicename)
{
  RECORD res := WaitForPromise(link->SetEnabled(servicename,TRUE));
  Print("Service enabled: " || servicename || "\n");
}

MACRO DisableService(STRING servicename)
{
  RECORD res := WaitForPromise(link->SetEnabled(servicename,FALSE));
  Print("Service disabled: " || servicename || ". To invoke manually:\n");
  Print("  " || res.app || " " || Detokenize(res.args,' ') || "\n");
}

MACRO HandleWildcards(MACRO PTR func, STRING servicemask)
{
  IF(SearchSubstring(servicemask,'?') = -1 AND SearchSubstring(servicemask,'*') = -1)
  {
    func(servicemask);
    RETURN;
  }

  RECORD report := WaitForPromise(link->GetReport());
  RECORD ARRAY matches := SELECT * FROM report.report WHERE ToUppercase(name) LIKE ToUppercase(servicemask) ORDER BY ToUppercase(name);
  IF(Length(matches) = 0)
    TerminateScriptWithError(`No service matches mask '${servicemask}' - you may need to 'wh services reload'?`);

  FOREVERY(RECORD match FROM matches)
  {
    //Could have done them all async... but that might give hard to reproduce bugreports
    func(match.name);
  }
}

MACRO DebugService(STRING servicename, STRING ARRAY serviceargs)
{
  RECORD app := WaitForPromise(link->SetEnabled(servicename,FALSE));

  SetConsoleLineBuffered(FALSE);
  SetOutputBuffering(FALSE); //do we need this?

  STRING ARRAY debugargs := app.args CONCAT serviceargs;
  FOR (INTEGER i := -1; i < LENGTH(debugargs); i := i + 1)
    IF ((i >= 0 AND debugargs[i] LIKE "mod::*") OR i = LENGTH(app.args) - 1)
    {
      INSERT "--debug" INTO debugargs AT i + 1;
      BREAK;
    }

  WHILE(TRUE)
  {
    OBJECT proc := CreateProcess(app.app, debugargs,
      [ take_input := FALSE
      , take_output := TRUE
      , take_errors := TRUE
      ]);

    proc->Start();
    WHILE(TRUE)
    {
      INTEGER read := WaitForMultiple(INTEGER[proc->output_handle, proc->errors_handle, 0], INTEGER[], 100);
      STRING out,err;

      IF(read = 0)
      {
        STRING cmd := ToUppercase(ReadFrom(0,-1));
        IF(cmd = "?" OR cmd NOT IN ["Q","R"])
        {
          Print("\n** Q to quit, R to restart **\n");
          CONTINUE;
        }
        IF(cmd="R")
        {
          Print("\nrestarting\n");
          proc->SendInterrupt();
          Sleep(400); //TODO watch process, if it closes faster than in 400 msecs that's okay too
          proc->Terminate();
          BREAK;
        }

        Print("\nquitting\n");
        proc->SendInterrupt();
        Sleep(400);
        proc->Terminate();

        Print("Reenabling service\n");
        link->SetEnabled(servicename,TRUE);
        RETURN;
      }

      IF(read = proc->output_handle)
      {
        out := ReadFrom(proc->output_handle, -16384);
        PrintTo(1, out);
      }

      IF(read = proc->errors_handle)
      {
        err := ReadFrom(proc->errors_handle, -16384);
        PrintTo(2, err);
      }

      IF(out = "" AND err = "" AND NOT proc->IsRunning())
        BREAK;
    }

    Print(`\nProcess exited with exit code ${proc->exitcode}\n`);
    proc->Close();

    //FIXME if not an autostart app, break now
    Sleep(1000);
  }
}

MACRO Main()
{
  link := WaitForPromise(OpenWebHareService("system:apprunner"));

  trans := OpenPrimary();
  RECORD call := ParseArguments(GetConsoleArguments(), [ [ name := "cmd",         type := "param" ]
                                                       , [ name := "service",     type := "param" ]
                                                       , [ name := "serviceargs", type := "paramlist" ]
                                                       ]);

  IF(RecordExists(call) AND Length(call.serviceargs) > 0 AND call.cmd NOT IN ["debug"])
  {
    TerminateScriptWithError(`This subcommand does not support arguments to the sevice`);
  }

  SWITCH(RecordExists(call) ? call.cmd : "help")
  {
    CASE "", "list"
    {
      ListServices();
    }
    CASE "reload"
    {
      BroadcastEvent("system:apprunner.internal.rescan", DEFAULT RECORD);
    }
    CASE "stop"
    {
      HandleWildcards(PTR DisableService, call.service);
    }
    CASE "start"
    {
      HandleWildcards(PTR EnableService, call.service);
    }
    CASE "restart"
    {
      HandleWildcards(PTR RestartService, call.service);
    }
    CASE "debug"
    {
      DebugService(call.service, call.serviceargs);
    }
    DEFAULT
    {
      Print("Syntax: wh services <command>\n");
      Print("  list               List all services\n");
      Print("  reload             Reload service list\n");
      Print("  start <service>    Start the specified service(s) (wildcards accepted)\n");
      Print("  stop <service>     Stop the specified service(s) (wildcards accepted)\n");
      Print("  restart <service>  Restart the specified service(s) (wildcards accepted)\n");
      IF(RecordExists(call))
        TerminateScript();
      ELSE
        TerminateScriptWithError("Invalid syntax");
    }
  }
}

Main();
