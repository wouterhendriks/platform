<?wh
/** @private Catalog management should be done through api.whlib */

LOADLIB "mod::system/lib/database.whlib";

/** @loadlib mod::consilio/lib/internal/dbschema.whlib
    @schemadef consilio
*/
PUBLIC SCHEMA
  < TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , STRING "description"
    , STRING "definedby"
    , INTEGER "loglevel"
    , INTEGER "priority"
    , INTEGER "type"
    , STRING "fieldgroups"
    , STRING "lang"
    , STRING "suffix"
    , BLOB "internalmetadata"
    ; KEY id
    > catalogs
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "catalogid" NULL := 0
    , INTEGER "indexmanager" NULL := 0
    , STRING "indexname"
    , INTEGER "searchpriority"
    , BOOLEAN "readonly"
    ; KEY id
    > catalog_indexmanagers
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "contentprovider"
    , STRING "data"
    , INTEGER "catalogid" NULL := 0
    , STRING "tag"
    , STRING "title"
    , INTEGER "status"
    , DATETIME "last_indexed"
    , DATETIME "orphansince"
    , STRING "definedby"
    , INTEGER "fsobject" NULL := 0
    ; KEY id
    > contentsources
  , TABLE
    < INTEGER "id" NULL := 0
    , DATETIME "checked"
    , INTEGER "checktime"
    , DATETIME "last_referred"
    , DATETIME "nextcheck"
    , INTEGER "status"
    , STRING "url"
    , INTEGER "hashed_url_int"
    ; KEY id
    > checked_links
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "link" NULL := 0
    , INTEGER64 "system_fs_setting" NULL := 0
    , STRING "text"
    , STRING "url"
    , INTEGER64 "wrd_entity_setting" NULL := 0
    ; KEY id
    > checked_objectlinks
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "server"
    , INTEGER "status"
    , DATETIME "nextcheck"
    , DATETIME "down_since"
    ; KEY id
    > checked_servers
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "name"
    , STRING "address"
    , STRING "configuration"
    ; KEY id
    > indexmanagers
  , TABLE
    < INTEGER "id" NULL := 0
    , INTEGER "catalogid" NULL := 0
    , INTEGER "wordgroup"
    , STRING "word"
    ; KEY id
    > thesaurus
  , TABLE
    < INTEGER "id" NULL := 0
    , STRING "pattern"
    , BOOLEAN "nolinkcheck_from"
    , BOOLEAN "nolinkcheck_to"
    , BOOLEAN "noindex"
    , BOOLEAN "nofollow"
    , BOOLEAN "pattern_regex"
    ; KEY id
    > blacklist
  > consilio;

/** @short Bind the module tables
    @long This macro binds all of the search module's tables to the specified transaction
    @param transaction Transaction ID to bind the tables to */
MACRO BindConsilioTables(INTEGER transaction)
{
  consilio := BindTransactionToSchema(transaction, "consilio");
}

SetupPrimaryTransactionBinder(PTR BindConsilioTables);
