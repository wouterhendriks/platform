export * from "./pxl";

import { warnESFile } from "@mod-system/js/internal/es-warning";
warnESFile("@mod-consilio/js/pxl.es");
