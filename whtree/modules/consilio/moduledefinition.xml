<module xmlns="http://www.webhare.net/xmlns/system/moduledefinition">

  <meta configscreen="screens/catalog.xml#catalogs">
    <description>Consilio - The WebHare Search Module</description>
    <packaging>
      <dependency module="system" />
    </packaging>
    <validation options="nowarnings">
      <format masks="*.ts *.tsx" />
    </validation>
  </meta>

  <servicemanager>
    <!-- migrate_indices_v3: v5.02 database upgrade to prepare for opensearch managers -->
    <runonce script="scripts/runonce/migrate_indices.whscr" tag="migrate_indices_v3" when="afterschemacreation" />

    <!-- setupopensearch: launches the initial builtin opensearch if not yet present -->
    <runonce script="scripts/runonce/setupopensearch.whscr" tag="setupopensearch" when="poststart" />

    <task runat="*/15   * * * *" script="scripts/tasks/executeindextasks.whscr" tag="indextasks" runatstartup="true" />
    <task runat="17 */8 * * *" script="scripts/tasks/executelinkcheck.whscr" tag="linkchecker" description="Check links on indexed pages" />  <!-- even though the script self-schedules, we should regularly retry it if it crashed -->
    <task runat="27   3 * * *" runtz="maintenance" script="scripts/tasks/executemaintenance.whscr" tag="maintenance_task" description="Consilio maintenance task" />
    <task runat=" 6   3 * * *" runtz="maintenance" script="scripts/tasks/cleanupindices.whscr" tag="cleanupindices" description="Verify and cleanup indices" />
    <task tag="update" script="scripts/whcommands/update.whscr" description="Update indices" runatstartup="true" />
  </servicemanager>

  <services>
    <webservice name="search"
                library="lib/internal/rpc_search.whlib"
                prefix="RPC_"
                primarytrans="auto"
                transports="jsonrpc">
      <accesscheck />
    </webservice>

    <!-- backend API. not intended for external usage but wrapped through a JS library so we can switch to eg a more cacheable REST api in the future -->
    <webservice name="backend" library="lib/internal/backendrpc.whlib" prefix="RPC_" primarytrans="none" transports="jsonrpc">
      <accesscheck />
    </webservice>

    <apprunnerconfig configfunction="lib/internal/support.whlib#getconsilioapprunnerconfig" />
  </services>

  <backend>
    <webrule path="allroots:/.px/" match="initial" handlebyscript="web/resources/pxl.txt">
      <addheader header="Content-Type" value="text/plain" />
    </webrule>
    <webrule path="root:/.consilio/builtin-opensearch/" match="initial" handlebyscript="web/resources/builtin-opensearch.shtml" allowallmethods="true" />
  </backend>

  <tollium>
    <!-- Entry point for our component parser -->
    <components namespace="http://www.webhare.net/xmlns/consilio/components" xmlschema="components.xsd" />
  </tollium>

  <rights>
    <objecttype name="catalog"
                describer="lib/internal/rights.whlib#catalogobjecttypedescriber"
                icon="consilio:consilio"
                table=".catalogs"
                tid="module.rights.catalogs" />

    <right name="manage" descriptiontid="module.rights.manage_descr" objecttype="catalog" tid="module.rights.manage">
      <impliedby right="system:sysop" />
    </right>
    <right name="metadata"
           descriptiontid="module.rights.metadata_descr"
           objecttype="catalog"
           showafter="manage"
           tid="module.rights.metadata">
      <impliedby right="manage" />
    </right>
    <right name="read"
           descriptiontid="module.rights.read_descr"
           objecttype="catalog"
           showafter="readwrite"
           tid="module.rights.read">
      <impliedby right="metadata" />
    </right>
  </rights>

  <portal>
    <application name="config"
                 group="system:setup"
                 icon="consilio:consilio"
                 screen="/screens/catalog.xml#catalogs"
                 tid="catalog.catalogs.apptitle">
      <accesscheck>
        <requireright right="read" />
      </accesscheck>
    </application>

    <dashboardgroup name="consilio" orderafter="system:ap system:tasks" tid="module.name" />

    <dashboardpanel name="indexmanagers"
                    group="consilio"
                    screen="tolliumapps/dashboard/indexmanagers.xml"
                    tid="tolliumapps.dashboard.indexmanagers.title" />
    <dashboardpanel name="queries"
                    group="consilio"
                    screen="tolliumapps/dashboard/queries.xml"
                    tid="tolliumapps.dashboard.queries.title" />

    <notification name="notifications.catalog_uptodate"
                  descriptiontid="module.notifications.catalog_uptodate-desc"
                  tid="module.notifications.catalog_uptodate" />
  </portal>

  <publisher>
    <siteprofile path="data/consilio.siteprl.xml" />

    <!-- webfeatures/sitecontent implements frontend parts for consilio. it's always active, not something to enable -->
    <siteprofile path="webfeatures/sitecontent/sitecontent.siteprl.xml" />

    <webdesignplugin name="robots"
                     namespace="http://www.webhare.net/xmlns/consilio"
                     objectname="lib/internal/webdesignplugin.whlib#robotsplugin" />
    <webdesignplugin name="searchpage"
                     namespace="http://www.webhare.net/xmlns/publisher/siteprofile"
                     objectname="lib/internal/sitecontent/searchplugin.whlib#SearchPagePlugin" />

    <indexfield name="linksto" fieldfunc="lib/internal/publishersearch.whlib#getitemfield" />
    <indexfield name="internallink" fieldfunc="lib/internal/publishersearch.whlib#getitemfield" />
    <indexfield name="externallink" fieldfunc="lib/internal/publishersearch.whlib#getitemfield" tokenized="false" />
    <indexfield name="indexversions" fieldfunc="lib/internal/publishersearch.whlib#getitemfield" />
    <indexfield name="indextypes" fieldfunc="lib/internal/publishersearch.whlib#getitemfield" />

    <searchfilter name="linksto"
                  filterobject="lib/internal/publishersearch.whlib#linkstofilter"
                  tid="publishersearch.linksto" />
  </publisher>

  <databaseschema xmlns:d="http://www.webhare.net/xmlns/whdb/databaseschema">
    <d:table name="catalogs" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:obsoletecolumn name="indexmanager" />
      <d:varchar name="description" maxlength="4096">
        <d:documentation>Catalog description</d:documentation>
      </d:varchar>
      <d:varchar name="name" maxlength="256" nullable="0" unique="1">
        <d:documentation>Tag to identify this catalog (should be in the form "module:tag")</d:documentation>
      </d:varchar>
      <d:integer name="loglevel">
        <d:documentation>Log level for this catalog: 0 errors only, 1 skipped documents, 2 general information, 3 all debugging information</d:documentation>
      </d:integer>
      <d:integer name="priority">
        <d:documentation>Priority in the range -9 to 9 (0 is default priority, 9 is highest priority)</d:documentation>
      </d:integer>
      <d:obsoletecolumn name="rebuild" />
      <d:integer name="type">
        <d:documentation>Type of index: 0 managed index, 1 unmanaged index</d:documentation>
      </d:integer>
      <d:obsoletecolumn name="indexname" />
      <d:varchar name="definedby" maxlength="512">
        <d:documentation>Source where we last saw this index</d:documentation>
      </d:varchar>
      <d:varchar name="fieldgroups" maxlength="512">
        <d:documentation>Field groups included in the catalog mapping as set by either wh consilio:update or the catalog API.</d:documentation>
      </d:varchar>
      <d:varchar name="lang" maxlength="5">
        <d:documentation>Index base language</d:documentation>
      </d:varchar>
      <d:varchar name="suffix" maxlength="32">
        <d:documentation>Suffix for suffixed indices. Will currently contain either `-*` or be empty.</d:documentation>
      </d:varchar>
      <d:blob name="internalmetadata">
        <d:documentation>Internal metadata (eg mapping definitions. hson encoded)</d:documentation>
      </d:blob>
    </d:table>

    <d:table name="catalog_indexmanagers" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:integer name="catalogid" ondelete="cascade" references=".catalogs">
        <d:documentation>The catalog</d:documentation>
      </d:integer>
      <d:integer name="indexmanager" nullable="false" ondelete="cascade" references=".indexmanagers">
        <d:documentation>The index manager that is responsible for this catalog</d:documentation>
      </d:integer>
      <d:varchar name="indexname" maxlength="128" nullable="0">
        <d:documentation>Index name in OpenSearch</d:documentation>
      </d:varchar>
      <d:integer name="searchpriority">
        <d:documentation>The 'priority' with which to use this indexmanager for searching. Higher priorities get used before lower priorities, priorities zero and lower are ignored for searches.</d:documentation>
      </d:integer>
      <d:boolean name="readonly">
        <d:documentation>Disable writes and mapping updates</d:documentation>
      </d:boolean>
      <d:obsoletecolumn name="rebuild" />
      <d:obsoletecolumn name="deleted" />
      <d:obsoletecolumn name="primary" />
    </d:table>

    <d:table name="contentsources" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:varchar name="contentprovider" maxlength="64" />
      <d:varchar name="data" maxlength="4096" nullable="0">
        <d:documentation>HSON encoded. required because not setting it wil lcurrently crash a lot of code</d:documentation>
      </d:varchar>
      <d:obsoletecolumn name="discardsummaries" />
      <d:obsoletecolumn name="indexid" />
      <d:integer name="catalogid" nullable="0" ondelete="cascade" references=".catalogs">
        <d:documentation>The catalog this content source belongs to</d:documentation>
      </d:integer>
      <d:obsoletecolumn name="maxgroupobjects" /> <!-- Obsoleted 2023-05-26 (5.3) -->
      <d:integer name="fsobject" ondelete="set default" references="system.fs_objects">
        <d:documentation>Associated WHFS object. For consilio:site content sources, the publisher folder where indexing starts</d:documentation>
      </d:integer>
      <d:varchar name="tag" maxlength="64">
        <d:documentation>Tag assigned to the source</d:documentation>
      </d:varchar>
      <d:varchar name="title" maxlength="256">
        <d:documentation>Title for the content source</d:documentation>
      </d:varchar>
      <d:integer name="status">
        <d:documentation>-1 disabled, 0 idle, 1 being updated (obsolete), 2 being rebuild, 3 unavailable, 4 error</d:documentation>
      </d:integer>
      <d:datetime name="last_indexed" />
      <d:datetime name="orphansince">
        <d:documentation>When did we last see this contentsource mentioned in a siteprofile ?</d:documentation>
      </d:datetime>
      <d:varchar name="definedby" maxlength="512">
        <d:documentation>Source where we last saw this contentsource</d:documentation>
      </d:varchar>
    </d:table>

    <d:obsoletetable name="mapping" />

    <d:obsoletetable name="contentsourcefields" />

    <!-- Link checking -->
    <d:table name="checked_links" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:datetime name="checked" />
      <d:integer name="checktime" />
      <d:integer name="hashed_url_int" />
      <d:datetime name="last_referred" />
      <d:datetime name="nextcheck" />
      <d:integer name="status" />
      <d:varchar name="url" maxlength="1024" nullable="0" />
      <d:index name="nextchecks" columns="nextcheck" />
      <d:index name="hashed_url_int" columns="hashed_url_int">
        <d:documentation>Index to quickly search hashed_url_int</d:documentation>
      </d:index>
    </d:table>

    <d:table name="checked_objectlinks" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:integer name="link" noupdate="1" nullable="0" ondelete="cascade" references=".checked_links" />
      <d:number name="wrd_entity_setting" noupdate="1" nullable="1" ondelete="cascade" references="wrd.entity_settings" />
      <d:number name="system_fs_setting" noupdate="1" nullable="1" ondelete="cascade" references="system.fs_settings" />
      <d:varchar name="text" maxlength="90" />
      <d:varchar name="url" maxlength="1024" nullable="0" />  <!-- Original, non-normalized URL -->
      <d:index name="objectslinks" columns="wrd_entity_setting system_fs_setting link" unique="1" />
    </d:table>

    <d:table name="checked_servers" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:datetime name="down_since" />
      <d:datetime name="nextcheck" />
      <d:varchar name="server" maxlength="262" nullable="0" />
      <d:integer name="status" />
      <d:index name="servers" columns="server(52)" unique="1" uppercase="1" />
    </d:table>

    <!-- Settings -->
    <d:table name="indexmanagers" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:varchar name="name" maxlength="256" nullable="0" unique="1">
        <d:documentation>The name of the index manager</d:documentation>
      </d:varchar>
      <d:varchar name="address" maxlength="512" nullable="0" unique="1">
        <d:documentation>Connect url, including username and password, if necessary (may be "builtin" for type 1)</d:documentation>
      </d:varchar>
      <d:varchar name="configuration" maxlength="4096">
        <d:documentation>HSON encoded server configuration</d:documentation>
      </d:varchar>
      <d:obsoletecolumn name="type" />
      <d:obsoletecolumn name="loglevel" />
      <d:obsoletecolumn name="enablestemming" />
      <d:obsoletecolumn name="secure" />
    </d:table>

    <d:table name="thesaurus" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:obsoletecolumn name="indexid" />
      <d:integer name="catalogid" nullable="0" ondelete="cascade" references=".catalogs" />
      <d:varchar name="word" maxlength="512" nullable="0" />
      <d:integer name="wordgroup" nullable="0" />
    </d:table>

    <d:table name="blacklist" primarykey="id">
      <d:integer name="id" autonumberstart="1" />
      <d:boolean name="nofollow" />
      <d:boolean name="noindex" />
      <d:boolean name="nolinkcheck_from" />
      <d:boolean name="nolinkcheck_to" />
      <d:varchar name="pattern" maxlength="1024" nullable="0" unique="1" />
      <d:boolean name="pattern_regex" />
    </d:table>

    <d:obsoletetable name="query_results" primarykey="id" />

    <d:obsoletetable name="query_clicks" primarykey="id" />
  </databaseschema>

  <moduleregistry>
    <node name="queuemanager">
      <integer name="loglevel" description="Logging level" initialval="0" />
      <integer name="maxfetchtime" description="Maximum time (in seconds) fetching a document may take" initialval="180" />
      <obsoletenode name="maxgroupobjects" />
      <integer name="workers" description="Number of parallel workers to start" initialval="2" />
    </node>
    <node name="linkchecker">
      <boolean name="enabled" description="Enable link checking" initialval="true" />
      <boolean name="contentlinksonly" description="Only check links within indexed content" initialval="true" />
      <integer name="keeplinks" description="Number of days after which unreferenced links are removed" initialval="3" />
    </node>
    <node name="options">
      <integer name="keepsearchterms" description="Number of days to keep searched terms" initialval="30" />
    </node>

    <!-- Migrated to consilio.indexmanagers -->
    <obsoletenode name="indexmanager" />
    <obsoletenode name="builtinelasticsearch" />

    <node name="suggest">
      <integer name="maxconcurrent" description="Maximum of concurrent suggest queries" initialval="10" />
      <integer name="maxwait" description="Maximum queue wait time for suggest queries" initialval="200" />
    </node>
  </moduleregistry>

  <consilio>
    <contentprovider name="site"
                     contentproviderobject="lib/internal/sitecontent/sitecontentprovider.whlib#WebHareContentProvider"
                     icon="tollium:folders/site"
                     tid="contentproviders.site.providertitle" />
    <contentprovider name="custom"
                     contentproviderobject="lib/contentproviders/customcontent.whlib#CustomContentProvider"
                     icon="tollium:database/database"
                     tid="contentproviders.custom.providertitle" />
    <catalog tag="whfs" priority="-5">
      <customsource contentobject="lib/internal/whfscatalog/whfsobjectsource.whlib#WHFSObjectContentSource" tag="whfsobjects" />
    </catalog>
    <fieldgroup tag="pxl_data">
      <keyword name="ds_*" />
      <float name="dn_*" />
      <boolean name="db_*" />
    </fieldgroup>

    <!-- TODO ?
         Originally there was a 'lastmodified' field but it was inconstently set
         'size' should have been a 64bit field
         'whfspath' should have been a keyword field
         'keywords'... perhaps too? or is stemming relevant then

         perhaps a next-gen sitecontent would fix this -->
    <fieldgroup tag="sitecontent">
      <text name="title" suggested="true" />
      <keyword name="filetype" />
      <datetime name="modificationdate" />
      <integer name="size" />
      <text name="whfspath" />
      <text name="keywords" suggested="true" />
      <text name="description" suggested="true" />
    </fieldgroup>

    <!-- Fieldgroups used in reference test -->
    <fieldgroup tag="test_reference_group">
      <text name="consilio" />
      <addfieldgroup ref="webhare_testsuite:test_reference_group" />
    </fieldgroup>
    <fieldgroup tag="circular_test_reference_group">
      <text name="consilio" />
      <addfieldgroup ref="webhare_testsuite:circular_test_reference_group" />
    </fieldgroup>
  </consilio>

  <hooking>
    <target name="fetcher_blacklist" />  <!-- Register extensions to fetcher blacklisting -->
    <intercept name="blacklist"
               interceptfunction="lib/internal/fetcher_blacklisting.whlib#BlacklistIntercept"
               target="fetcher_blacklist" />
  </hooking>

  <logging>
    <log name="queries" filename="consilio_queries" timestamps="false" />
  </logging>

  <development>
    <debugflag name="searches" description="Log searches" />
    <debugflag name="traffic" description="RPClog indexmanager traffic" />
    <debugflag name="whfscatalog" description="Log timings/actions around the WHFS catalog" />
    <debugflag name="queuemanager" description="Enable queuemanager debug output" />
  </development>

</module>
