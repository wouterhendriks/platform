# fast_float
This directory contains the necessaray include files of the fast_float header-only library.

Source: https://github.com/fastfloat/fast_float
Commit: 8e53c41385ac1250812e35820ec82a2279eb69d2, 19 March 2022

From the README.md file of that project:

# License
Licensed under either of Apache License, Version 2.0 or MIT license at your option.

Unless you explicitly state otherwise, any contribution intentionally submitted for inclusion in this repository by you, as defined in the Apache-2.0 license, shall be dual licensed as above, without any additional terms or conditions.
