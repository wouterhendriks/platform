# Dragonbox
This directory contains the necessaray include files of the Dragonbox header-only library.

Source: https://github.com/jk-jeon/dragonbox
Commit: 9c26179a9c4368db9f7703879e3b2e34aa0e29c2, 26 February 2022

From the README.md file of that project:

# License
All code, except for those belong to third-party libraries (code in [`subproject/3rdparty`](subproject/3rdparty)), is licensed under either of

 * Apache License Version 2.0 with LLVM Exceptions ([LICENSE-Apache2-LLVM](LICENSE-Apache2-LLVM) or https://llvm.org/foundation/relicensing/LICENSE.txt) or
 * Boost Software License Version 1.0 ([LICENSE-Boost](LICENSE-Boost) or https://www.boost.org/LICENSE_1_0.txt).
