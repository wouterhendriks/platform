#!/bin/bash
# We can't mark this script as executable as it shouldn't be run on a build host. But we still need the she-bang for shellcheck

# DL instructions from https://opensearch.org/docs/latest/opensearch/install/tar/
# DL packages from here - https://opensearch.org/downloads.html

# To locally test and debug changes to the OpenSearch build and initialization in docker:
# wh builddocker && wh testdocker --sh --tag=-external -w local consilio

GETFILE=opensearch-2.6.0-linux-x64.tar.gz
DLPATH=/tmp/downloads/$GETFILE

if ! curl -fsS -o $DLPATH -z $DLPATH https://build.webhare.dev/whbuild/$GETFILE ; then
  rm -f $DLPATH
  echo "Download failed"
  exit 1
fi

mkdir /opt/opensearch
tar zx -C /opt/opensearch --strip-components=1 -f $DLPATH
chown -R opensearch /opt/opensearch

# Remove the bundled JDK and plugins
rm -rf /opt/opensearch/jdk /opt/opensearch/plugins/* /opt/opensearch/performance-analyzer-rca

runuser --user opensearch --group opensearch -- /opt/opensearch/bin/opensearch --version
RETVAL="$?"
if [ "$RETVAL" != "0" ]; then
  echo "Install failed? Errorcode $RETVAL from 'opensearch --version'"
  exit 1
fi

# Remove alll the shipped plugins
#for PLUGIN in $(runuser --user opensearch --group opensearch -- /opt/opensearch/bin/opensearch-plugin list); do
#  runuser --user opensearch --group opensearch -- /opt/opensearch/bin/opensearch-plugin remove "$PLUGIN"
#done

runuser --user opensearch --group opensearch -- /opt/opensearch/bin/opensearch-plugin install analysis-icu
RETVAL="$?"
if [ "$RETVAL" != "0" ]; then
  echo "Install failed? Errorcode $RETVAL from analysis-icu installation"
  exit 1
fi



exit 0
