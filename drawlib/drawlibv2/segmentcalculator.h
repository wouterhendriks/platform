#ifndef segmentcalculator_h
#define segmentcalculator_h

namespace DrawLib
{
        //never retuns 0;
        uint32_t GetNumberOfSegments(double xradius, double yradius);
}
#endif
